; indicator.nsi
;
; A container for the Single IO Tester installer.
; Drops a copy of the files into the temp folder and executes setup.
; Can install the .NET framework.
;--------------------------------

!Include "${NSISDIR}\Include\x64.nsh"
!Include $%my%\scripts\nsis\!PostExec.nsh
!Include $%my%\scripts\nsis\!GetVersionLocal.nsh

!define MyVer_
!insertmacro GetVersionLocal "$%my%\LIBRARIES\VS10\IO\OpenLayers.2.x\Testers\SingleIO\bin\Debug\isr.IO.OpenLayers.SingleIO.exe" MyVer_

!define VERSION "${MyVer_1}.${MyVer_2}.${MyVer_3}"
!define FileVersion "${MyVer_1}.${MyVer_2}.${MyVer_3}.${MyVer_4}"
!define TRADEMARK "IO.OpenLayers.SingleIO"
!define PRODUCT "${TRADEMARK}.2013"
!define DESCRIPTION "Open Layers Single IO Tester"

!define NETVersion "4.0.30319"
!ifdef NETInstaller
  !define FileDescription "${DESCRIPTION} Full Setup"
  !define TITLE "${PRODUCT}.FullSetup"
!else
  !define TITLE "${PRODUCT}.Setup"
  !define FileDescription "${DESCRIPTION} Setup"
!endif

!define AUTHOR "Integrated Scientific Resources, Inc."
!define COMPANY "Integrated.Scientific.Resources"

; The name of the installer
!define NAME "${TITLE}-${VERSION}"
Name "${NAME}"

; The file to write
!define OUTFILE "$%my%\BUILDS\Testers\OpenLayers\DotNetTester.exe"
OutFile ${OUTFILE}

; sign the installer after it has been created
${PostExec3} "%my%\PRIVATE\Certificate\SignAssemblyLocalLog.bat" "${TITLE}" ${OUTFILE}

; The default installation directory
InstallDir $Temp\${PRODUCT}

; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

;Page directory
;Page instfiles

;--------------------------------

LoadLanguageFile "${NSISDIR}\Contrib\Language files\English.nlf"
;--------------------------------
;Version Information

  VIProductVersion "${VERSION}.0"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "${PRODUCT}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" "${DESCRIPTION}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "${COMPANY}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalTrademarks" "${TRADEMARK} is a trademark"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "(c) 2005 ${AUTHOR}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${FileDescription}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "${FileVersion}"

;--------------------------------
; The stuff to install
Section "" ;No components page, name is not important

  ; Set output path to the installation directory.
  SetOutPath "$INSTDIR"
  SetOverwrite on

  ; .NET Option
  ; Must define NETVersion; NETInstaller; NETfolder
  !ifdef NETInstaller
    !Include $%my%\scripts\nsis\DotNetFramework.nsh
  !endif
  
  IfFileExists "$WINDIR\Microsoft.NET\Framework\v${NETVersion}" PROCEED1 0
  !ifdef NETInstaller
    DetailPrint "Microsoft .NET Framework v${NETVersion} did not install."
    MessageBox MB_OK|MB_ICONSTOP \
      "Microsoft .NET Framework v${NETVersion} did not install. Try to restart."
  !Else
    DetailPrint "Microsoft .NET Framework v${NETVersion} not found."
    MessageBox MB_OK|MB_ICONSTOP \
      "Microsoft .NET Framework v${NETVersion} not found. Use the ${DESCRIPTION}.FullSetup to install ${PRODUCT}."
  !endif
  MessageBox MB_OK|MB_ICONINFORMATION "Aborting."
  Abort "Installation aborted. Microsoft .NET Framework v${NETVersion} not found."
  
PROCEED1:
  MessageBox MB_OK|MB_ICONINFORMATION "Microsoft .NET Framework v${NETVersion} Exists."

  ; Put file there
  ${If} ${RunningX64}
	DetailPrint "Running under Windows 64 bits."
    MessageBox MB_OK|MB_ICONINFORMATION "Installing 64 bit system"
  ${Else}
	DetailPrint "Running under Windows 32 bits."
    MessageBox MB_OK|MB_ICONINFORMATION "Installing 32 bit system"

   ${EndIf}
    MessageBox MB_OK "Done"

SectionEnd ; end the section
