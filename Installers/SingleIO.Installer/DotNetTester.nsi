; DotNettester.nsi
;
; A container for the Single IO Tester installer.
; Drops a copy of the files into the temp folder and executes setup.
; Can install the .NET framework.
;--------------------------------

Var Message

!Include "${NSISDIR}\Include\x64.nsh"
!Include "${NSISDIR}\Include\LogicLib.nsh"
!Include $%my%\scripts\nsis\!PostExec.nsh
!Include $%my%\scripts\nsis\!GetVersionLocal.nsh

!define MyVer_
!insertmacro GetVersionLocal "$%my%\LIBRARIES\VS10\IO\OpenLayers.2.x\Testers\SingleIO\bin\Debug\isr.IO.OpenLayers.SingleIO.exe" MyVer_

!define VERSION "${MyVer_1}.${MyVer_2}.${MyVer_3}"
!define FileVersion "${MyVer_1}.${MyVer_2}.${MyVer_3}.${MyVer_4}"
!define TRADEMARK "IO.OpenLayers.SingleIO"
!define PRODUCT "${TRADEMARK}.2013"
!define DESCRIPTION "Open Layers Single IO Tester"

; .NET Option
; Must define NETVersion; NETInstaller
!define NETVersion "4.0"
!define NETlabel "Microsoft .NET Framework v${NETVersion}"
!ifdef NETInstaller
  !define FileDescription "${DESCRIPTION} Full Setup"
  !define TITLE "${PRODUCT}.FullSetup"
!else
  !define TITLE "${PRODUCT}.Setup"
  !define FileDescription "${DESCRIPTION} Setup"
!endif

!define AUTHOR "Integrated Scientific Resources, Inc."
!define COMPANY "Integrated.Scientific.Resources"

; The name of the installer
!define NAME "${TITLE}-${VERSION}"
Name "${NAME}"

; The file to write
!define OUTFILE "$%my%\BUILDS\Testers\OpenLayers\DotNetTester.exe"
OutFile ${OUTFILE}

; sign the installer after it has been created
${PostExec3} "%my%\PRIVATE\Certificate\SignAssemblyLocalLog.bat" "${TITLE}" ${OUTFILE}

; The default installation directory
InstallDir $Temp\${PRODUCT}

; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

;Page directory
;Page instfiles

;--------------------------------

LoadLanguageFile "${NSISDIR}\Contrib\Language files\English.nlf"
;--------------------------------
;Version Information

  VIProductVersion "${VERSION}.0"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "${PRODUCT}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" "${DESCRIPTION}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "${COMPANY}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalTrademarks" "${TRADEMARK} is a trademark"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "(c) 2005 ${AUTHOR}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${FileDescription}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "${FileVersion}"

;--------------------------------
; The stuff to install
Section "" ;No components page, name is not important

  ; Set output path to the installation directory.
  SetOutPath "$INSTDIR"
  SetOverwrite on

  ; .NET Option
  ; Must define NETVersion; NETInstaller
  !Include "${NSISDIR}\Include\DotNetVer.nsh"
  !ifdef NETInstaller
    !Include $%my%\scripts\nsis\DotNetFramework.nsh
  !endif

!ifdef NETInstaller
	${If} ${HasDotNet4.0}
		StrCpy $Message "${NETlabel} Exists."
		DetailPrint $Message
		MessageBox MB_OK|MB_ICONINFORMATION $Message
		${If} ${DOTNETVER_4_0} AtLeastDotNetServicePack 1
			StrCpy $Message "${NETlabel} is at least SP1."
			DetailPrint $Message
			MessageBox MB_OK|MB_ICONINFORMATION $Message
		${Else}
			StrCpy $Message "${NETlabel} SP1 not installed."
			DetailPrint $Message
			MessageBox MB_OK|MB_ICONINFORMATION $Message
		${EndIf}
		${If} ${DOTNETVER_4_0} HasDotNetClientProfile 1
			StrCpy $Message "${NETlabel} (Client Profile) available."
			DetailPrint $Message
			MessageBox MB_OK|MB_ICONINFORMATION $Message
		${EndIf}
		${If} ${DOTNETVER_4_0} HasDotNetFullProfile 0
			StrCpy $Message "${NETlabel} (Full Profile) not available."
			DetailPrint $Message
			MessageBox MB_OK|MB_ICONINFORMATION $Message
		${EndIf}
		${If} ${DOTNETVER_4_0} HasDotNetFullProfile 1
			StrCpy $Message "${NETlabel} (Full Profile) available."
			DetailPrint $Message
			MessageBox MB_OK|MB_ICONINFORMATION $Message
			Goto PROCEED1
		${EndIf}
	${Else}
		StrCpy $Message "${NETlabel} not installed. Try to restart this computer or reinstall the program."
		DetailPrint $Message
		MessageBox MB_OK|MB_ICONINFORMATION $Message
	${EndIf}
!Else
	${If} ${HasDotNet4.0}
		StrCpy $Message "${NETlabel} Exists."
		DetailPrint $Message
		MessageBox MB_OK|MB_ICONINFORMATION $Message
		; MessageBox MB_OK|MB_ICONINFORMATION $__DOTNETVER_4.0_SP
		${If} ${DOTNETVER_4_0} AtLeastDotNetServicePack 1
			StrCpy $Message "${NETlabel} is at least SP1."
			DetailPrint $Message
			MessageBox MB_OK|MB_ICONINFORMATION $Message
		${Else}
			StrCpy $Message "${NETlabel} SP1 not installed."
			DetailPrint $Message
			MessageBox MB_OK|MB_ICONINFORMATION $Message
		${EndIf}
		${If} ${DOTNETVER_4_0} HasDotNetClientProfile 1
			StrCpy $Message "${NETlabel} (Client Profile) available."
			DetailPrint $Message
			MessageBox MB_OK|MB_ICONINFORMATION $Message
		${EndIf}
		${If} ${DOTNETVER_4_0} HasDotNetFullProfile 0
			StrCpy $Message "${NETlabel} (Full Profile) not available."
			DetailPrint $Message
			MessageBox MB_OK|MB_ICONINFORMATION $Message
		${EndIf}
		${If} ${DOTNETVER_4_0} HasDotNetFullProfile 1
			StrCpy $Message "${NETlabel} (Full Profile) available."
			DetailPrint $Message
			MessageBox MB_OK|MB_ICONINFORMATION $Message
			Goto PROCEED1
		${EndIf}
	${Else}
		StrCpy $Message "${NETlabel} not installed."
		DetailPrint $Message
		MessageBox MB_OK|MB_ICONINFORMATION $Message
	${EndIf}
!endif
StrCpy $Message "Installation aborted. ${NETlabel} not found."
DetailPrint $Message
Abort $Message
  
PROCEED1:

StrCpy $Message "Select OK to continue or CANCEL to cancel this installation."
DetailPrint $Message
MessageBox MB_OKCANCEL|MB_ICONQUESTION $Message IDOK okay IDCANCEL cancel
cancel:
	StrCpy $Message "Installation aborted. ${NETlabel} not found."
	DetailPrint $Message
	Abort $Message
okay:

	StrCpy $Message "${NETlabel} exists."
	DetailPrint $Message
	MessageBox MB_OK|MB_ICONINFORMATION $Message

	; Put file there
	${If} ${RunningX64}
		StrCpy $Message "Installing 64 bit system"
		DetailPrint $Message
		MessageBox MB_OK|MB_ICONINFORMATION $Message
	${Else}
		StrCpy $Message "Installing 32 bit system"
		DetailPrint $Message
		MessageBox MB_OK|MB_ICONINFORMATION $Message
    ${EndIf}
	StrCpy $Message "Done"
	DetailPrint $Message
	MessageBox MB_OK|MB_ICONINFORMATION $Message

SectionEnd ; end the section
