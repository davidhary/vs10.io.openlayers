@echo off
setlocal
if '%my%'=='' set my=C:\My
set src=%SystemRoot%\assembly\gac\openlayers.controls\1.0.0.0__6ee318a4e5ffdf99
set dst=%my%\LIBRARIES\VS10\IO\Openlayers\Drivers
set dst=%programfiles(x86)%\Data Translation\DotNet\OLClassLib\Framework 2.0 Assemblies
set fl=OpenLayers.Controls.dll
echo From: "%src%" 
echo TO:   "%dst%"
echo FILE: %fl%
Pause
robocopy "%src%" "%dst%" %fl%
pause

:cleanup
set fl=
Set dst=
Set src=
pause
endlocal
