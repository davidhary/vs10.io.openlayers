﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Shared ReadOnly TraceEventId As Integer = isr.Core.Diagnosis.TraceEventIds.isrIOOpenLayersSingleIO

        Public Const AssemblyTitle As String = "Open Layers Library Single I/O Tester"
        Public Const AssemblyDescription As String = "Single I/O Tester for the Open Layers Library"
        Public Const AssemblyProduct As String = "IO.Open.Layers.SingleIO.Tester.2014"

    End Class

End Namespace

