<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class AnalogInputDisplayPanel

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._AnalogInputDisplay = New isr.IO.OL.AnalogInputDisplay()
        Me._Tabs = New System.Windows.Forms.TabControl()
        Me._DisplayTabPage = New System.Windows.Forms.TabPage()
        Me._MessagesTabPage = New System.Windows.Forms.TabPage()
        Me._MessagesBox = New isr.Core.Diagnosis.TraceMessagesBox()
        Me._Tabs.SuspendLayout()
        Me._DisplayTabPage.SuspendLayout()
        Me._MessagesTabPage.SuspendLayout()
        Me.SuspendLayout()
        '
        '_AnalogInputDisplay
        '
        Me._AnalogInputDisplay.CalculatePeriodCount = 0
        Me._AnalogInputDisplay.ChartMode = isr.IO.OL.ChartModeId.Scope
        Me._AnalogInputDisplay.ConfigurationPanelVisible = True
        Me._AnalogInputDisplay.Dock = System.Windows.Forms.DockStyle.Fill
        Me._AnalogInputDisplay.Location = New System.Drawing.Point(3, 3)
        Me._AnalogInputDisplay.MaximumMovingAverageLength = 1000
        Me._AnalogInputDisplay.MaximumRefreshRate = New Decimal(New Integer() {100, 0, 0, 0})
        Me._AnalogInputDisplay.MaximumSampleSize = 1000000
        Me._AnalogInputDisplay.MaximumSamplingRate = New Decimal(New Integer() {100000000, 0, 0, 0})
        Me._AnalogInputDisplay.MaximumSignalBufferSize = 1000000
        Me._AnalogInputDisplay.MinimumMovingAverageLength = 2
        Me._AnalogInputDisplay.MinimumRefreshRate = New Decimal(New Integer() {1, 0, 0, 0})
        Me._AnalogInputDisplay.MinimumSamplingRate = New Decimal(New Integer() {0, 0, 0, 0})
        Me._AnalogInputDisplay.MinimumSignalMemoryLength = 0
        Me._AnalogInputDisplay.Name = "_AnalogInputDisplay"
        Me._AnalogInputDisplay.SampleSize = 1000
        Me._AnalogInputDisplay.SamplingPeriod = 0.0R
        Me._AnalogInputDisplay.SignalBufferSize = 1000
        Me._AnalogInputDisplay.SignalMemoryLength = 10000
        Me._AnalogInputDisplay.Size = New System.Drawing.Size(871, 510)
        Me._AnalogInputDisplay.TabIndex = 0
        Me._AnalogInputDisplay.UpdatePeriodCount = 10
        '
        '_Tabs
        '
        Me._Tabs.Controls.Add(Me._DisplayTabPage)
        Me._Tabs.Controls.Add(Me._MessagesTabPage)
        Me._Tabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Tabs.Location = New System.Drawing.Point(0, 0)
        Me._Tabs.Name = "_Tabs"
        Me._Tabs.SelectedIndex = 0
        Me._Tabs.Size = New System.Drawing.Size(885, 542)
        Me._Tabs.TabIndex = 1
        '
        '_DisplayTabPage
        '
        Me._DisplayTabPage.Controls.Add(Me._AnalogInputDisplay)
        Me._DisplayTabPage.Location = New System.Drawing.Point(4, 22)
        Me._DisplayTabPage.Name = "_DisplayTabPage"
        Me._DisplayTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._DisplayTabPage.Size = New System.Drawing.Size(877, 516)
        Me._DisplayTabPage.TabIndex = 0
        Me._DisplayTabPage.Text = "DISPLAY"
        Me._DisplayTabPage.UseVisualStyleBackColor = True
        '
        '_MessagesTabPage
        '
        Me._MessagesTabPage.Controls.Add(Me._MessagesBox)
        Me._MessagesTabPage.Location = New System.Drawing.Point(4, 22)
        Me._MessagesTabPage.Name = "_MessagesTabPage"
        Me._MessagesTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._MessagesTabPage.Size = New System.Drawing.Size(877, 516)
        Me._MessagesTabPage.TabIndex = 1
        Me._MessagesTabPage.Text = "MESSAGES"
        Me._MessagesTabPage.UseVisualStyleBackColor = True
        '
        '_MessagesBox
        '
        Me._MessagesBox.BackColor = System.Drawing.SystemColors.Info
        Me._MessagesBox.CausesValidation = False
        Me._MessagesBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._MessagesBox.Location = New System.Drawing.Point(3, 3)
        Me._MessagesBox.Multiline = True
        Me._MessagesBox.Name = "_MessagesBox"
        Me._MessagesBox.PresetCount = 50
        Me._MessagesBox.ReadOnly = True
        Me._MessagesBox.ResetCount = 100
        Me._MessagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._MessagesBox.Size = New System.Drawing.Size(871, 510)
        Me._MessagesBox.TabIndex = 0
        '
        'AnalogInputDisplayPanel
        '
        Me.ClientSize = New System.Drawing.Size(885, 542)
        Me.Controls.Add(Me._Tabs)
        Me.Name = "AnalogInputDisplayPanel"
        Me.Text = "Analog Input Display Panel"
        Me._Tabs.ResumeLayout(False)
        Me._DisplayTabPage.ResumeLayout(False)
        Me._MessagesTabPage.ResumeLayout(False)
        Me._MessagesTabPage.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _AnalogInputDisplay As isr.IO.OL.AnalogInputDisplay
    Private WithEvents _Tabs As System.Windows.Forms.TabControl
    Private WithEvents _DisplayTabPage As System.Windows.Forms.TabPage
    Private WithEvents _MessagesTabPage As System.Windows.Forms.TabPage
    Private WithEvents _MessagesBox As isr.Core.Diagnosis.TraceMessagesBox
End Class
