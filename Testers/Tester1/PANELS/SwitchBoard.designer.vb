<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Switchboard

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._OpenScopeButton = New System.Windows.Forms.Button
        Me._OpenSingleIoPanelButton = New System.Windows.Forms.Button
        Me._LaunchAnalogDisplayButton = New System.Windows.Forms.Button
        Me._ExitButton = New System.Windows.Forms.Button
        Me._ContinuousDisplayButton = New System.Windows.Forms.Button
        Me._StripChartControlButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        '_OpenScopeButton
        '
        Me._OpenScopeButton.Location = New System.Drawing.Point(99, 89)
        Me._OpenScopeButton.Name = "openScopeButton"
        Me._OpenScopeButton.Size = New System.Drawing.Size(162, 46)
        Me._OpenScopeButton.TabIndex = 9
        Me._OpenScopeButton.Text = "&Oscilloscope"
        '
        '_OpenSingleIoPanelButton
        '
        Me._OpenSingleIoPanelButton.Location = New System.Drawing.Point(99, 165)
        Me._OpenSingleIoPanelButton.Name = "_OpenSingleIoPanelButton"
        Me._OpenSingleIoPanelButton.Size = New System.Drawing.Size(162, 46)
        Me._OpenSingleIoPanelButton.TabIndex = 8
        Me._OpenSingleIoPanelButton.Text = "&Single I/O Panel"
        '
        '_LaunchAnalogDisplayButton
        '
        Me._LaunchAnalogDisplayButton.Location = New System.Drawing.Point(315, 12)
        Me._LaunchAnalogDisplayButton.Name = "launchAnalogDisplayButton"
        Me._LaunchAnalogDisplayButton.Size = New System.Drawing.Size(162, 49)
        Me._LaunchAnalogDisplayButton.TabIndex = 7
        Me._LaunchAnalogDisplayButton.Text = "&Analog Input Display "
        '
        '_ExitButton
        '
        Me._ExitButton.Location = New System.Drawing.Point(402, 188)
        Me._ExitButton.Name = "_ExitButton"
        Me._ExitButton.Size = New System.Drawing.Size(75, 23)
        Me._ExitButton.TabIndex = 4
        Me._ExitButton.Text = "E&xit"
        '
        '_ContinuousDisplayButton
        '
        Me._ContinuousDisplayButton.Location = New System.Drawing.Point(99, 12)
        Me._ContinuousDisplayButton.Name = "_ContinuousDisplayButton"
        Me._ContinuousDisplayButton.Size = New System.Drawing.Size(162, 49)
        Me._ContinuousDisplayButton.TabIndex = 6
        Me._ContinuousDisplayButton.Text = "&Continuous Display"
        '
        '_StripChartControlButton
        '
        Me._StripChartControlButton.Enabled = False
        Me._StripChartControlButton.Location = New System.Drawing.Point(315, 86)
        Me._StripChartControlButton.Name = "_StripChartControlButton"
        Me._StripChartControlButton.Size = New System.Drawing.Size(162, 49)
        Me._StripChartControlButton.TabIndex = 5
        Me._StripChartControlButton.Text = "&Strip Chart"
        '
        'SwitchBoard
        '
        Me.ClientSize = New System.Drawing.Size(585, 239)
        Me.Controls.Add(Me._OpenScopeButton)
        Me.Controls.Add(Me._OpenSingleIoPanelButton)
        Me.Controls.Add(Me._LaunchAnalogDisplayButton)
        Me.Controls.Add(Me._ExitButton)
        Me.Controls.Add(Me._ContinuousDisplayButton)
        Me.Controls.Add(Me._StripChartControlButton)
        Me.Name = "SwitchBoard"
        Me.Text = "SwitchBoard"
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _OpenScopeButton As System.Windows.Forms.Button
    Private WithEvents _OpenSingleIoPanelButton As System.Windows.Forms.Button
    Private WithEvents _LaunchAnalogDisplayButton As System.Windows.Forms.Button
    Private WithEvents _ExitButton As System.Windows.Forms.Button
    Private WithEvents _ContinuousDisplayButton As System.Windows.Forms.Button
    Private WithEvents _StripChartControlButton As System.Windows.Forms.Button
End Class
