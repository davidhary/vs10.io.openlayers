; SingleIO.Installer.nsi
;
; A container for the Single IO Tester installer.
; Drops a copy of the files into the temp folder and executes setup.
; Can install the .NET framework.
;--------------------------------

Var Message

!Include "${NSISDIR}\Include\x64.nsh"
!Include "${NSISDIR}\Include\LogicLib.nsh"
!Include $%my%\scripts\nsis\!PostExec.nsh
!Include $%my%\scripts\nsis\!GetVersionLocal.nsh

!define MyVer_
!insertmacro GetVersionLocal "$%my%\LIBRARIES\VS10\IO\OpenLayers\Testers\SingleIO\bin\Debug\isr.IO.OpenLayers.SingleIO.exe" MyVer_

!define VERSION "${MyVer_1}.${MyVer_2}.${MyVer_3}"
!define FileVersion "${MyVer_1}.${MyVer_2}.${MyVer_3}.${MyVer_4}"
!define TRADEMARK "IO.OpenLayers.SingleIO"
!define PRODUCT "${TRADEMARK}.2013"
!define DESCRIPTION "Open Layers Single IO Tester"

; .NET Option
; Must define NETVersion; NETInstaller
!define NETVersion "4.0"
!define NETlabel "Microsoft .NET Framework v${NETVersion}"
!ifdef NETInstaller
  !define FileDescription "${DESCRIPTION} Full Setup"
  !define TITLE "${PRODUCT}.FullSetup"
!else
  !define TITLE "${PRODUCT}.Setup"
  !define FileDescription "${DESCRIPTION} Setup"
!endif

!define AUTHOR "Integrated Scientific Resources, Inc."
!define COMPANY "Integrated.Scientific.Resources"

; The name of the installer
!define NAME "${TITLE}-${VERSION}"
Name "${NAME}"

; The file to write
!define OUTFILE "$%my%\BUILDS\Testers\OpenLayers\${NAME}.exe"
OutFile ${OUTFILE}

; sign the installer after it has been created
${PostExec3} "%my%\PRIVATE\Certificate\SignAssemblyLocalLog.bat" "${TITLE}" ${OUTFILE}

; The default installation directory
InstallDir $Temp\${PRODUCT}

; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

;Page directory
;Page instfiles

;--------------------------------

LoadLanguageFile "${NSISDIR}\Contrib\Language files\English.nlf"
;--------------------------------
;Version Information

  VIProductVersion "${VERSION}.0"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "${PRODUCT}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "Comments" "${DESCRIPTION}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "${COMPANY}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalTrademarks" "${TRADEMARK} is a trademark"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "(c) 2005 ${AUTHOR}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "${FileDescription}"
  VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "${FileVersion}"

;--------------------------------
; The stuff to install
Section "" ;No components page, name is not important

  ; Set output path to the installation directory.
  SetOutPath "$INSTDIR"
  SetOverwrite on

  ; .NET Option
  ; Must define NETVersion; NETInstaller
  !Include "${NSISDIR}\Include\DotNetVer.nsh"
  !ifdef NETInstaller
    !Include $%my%\scripts\nsis\DotNetFramework.nsh
  !endif

!ifdef NETInstaller
	${If} ${HasDotNet4.0}
		StrCpy $Message "${NETlabel} Exists."
		DetailPrint $Message
		${If} ${DOTNETVER_4_0} HasDotNetFullProfile 1
			StrCpy $Message "${NETlabel} (Full Profile) available."
			DetailPrint $Message
			Goto HASDOTNET
		${EndIf}
	${EndIf}
!Else
	${If} ${HasDotNet4.0}
		StrCpy $Message "${NETlabel} Exists."
		DetailPrint $Message
		${If} ${DOTNETVER_4_0} HasDotNetFullProfile 1
			StrCpy $Message "${NETlabel} (Full Profile) available."
			DetailPrint $Message
			Goto HASDOTNET
		${EndIf}
	${EndIf}
!endif

StrCpy $Message "${NETlabel} not found. Select OK to allow the installer to download and install ${NETlabel}."
DetailPrint $Message
MessageBox MB_OKCANCEL|MB_ICONQUESTION $Message IDOK okay IDCANCEL cancel
cancel:
	StrCpy $Message "Installation aborted. ${NETlabel} not found."
	DetailPrint $Message
	Abort $Message
okay:
  
HASDOTNET:	

  ; Put file there
  ${If} ${RunningX64}
	DetailPrint "Running under Windows 64 bits."
    File "bin\x64\debug\SingleIoTesterSetup.msi"
  ${Else}
	DetailPrint "Running under Windows 32 bits."
    File "bin\x86\debug\SingleIoTesterSetup.msi"
   ${EndIf}
  ExecWait "msiexec /i '$INSTDIR\SingleIoTesterSetup.msi' /l* ${TITLE}.log"

SectionEnd ; end the section
