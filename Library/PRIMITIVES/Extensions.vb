﻿Imports System.Runtime.CompilerServices
''' <summary>
''' Extends the Data Translation Open Layers <see cref="OpenLayers.Base.SubsystemBase">subsystem base</see>,
''' <see cref="OpenLayers.Base.Device">Device</see>,
''' <see cref="OpenLayers.Base.ChannelList">Channel List</see>, and 
''' <see cref="OpenLayers.Signals.MemorySignalList">Signal List</see> functionality.
''' </summary>
''' <license>
''' (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Module Extensions

#Region " CHANNEL LIST "

    ''' <summary> Returns the logical channel number (the channel index in the
    ''' <see cref="OpenLayers.Base.ChannelList">channel list</see>)
    ''' of the specified channel name. </summary>
    ''' <param name="channelList"> The channel list. </param>
    ''' <param name="channelName"> Name of the channel. </param>
    ''' <returns> Non-negative channel number if found; otherwise -1. </returns>
    <Extension()>
    Public Function LogicalChannelNumber(ByVal channelList As OpenLayers.Base.ChannelList,
                                         ByVal channelName As String) As Integer
        Dim number As Integer = -1
        If Not String.IsNullOrWhiteSpace(channelName) AndAlso channelList IsNot Nothing Then
            For Each entry As OpenLayers.Base.ChannelListEntry In channelList
                number += 1
                If channelName.Equals(entry.Name, StringComparison.OrdinalIgnoreCase) Then
                    Exit For
                End If
            Next
        End If
        Return number
    End Function

    ''' <summary> Returns the logical channel number (the channel index in the
    ''' <see cref="OpenLayers.Base.ChannelList">channel list</see>)
    ''' of the specified channel number. </summary>
    ''' <param name="channelList">           The channel list. </param>
    ''' <param name="physicalChannelNumber"> The physical channel number. </param>
    ''' <returns> Non-negative channel number if found; otherwise -1. </returns>
    <Extension()>
    Public Function LogicalChannelNumber(ByVal channelList As OpenLayers.Base.ChannelList,
                                         ByVal physicalChannelNumber As Integer) As Integer
        Dim number As Integer = -1
        If channelList IsNot Nothing Then
            For Each entry As OpenLayers.Base.ChannelListEntry In channelList
                number += 1
                If entry.PhysicalChannelNumber = physicalChannelNumber Then
                    Exit For
                End If
            Next
        End If
        Return number
    End Function

    ''' <summary> Returns the logical channel number (the channel index in the
    ''' <see cref="OpenLayers.Base.ChannelList">channel list</see>)
    ''' of the specified channel number. </summary>
    ''' <param name="channelList"> The channel list. </param>
    ''' <param name="channel">     The channel. </param>
    ''' <returns> Non-negative channel number if found; otherwise -1. </returns>
    <Extension()>
    Public Function LogicalChannelNumber(ByVal channelList As OpenLayers.Base.ChannelList,
                                         ByVal channel As OpenLayers.Base.ChannelListEntry) As Integer
        If channelList Is Nothing OrElse channel Is Nothing Then
            Return -1
        Else
            Return LogicalChannelNumber(channelList, channel.PhysicalChannelNumber)
        End If
    End Function

    ''' <summary> Checks if a named channel exists. </summary>
    ''' <param name="channelList"> The channel list. </param>
    ''' <param name="channelName"> Name of the channel. </param>
    ''' <returns> <c>True</c> if exists, <c>False</c> otherwise. </returns>
    <Extension()>
    Public Function Contains(ByVal channelList As OpenLayers.Base.ChannelList, ByVal channelName As String) As Boolean
        Dim result As Boolean = False
        If channelList IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(channelName) Then
            For Each entry As OpenLayers.Base.ChannelListEntry In channelList
                result = channelName.Equals(entry.Name, StringComparison.OrdinalIgnoreCase)
                If result Then Exit For
            Next
        End If
        Return result
    End Function

    ''' <summary> Checks if a physical channel exists. </summary>
    ''' <param name="channelList">           The channel list. </param>
    ''' <param name="physicalChannelNumber"> The physical channel number. </param>
    ''' <returns> <c>True</c> if exists, <c>False</c> otherwise. </returns>
    <Extension()>
    Public Function Contains(ByVal channelList As OpenLayers.Base.ChannelList, ByVal physicalChannelNumber As Integer) As Boolean
        Dim result As Boolean = False
        If channelList IsNot Nothing Then
            For Each entry As OpenLayers.Base.ChannelListEntry In channelList
                result = entry.PhysicalChannelNumber = physicalChannelNumber
                If result Then Exit For
            Next
        End If
        Return result

    End Function

    ''' <summary> Selects a channel from the channel list. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    ''' are null. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="channelList">          The channel list. </param>
    ''' <param name="logicalChannelNumber"> The logical channel number (the channel index in the
    ''' <see cref="OpenLayers.Base.ChannelList">channel list</see>.) </param>
    ''' <returns> OpenLayers.Base.ChannelListEntry. </returns>
    <Extension()>
    Public Function SelectLogicalChannel(ByVal channelList As OpenLayers.Base.ChannelList, ByVal logicalChannelNumber As Integer) As OpenLayers.Base.ChannelListEntry

        If channelList Is Nothing Then
            Throw New ArgumentNullException("channelList", "Channel list is not defined")
        ElseIf channelList.Count = 0 Then
            Throw New ArgumentNullException("channelList", "Channel list is empty")
        ElseIf channelList.Count < logicalChannelNumber + 1 Then
            Throw New ArgumentOutOfRangeException("logicalChannelNumber", logicalChannelNumber, "Channel list does not include this channel.")
        Else
            Return channelList.Item(logicalChannelNumber)
        End If

    End Function

    ''' <summary> Selects a channel by name. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channelList"> The channel list. </param>
    ''' <param name="channelName"> Name of the channel. </param>
    ''' <returns> OpenLayers.Base.ChannelListEntry. </returns>
    <Extension()>
    Public Function SelectNamedChannel(ByVal channelList As OpenLayers.Base.ChannelList, ByVal channelName As String) As OpenLayers.Base.ChannelListEntry
        Dim result As OpenLayers.Base.ChannelListEntry = Nothing
        If channelList Is Nothing Then
            Throw New ArgumentNullException("channelList", "Channel list is not defined")
        ElseIf channelList.Count = 0 Then
            Throw New ArgumentNullException("channelList", "Channel list is empty")
        ElseIf String.IsNullOrWhiteSpace(channelName) Then
            Throw New ArgumentNullException("channelName", "Channel name must not be empty.")
        Else
            For Each entry As OpenLayers.Base.ChannelListEntry In channelList
                If channelName.Equals(entry.Name, StringComparison.OrdinalIgnoreCase) Then
                    result = entry
                    Exit For
                End If
            Next
        End If
        Return result
    End Function

    ''' <summary> Selects the physical channel. </summary>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    ''' the required range. </exception>
    ''' <param name="channelList">           The channel list. </param>
    ''' <param name="physicalChannelNumber"> The physical channel number. </param>
    ''' <returns> OpenLayers.Base.ChannelListEntry. </returns>
    <Extension()>
    Public Function SelectPhysicalChannel(ByVal channelList As OpenLayers.Base.ChannelList, ByVal physicalChannelNumber As Integer) As OpenLayers.Base.ChannelListEntry
        Dim result As OpenLayers.Base.ChannelListEntry = Nothing
        If channelList Is Nothing Then
            Throw New ArgumentOutOfRangeException("channelList", "Channel list is not defined")
        ElseIf channelList.Count = 0 Then
            Throw New ArgumentOutOfRangeException("channelList", "Channel list is empty")
        Else
            For i As Integer = 0 To channelList.Count - 1
                If channelList(i).PhysicalChannelNumber = physicalChannelNumber Then
                    Return channelList.Item(i)
                End If
            Next
            For Each entry As OpenLayers.Base.ChannelListEntry In channelList
                If entry.PhysicalChannelNumber = physicalChannelNumber Then
                    result = entry
                    Exit For
                End If
            Next
        End If
        Return result
    End Function

#End Region

#Region " DEVICE "

    ''' <summary> Returns true if the device is engaged.  This permits detecting if the device failed
    ''' to engage such as the case after waking up from system sleep. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> Reference to a valid <see cref="OpenLayers.Base.Device">device</see>. </param>
    ''' <returns> <c>True</c> if the specified device is exists; otherwise, <c>False</c>. </returns>
    <Extension()>
    Public Function Engaged(ByVal device As OpenLayers.Base.Device) As Boolean

        If device Is Nothing Then
            Throw New ArgumentNullException("device")
        End If
        Dim hardwareInfo As OpenLayers.Base.HardwareInfo = device.GetHardwareInfo
        Return Not (hardwareInfo.VendorId.Equals(0) Or hardwareInfo.ProductId.Equals(0))

    End Function

    ''' <summary> Returns the <see cref="isr.IO.OL.DeviceFamily">device family</see>. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> The device. </param>
    ''' <returns> isr.IO.OL.DeviceFamily. </returns>
    <Extension()>
    Public Function DeviceFamily(ByVal device As OpenLayers.Base.Device) As isr.IO.OL.DeviceFamily
        If device Is Nothing Then
            Throw New ArgumentNullException("device")
        End If
        ' "DT9816-A" 
        If device.BoardModelName.Substring(0, 4).Equals("DT98", StringComparison.OrdinalIgnoreCase) Then
            Return OL.DeviceFamily.DT9800
        Else
            Return OL.DeviceFamily.None
        End If
    End Function

#End Region

#Region " DEVICE MANAGER "

    ''' <summary> Returns true if the device exists.  This permits detecting if the device failed to
    ''' connect such as the case after waking up from system sleep. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="manager">    The device manager. </param>
    ''' <param name="deviceName"> Specifies the device name. </param>
    ''' <returns> <c>True</c> if a device with the specified name exists; otherwise, <c>False</c>. </returns>
    <Extension()>
    Public Function Exists(ByVal manager As OpenLayers.Base.DeviceMgr, ByVal deviceName As String) As Boolean

        If manager Is Nothing Then
            Throw New ArgumentNullException("manager")
        End If
        If String.IsNullOrWhiteSpace(deviceName) Then
            Throw New ArgumentNullException("deviceName")
        End If

        If manager.HardwareAvailable Then
            Return manager.GetDeviceNames.Contains(deviceName, StringComparer.CurrentCultureIgnoreCase)
        Else
            Return False
        End If

    End Function

    ''' <summary> Fetches the device name. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="manager">    The device manager. </param>
    ''' <param name="deviceName"> Specifies the device name. </param>
    ''' <returns> OpenLayers.Base.HardwareInfo. </returns>
    <Extension()>
    Public Function DeviceHardwareInfo(ByVal manager As OpenLayers.Base.DeviceMgr, ByVal deviceName As String) As OpenLayers.Base.HardwareInfo

        If manager Is Nothing Then
            Throw New ArgumentNullException("manager")
        End If
        If deviceName Is Nothing OrElse deviceName.Length = 0 Then
            Throw New ArgumentNullException("deviceName")
        End If

        If manager.HardwareAvailable Then
            Return manager.SelectDevice(deviceName).GetHardwareInfo
        Else
            Return New OpenLayers.Base.HardwareInfo
        End If

    End Function

    ''' <summary> Fetches the device driver version number for the specified board. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="manager">    The device manager. </param>
    ''' <param name="deviceName"> Specifies the board. </param>
    ''' <returns> Version number for the board.<para>
    ''' The returned string has the format "VM.XY.BB" described as follows:</para><para>
    ''' Code    Meaning Possible Values</para><para>
    ''' V   Release type    V - released software version </para><para>
    ''' M   Major revision number   00 through 99 - major feature change</para><para>
    ''' X   Minor revision number   00 through 99 - minor feature change</para><para>
    ''' Y   Patch revision number   00 through 99 - minor bug fix</para><para>
    ''' BB  Internal build number   00 through 99</para> </returns>
    <Extension()>
    Public Function DeviceDriverVersion(ByVal manager As OpenLayers.Base.DeviceMgr, ByVal deviceName As String) As String

        If manager Is Nothing Then
            Throw New ArgumentNullException("manager")
        End If
        If deviceName Is Nothing OrElse deviceName.Length = 0 Then
            Throw New ArgumentNullException("deviceName")
        End If

        If manager.HardwareAvailable Then
            Return manager.SelectDevice(deviceName).DriverVersion
        Else
            Return String.Empty
        End If

    End Function

    ''' <summary> Fetches the unique id of the specified board. </summary>
    ''' <remarks> This field contains the year (1 or 2 digits), week (1 or 2 digits), test station (1
    ''' digit), and sequence number (3 digits) of the device when it was tested in Manufacturing. For
    ''' example, if BoardId contains the value 5469419, this device was tested in 2005, week 46, on
    ''' test station 9, and is unit number 419. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="manager">    The device manager. </param>
    ''' <param name="deviceName"> Specifies the device name. </param>
    ''' <returns> Int64. </returns>
    <Extension()>
    Public Function DeviceUniqueId(ByVal manager As OpenLayers.Base.DeviceMgr, ByVal deviceName As String) As Int64

        If manager Is Nothing Then
            Throw New ArgumentNullException("manager")
        End If
        If deviceName Is Nothing OrElse deviceName.Length = 0 Then
            Throw New ArgumentNullException("deviceName")
        End If

        If manager.HardwareAvailable Then
            Return Convert.ToInt64(manager.SelectDevice(deviceName).GetHardwareInfo.BoardId)
        Else
            Return 0
        End If

    End Function

    ''' <summary> Selects and returns an open layers device.  If the system has multiple board, this
    ''' would allow the operator to select a board from a list. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="manager"> The device manager. </param>
    ''' <returns> OpenLayers.Base.Device or nothing if no hardware or no such device. </returns>
    <Extension()>
    Public Function SelectDevice(ByVal manager As OpenLayers.Base.DeviceMgr) As OpenLayers.Base.Device

        If manager Is Nothing Then
            Throw New ArgumentNullException("manager")
        End If
        If manager.HardwareAvailable Then
            Return manager.SelectDevice(DeviceChooser.SelectDeviceName())
        Else
            Return Nothing
        End If

    End Function

    ''' <summary> Selects and returns an open layers device.  If the system has multiple board, this
    ''' would allow the operator to select a board from a list. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="manager">    The device manager. </param>
    ''' <param name="deviceName"> Specifies the device name. </param>
    ''' <returns> OpenLayers.Base.Device. </returns>
    <Extension()>
    Public Function SelectDevice(ByVal manager As OpenLayers.Base.DeviceMgr, ByVal deviceName As String) As OpenLayers.Base.Device

        If manager Is Nothing Then
            Throw New ArgumentNullException("manager")
        End If
        If String.IsNullOrWhiteSpace(deviceName) Then
            Throw New ArgumentNullException("deviceName")
        End If
        If manager.Exists(deviceName) Then
            Return manager.GetDevice(deviceName)
        Else
            Return Nothing
        End If

    End Function

#End Region

#Region " RANGE "

    ''' <summary> Compares two ranges. </summary>
    ''' <param name="left">  Specifies the range to compare to. </param>
    ''' <param name="right"> Specifies the range to compare. </param>
    ''' <returns> A Boolean data type. </returns>
    <Extension()>
    Public Function Equals(ByVal left As OpenLayers.Base.Range, ByVal right As OpenLayers.Base.Range) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.Low.Equals(right.Low) AndAlso left.High.Equals(right.High)
        End If
    End Function

    ''' <summary> Compares two ranges. </summary>
    ''' <remarks> The two ranges are the same if their two end points are within the tolerance. </remarks>
    ''' <param name="left">      Specifies the range to compare to. </param>
    ''' <param name="right">     Specifies the range to compare. </param>
    ''' <param name="tolerance"> Specifies the tolerance for comparing the two ranges. </param>
    ''' <returns> A Boolean data type. </returns>
    <Extension()>
    Public Function Equals(ByVal left As OpenLayers.Base.Range, ByVal right As OpenLayers.Base.Range, ByVal tolerance As Double) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return Math.Abs(left.Low - right.Low) <= tolerance AndAlso
                   Math.Abs(left.High - right.High) <= tolerance
        End If
    End Function

#End Region

#Region " SIGNAL LIST "

    ''' <summary> Checks if a signal exists. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="signalList"> The signal list. </param>
    ''' <param name="signalName"> Name of the signal. </param>
    ''' <returns> <c>True</c> if exists, <c>False</c> otherwise. </returns>
    <Extension()>
    Public Function Contains(ByVal signalList As OpenLayers.Signals.MemorySignalList, ByVal signalName As String) As Boolean
        Dim result As Boolean = False
        If signalList Is Nothing Then
            Return False
        ElseIf signalList.Count = 0 Then
            Return False
        ElseIf String.IsNullOrWhiteSpace(signalName) Then
            Throw New ArgumentNullException("signalName", "Signal name must not be empty.")
        Else
            For Each signal As OpenLayers.Signals.MemorySignal In signalList
                result = signalName.Equals(signal.Name, StringComparison.OrdinalIgnoreCase)
                If result Then Exit For
            Next
        End If
        Return result
    End Function

    ''' <summary> Selects a signal by name. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="signalList"> The signal list. </param>
    ''' <param name="signalName"> Name of the signal. </param>
    ''' <returns> OpenLayers.Signals.MemorySignal. </returns>
    <Extension()>
    Public Function SelectNamedSignal(ByVal signalList As OpenLayers.Signals.MemorySignalList,
                                      ByVal signalName As String) As OpenLayers.Signals.MemorySignal
        Dim result As OpenLayers.Signals.MemorySignal = Nothing
        If signalList Is Nothing Then
            Throw New ArgumentNullException("signalList", "Signal list is not defined")
        ElseIf signalList.Count = 0 Then
            Throw New ArgumentNullException("signalList", "Signal list is empty")
        ElseIf String.IsNullOrWhiteSpace(signalName) Then
            Throw New ArgumentNullException("signalName", "Signal name must not be empty.")
        Else
            For Each signal As OpenLayers.Signals.MemorySignal In signalList
                If signalName.Equals(signal.Name, StringComparison.OrdinalIgnoreCase) Then
                    result = signal
                    Exit For
                End If
            Next
        End If
        Return result
    End Function

#End Region

#Region " SUBSYSTEM BASE "

    ''' <summary> Returns the subsystem maximum reading. </summary>
    ''' <param name="subsystem"> A reference to an open
    ''' <see cref="OpenLayers.Base.SubsystemBase">subsystem</see> </param>
    ''' <returns> System.Int32. </returns>
    <Extension()>
    Public Function MaxReading(ByVal subsystem As OpenLayers.Base.SubsystemBase) As Integer
        If subsystem Is Nothing Then
            Throw New ArgumentNullException("subsystem")
        End If
        If subsystem.Encoding = OpenLayers.Base.Encoding.TwosComplement Then
            Return Convert.ToInt32(Math.Pow(2, subsystem.Resolution - 1)) - 1
        Else
            Return Convert.ToInt32(Math.Pow(2, subsystem.Resolution)) - 1
        End If
    End Function

    ''' <summary> Returns the subsystem minimum reading. </summary>
    ''' <param name="subsystem"> A reference to an open
    ''' <see cref="OpenLayers.Base.SubsystemBase">analog I/O subsystem</see> </param>
    ''' <returns> System.Int32. </returns>
    <Extension()>
    Public Function MinReading(ByVal subsystem As OpenLayers.Base.SubsystemBase) As Integer
        If subsystem Is Nothing Then
            Throw New ArgumentNullException("subsystem")
        End If
        If subsystem.Encoding = OpenLayers.Base.Encoding.TwosComplement Then
            Return -Convert.ToInt32(Math.Pow(2, subsystem.Resolution - 1))
        Else
            Return 0
        End If
    End Function

#End Region

End Module

