''' <summary>
''' Adds functionality to the Data Translation Open Layers
''' <see cref="OpenLayers.Base.AnalogOutputSubsystem">Analog Output subsystem</see>.
''' </summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class AnalogOutputSubsystem

    Inherits Global.OpenLayers.Base.AnalogOutputSubsystem

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    ''' <param name="device">A reference to 
    '''     <see cref="OpenLayers.Base.Device">an open layers device</see>.</param>
    ''' <param name="elementNumber">Specifies the subsystem logical element number.</param>
    Public Sub New(ByVal device As OpenLayers.Base.Device, ByVal elementNumber As Integer)

        MyBase.new(device, elementNumber)
        Me._lastSingleReadingsChannel = -1
        Me._lastSingleVoltagesChannel = -1
        Me._lastSingleVoltages = New Collections.Generic.Dictionary(Of Integer, Double)
        Me._lastSingleReadings = New Collections.Generic.Dictionary(Of Integer, Integer)

    End Sub

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; <c>False</c> if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.</remarks>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not MyBase.disposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    Me.HandlesBufferDoneEvents = False

                End If

                ' Free shared unmanaged resources
            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Aborts the subsystem operations.
    ''' </summary>
    Public Overrides Sub Abort()

        MyBase.Abort()

        If Not MyBase.BufferQueue Is Nothing Then
            MyBase.BufferQueue.FreeAllQueuedBuffers()
        End If

    End Sub

    ''' <summary>
    ''' Starts the subsystem operations.
    ''' </summary>
    Public Overrides Sub Start()

        ' get started.
        MyBase.Start()

    End Sub

    ''' <summary>
    ''' Stops the subsystem operations.
    ''' </summary>
    Public Overrides Sub [Stop]()

        MyBase.Stop()

    End Sub

#End Region

#Region " PROPERTIES "

    Private _handlesBufferDoneEvents As Boolean
    ''' <summary>
    ''' Gets or Sets the condition as True have the sub system process buffer done events.
    ''' </summary>
    Public Property HandlesBufferDoneEvents() As Boolean
        Get
            Return Me._handlesBufferDoneEvents
        End Get
        Set(ByVal Value As Boolean)
            If Me._handlesBufferDoneEvents <> Value Then
                If Value Then
                    AddHandler Me.BufferDoneEvent, AddressOf BufferDoneHandler
                Else
                    RemoveHandler Me.BufferDoneEvent, AddressOf BufferDoneHandler
                End If
                Me._handlesBufferDoneEvents = Value
            End If
        End Set
    End Property

    ''' <summary>
    ''' Returns true if the subsystem has channels defined.
    ''' </summary>
    Public ReadOnly Property HasChannels() As Boolean
        Get
            Return MyBase.ChannelList.Count > 0
        End Get
    End Property

    Private _lastSingleVoltagesChannel As Integer
    Private _lastSingleVoltages As Collections.Generic.Dictionary(Of Integer, Double)
    ''' <summary>
    ''' Returns the last voltage set using <see cref="SingleVoltage" />.
    ''' </summary>
    ''' <value>The last single voltage.</value>
    Public Property LastSingleVoltage() As Double
        Get
            Return Me._lastSingleVoltages(Me._lastSingleVoltagesChannel)
        End Get
        Private Set(value As Double)
            If Me._lastSingleVoltages.ContainsKey(Me._lastSingleVoltagesChannel) Then
                Me._lastSingleVoltages.Remove(Me._lastSingleVoltagesChannel)
            End If
            Me._lastSingleVoltages.Add(Me._lastSingleVoltagesChannel, value)
        End Set
    End Property

    Private _lastSingleReadingsChannel As Integer
    Private _lastSingleReadings As Collections.Generic.Dictionary(Of Integer, Integer)
    ''' <summary>
    ''' Returns the last output reading set using <see cref="SingleVoltage" />.
    ''' </summary>
    ''' <value>The last single reading.</value>
    Public Property LastSingleReading() As Integer
        Get
            Return Me._lastSingleReadings(Me._lastSingleReadingsChannel)
        End Get
        Private Set(value As Integer)
            If Me._lastSingleReadings.ContainsKey(Me._lastSingleReadingsChannel) Then
                Me._lastSingleReadings.Remove(Me._lastSingleReadingsChannel)
            End If
            Me._lastSingleVoltages.Add(Me._lastSingleReadingsChannel, value)
        End Set
    End Property

    ''' <summary>
    ''' True if has bipolar range.
    ''' </summary>
    ''' <value><c>True</c> if [bipolar range]; otherwise, <c>False</c>.</value>
    Public ReadOnly Property BipolarRange() As Boolean
        Get
            Return MyBase.VoltageRange.Low < 0
        End Get
    End Property

    Private _bufferLength As Integer
    ''' <summary>
    ''' Gets or sets the length of each buffer allocated.
    ''' </summary>
    ''' <value>The length of the buffer.</value>
    Public Property BufferLength() As Integer
        Get
            Return Me._bufferLength
        End Get
        Set(ByVal value As Integer)
            Me._bufferLength = value
        End Set
    End Property

    ''' <summary>
    ''' Returns the subsystem maximum voltage.
    ''' </summary>
    ''' <value>The max voltage.</value>
    Public ReadOnly Property MaxVoltage() As Double
        Get
            Return Me.MaxVoltage(Me.SelectedChannel)
        End Get
    End Property

    ''' <summary>
    ''' Returns the subsystem maximum voltage.
    ''' </summary>
    ''' <value>The max voltage.</value>
    ''' <exception cref="System.ArgumentNullException">channel</exception>
    ''' <param name="channel">Specifies a reference to a <see cref="OpenLayers.Base.ChannelListEntry">channel</see>.</param>
    Public ReadOnly Property MaxVoltage(ByVal channel As OpenLayers.Base.ChannelListEntry) As Double
        Get
            If channel Is Nothing Then
                Throw New ArgumentNullException("channel")
            End If
            If channel.Gain = 0 Then
                Return 0
            Else
                Return MyBase.VoltageRange.High / channel.Gain
            End If
        End Get
    End Property

    ''' <summary>
    ''' Returns the subsystem minimum voltage.
    ''' </summary>
    ''' <value>The min voltage.</value>
    Public ReadOnly Property MinVoltage() As Double
        Get
            Return Me.MinVoltage(Me.SelectedChannel)
        End Get
    End Property

    ''' <summary>
    ''' Returns the subsystem minimum voltage.
    ''' </summary>
    ''' <value>The min voltage.</value>
    ''' <exception cref="System.ArgumentNullException">channel</exception>
    ''' <param name="channel">Specifies a reference to a <see cref="OpenLayers.Base.ChannelListEntry">channel</see>.</param>
    Public ReadOnly Property MinVoltage(ByVal channel As OpenLayers.Base.ChannelListEntry) As Double
        Get
            If channel Is Nothing Then
                Throw New ArgumentNullException("channel")
            End If
            If channel.Gain = 0 Then
                Return 0
            Else
                Return MyBase.VoltageRange.Low / channel.Gain
            End If
        End Get
    End Property

    ''' <summary>
    ''' Returns a single reading from the selected channel.
    ''' </summary>
    ''' <value>The single reading.</value>
    Public Property SingleReading() As Integer
        Get
            Return Me.LastSingleReading
        End Get
        Set(value As Integer)
            Me.SingleReading(Me.SelectedChannel) = value
        End Set
    End Property

    ''' <summary>
    ''' Returns a single reading from the specified channel.
    ''' </summary>
    ''' <value>The single reading.</value>
    ''' <exception cref="System.ArgumentNullException">channel</exception>
    ''' <param name="channel">Reference to the channel.</param>
    Public Property SingleReading(ByVal channel As OpenLayers.Base.ChannelListEntry) As Integer
        Get
            If channel Is Nothing Then
                Throw New ArgumentNullException("channel")
            End If
            Return Me._lastSingleReadings(channel.PhysicalChannelNumber)
        End Get
        Set(value As Integer)
            If channel Is Nothing Then
                Throw New ArgumentNullException("channel")
            End If
            Me.SingleReading(channel.PhysicalChannelNumber) = value
        End Set
    End Property

    ''' <summary>
    ''' Returns a single value from the sub system.
    ''' </summary>
    ''' <value>The single reading.</value>
    ''' <param name="physicalChannelNumber">Specifies the physical channel number.</param>
    Public Property SingleReading(ByVal physicalChannelNumber As Integer) As Integer
        Get
            Return Me._lastSingleReadings(physicalChannelNumber)
        End Get
        Set(value As Integer)
            Me._lastSingleReadingsChannel = physicalChannelNumber
            Me.LastSingleReading = value
            MyBase.SetSingleValueAsRaw(physicalChannelNumber, value)
        End Set
    End Property

    ''' <summary>
    ''' Returns a single Voltage from the selected channel.
    ''' </summary>
    ''' <value>The single Voltage.</value>
    Public Property SingleVoltage() As Double
        Get
            Return Me.LastSingleVoltage
        End Get
        Set(value As Double)
            Me.SingleVoltage(Me.SelectedChannel) = value
        End Set
    End Property

    ''' <summary>
    ''' Returns a single Voltage from the specified channel.
    ''' </summary>
    ''' <value>The single Voltage.</value>
    ''' <exception cref="System.ArgumentNullException">channel</exception>
    ''' <param name="channel">Reference to the channel.</param>
    Public Property SingleVoltage(ByVal channel As OpenLayers.Base.ChannelListEntry) As Double
        Get
            If channel Is Nothing Then
                Throw New ArgumentNullException("channel")
            End If
            Return Me._lastSingleVoltages(channel.PhysicalChannelNumber)
        End Get
        Set(value As Double)
            If channel Is Nothing Then
                Throw New ArgumentNullException("channel")
            End If
            Me.SingleVoltage(channel.PhysicalChannelNumber) = value
        End Set
    End Property

    ''' <summary>
    ''' Returns a single value from the sub system.
    ''' </summary>
    ''' <value>The single Voltage.</value>
    ''' <param name="physicalChannelNumber">Specifies the physical channel number.</param>
    Public Property SingleVoltage(ByVal physicalChannelNumber As Integer) As Double
        Get
            Return Me._lastSingleVoltages(physicalChannelNumber)
        End Get
        Set(value As Double)
            Me._lastSingleVoltagesChannel = physicalChannelNumber
            Me.LastSingleVoltage = value
            MyBase.SetSingleValueAsVolts(physicalChannelNumber, value)
        End Set
    End Property

    Private _statusMessage As String = String.Empty
    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
    Public ReadOnly Property StatusMessage() As String
        Get
            Return Me._statusMessage
        End Get
    End Property

    ''' <summary>
    ''' Returns the analog input voltage resolution.
    ''' </summary>
    ''' <value>The voltage resolution.</value>
    Public ReadOnly Property VoltageResolution() As Double
        Get
            Return Me.VoltageResolution(Me.SelectedChannel)
        End Get
    End Property

    ''' <summary>
    ''' Returns the analog input voltage resolution.
    ''' </summary>
    ''' <value>The voltage resolution.</value>
    ''' <exception cref="System.ArgumentNullException">channel</exception>
    ''' <param name="channel">Specifies a reference to a <see cref="OpenLayers.Base.ChannelListEntry">channel</see>.</param>
    Public ReadOnly Property VoltageResolution(ByVal channel As OpenLayers.Base.ChannelListEntry) As Double
        Get
            If channel Is Nothing Then
                Throw New ArgumentNullException("channel")
            End If
            Return (Me.MaxVoltage(channel) - Me.MinVoltage(channel)) / MyBase.Resolution
        End Get
    End Property

#End Region

#Region " BUFFERS "

    ''' <summary>
    ''' Allocates a buffer for the subsystem.
    ''' </summary>
    ''' <param name="bufferLength">Length of the buffer.</param>
    ''' <returns>OpenLayers.Base.OlBuffer.</returns>
    Public Function AllocateBuffer(ByVal bufferLength As Integer) As OpenLayers.Base.OlBuffer

        Me.BufferLength = bufferLength
        Return New OpenLayers.Base.OlBuffer(bufferLength, Me)

    End Function

    ''' <summary>
    ''' Allocates a set of buffers for data collection.
    ''' </summary>
    ''' <param name="bufferCount">The buffer count.</param>
    ''' <param name="bufferLength">Length of the buffer.</param>
    ''' <returns>OpenLayers.Base.BufferQueue.</returns>
    Public Function AllocateBuffers(ByVal bufferCount As Integer, ByVal bufferLength As Integer) As OpenLayers.Base.BufferQueue

        MyBase.BufferQueue.FreeAllQueuedBuffers()

        ' allocate queue for data collection
        For i As Integer = 1 To bufferCount
            MyBase.BufferQueue.QueueBuffer(Me.AllocateBuffer(bufferLength))
        Next i

        Return MyBase.BufferQueue

    End Function

    Private _voltages()() As Double = {}
    ''' <summary>
    ''' Returns a read only reference to the voltage array.
    ''' </summary>
    ''' <returns> An array of <see cref="T:System.Double"/> .</returns>
    Public Function Voltages() As Double()()
        Return Me._voltages
    End Function

    Private _readings()() As UInt32 = {}
    ''' <summary>Returns a read only reference to the readings array.</summary>
    Public Function Readings() As UInt32()()
        Return Me._readings
    End Function

#End Region

#Region " CHANNELS "

    ''' <summary>
    ''' Add a set of channels.
    ''' </summary>
    ''' <param name="firstPhysicalChannel">The first physical channel.</param>
    ''' <param name="lastPhysicalChannel">The last physical channel.</param>
    ''' <returns>OpenLayers.Base.ChannelList.</returns>
    Public Function AddChannels(ByVal firstPhysicalChannel As Integer, ByVal lastPhysicalChannel As Integer) As OpenLayers.Base.ChannelList

        MyBase.ChannelList.Clear()

        ' set the channel numbers in the channel list
        For phyicalChannelNumber As Integer = firstPhysicalChannel To lastPhysicalChannel

            ' add the channel to the list
            MyBase.ChannelList.Add(phyicalChannelNumber)

        Next phyicalChannelNumber

        Return MyBase.ChannelList

    End Function

    ''' <summary>
    ''' Add a set of channels.
    ''' </summary>
    ''' <param name="firstPhysicalChannel">The first physical channel.</param>
    ''' <param name="lastPhysicalChannel">The last physical channel.</param>
    ''' <param name="gain">The gain.</param>
    ''' <returns>OpenLayers.Base.ChannelList.</returns>
    Public Function AddChannels(ByVal firstPhysicalChannel As Integer, ByVal lastPhysicalChannel As Integer,
                                ByVal gain As Double) As OpenLayers.Base.ChannelList

        MyBase.ChannelList.Clear()

        ' set the channel numbers in the channel list
        For phyicalChannelNumber As Integer = firstPhysicalChannel To lastPhysicalChannel

            ' add the channel to the list
            MyBase.ChannelList.Add(phyicalChannelNumber).Gain = gain

        Next phyicalChannelNumber

        Return MyBase.ChannelList

    End Function

    Private _selectedChannel As OpenLayers.Base.ChannelListEntry
    ''' <summary>
    ''' Gets or sets reference to the selected channel for this subsystem.
    ''' </summary>
    ''' <value>The selected channel.</value>
    Public ReadOnly Property SelectedChannel() As OpenLayers.Base.ChannelListEntry
        Get
            Return Me._selectedChannel
        End Get
    End Property

    Private _selectedChannelLogicalNumber As Integer

    ''' <summary>
    ''' Gets the logical number (index) of the selected channel.
    ''' </summary>
    ''' <value>The selected channel logical number.</value>
    Public ReadOnly Property SelectedChannelLogicalNumber() As Integer
        Get
            Return Me._selectedChannelLogicalNumber
        End Get
    End Property

    ''' <summary>
    ''' Selects a channel.
    ''' </summary>
    ''' <param name="logicalChannelNumber">The logical channel number (channel index in the <see cref="ChannelList">channel list</see>.)</param>
    ''' <returns>OpenLayers.Base.ChannelListEntry.</returns>
    Public Function SelectLogicalChannel(ByVal logicalChannelNumber As Integer) As OpenLayers.Base.ChannelListEntry
        Me._selectedChannel = MyBase.ChannelList.SelectLogicalChannel(logicalChannelNumber)
        Return Me._selectedChannel
    End Function

    ''' <summary>
    ''' Selects a channel from the subsystem channel list by its physical channel number.
    ''' </summary>
    ''' <param name="physicalChannelNumber">Specifies the physical channel number.</param>
    ''' <returns>OpenLayers.Base.ChannelListEntry.</returns>
    Public Function SelectPhysicalChannel(ByVal physicalChannelNumber As Integer) As OpenLayers.Base.ChannelListEntry
        If Me.PhysicalChannelExists(physicalChannelNumber) Then
            Me._selectedChannelLogicalNumber = MyBase.ChannelList.LogicalChannelNumber(physicalChannelNumber)
            Me._selectedChannel = MyBase.ChannelList.Item(Me._selectedChannelLogicalNumber)
        End If
        Return Me._selectedChannel
    End Function

    ''' <summary> Queries if a given physical channel exists. </summary>
    ''' <param name="physicalChannelNumber"> Specifies the physical channel number. </param>
    ''' <returns> <c>True</c> if it succeeds, false if it fails. </returns>
    Public Function PhysicalChannelExists(ByVal physicalChannelNumber As Integer) As Boolean
        Return MyBase.ChannelList.Contains(physicalChannelNumber)
    End Function

#End Region

#Region " EVENT HANDLERS "

    ''' <summary>
    ''' Handles buffer done events.
    ''' </summary>
    ''' <param name="sender">Specifies reference to the 
    ''' <see cref="OpenLayers.Base.SubsystemBase">subsystem</see></param>
    ''' <param name="e">Specifies the 
    ''' <see cref="OpenLayers.Base.BufferDoneEventArgs">event arguments</see>.</param>
    Private Sub BufferDoneHandler(ByVal sender As Object, ByVal e As OpenLayers.Base.BufferDoneEventArgs)
        OnBufferDone(e)
    End Sub

    ''' <summary>
    ''' Handle buffer done events.
    ''' </summary>
    ''' <param name="e">The <see cref="OpenLayers.Base.BufferDoneEventArgs" /> instance containing the event data.</param>
    Protected Overridable Sub OnBufferDone(ByVal e As OpenLayers.Base.BufferDoneEventArgs)
    End Sub

#End Region

End Class

