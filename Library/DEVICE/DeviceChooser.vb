''' <summary>
''' Displays a form for selecting a device.
''' </summary>
''' <remarks>Launch this form by calling its Show or ShowDialog method from its default
'''   instance.</remarks>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
Public Class DeviceChooser

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly syncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared instance As DeviceChooser

    ''' <summary>
    ''' Instantiates the class.
    ''' </summary>
    ''' <returns>
    ''' A new or existing instance of the class.
    ''' </returns>
    ''' <remarks>
    ''' Use this property to instantiate a single instance of this class.
    ''' This class uses lazy instantiation, meaning the instance isn't 
    ''' created until the first time it's retrieved.
    ''' </remarks>
    Public Shared Function [Get]() As DeviceChooser
        If DeviceChooser.instance Is Nothing OrElse DeviceChooser.instance.IsDisposed Then
            SyncLock DeviceChooser.syncLocker
                DeviceChooser.instance = New DeviceChooser
            End SyncLock
        End If
        Return DeviceChooser.instance
    End Function

#End Region

#Region " SHARD "

    ''' <summary>
    ''' Selects and returns an open layers device name.  If the board name is not
    ''' found, allow the user to select from the list of existing boards..
    ''' </summary>
    ''' <param name="deviceName">Name of the device.</param>
    ''' <returns>System.String.</returns>
    Public Shared Function SelectDeviceName(ByVal deviceName As String) As String

        If OpenLayers.Base.DeviceMgr.Get.HardwareAvailable Then
            Dim boards As String() = OpenLayers.Base.DeviceMgr.Get.GetDeviceNames()
            If OpenLayers.Base.DeviceMgr.Get.GetDeviceNames().Length = 1 Then
                Return OpenLayers.Base.DeviceMgr.Get.GetDeviceNames()(0)
            Else
                Dim index As Integer = Array.IndexOf(Of String)(boards, deviceName)
                If index >= 0 Then
                    Return boards(index)
                Else
                    If DeviceChooser.[Get].ShowDialog() = Windows.Forms.DialogResult.OK Then
                        Return DeviceChooser.[Get].DeviceName
                    Else
                        Return String.Empty
                    End If
                End If
            End If
        Else
            Return String.Empty
        End If

    End Function

    ''' <summary>
    ''' Selects and returns an open layers device name.  If the system has multiple
    ''' boards, this would allow the operator to select a board from a list.
    ''' </summary>
    ''' <returns>System.String.</returns>
    Public Shared Function SelectDeviceName() As String

        If OpenLayers.Base.DeviceMgr.Get.HardwareAvailable Then
            If OpenLayers.Base.DeviceMgr.Get.GetDeviceNames().Length = 1 Then
                Return OpenLayers.Base.DeviceMgr.Get.GetDeviceNames()(0)
            Else
                If DeviceChooser.[Get].ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Return DeviceChooser.[Get].DeviceName
                Else
                    Return String.Empty
                End If
            End If
        Else
            Return String.Empty
        End If

    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Returns the selected device name.
    ''' </summary>
    ''' <value>The name of the device.</value>
    Public ReadOnly Property DeviceName() As String
        Get
            Return Me._DeviceNameChooser.DeviceName
        End Get
    End Property

#End Region

#Region " FORM AND CONTROL EVENT HANDLERS "

    ''' <summary>
    ''' Accepts changes and exits this form.
    ''' </summary>
    Private Sub okButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AcceptButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' Cancels changes and exits this form.
    ''' </summary>
    Private Sub cancelButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
    ''' </summary>
    ''' <param name="e">A <see cref="T:System.EventArgs" /> that contains the event data.</param>
    Protected Overrides Sub OnShown(e As System.EventArgs)
        MyBase.OnShown(e)
        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor


        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Handles the Load event of the form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' center the form
            Me.CenterToScreen()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Handles the Shown event of the form control.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub form_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' Initialize and set the user interface
            Me._DeviceNameChooser.RefreshDeviceNamesList()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " CONTROL EVENTS "

    ''' <summary>
    ''' Handles the DeviceSelected event of the DeviceNameChooser1 control.
    ''' Enables acceptance exist.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub _DeviceNameChooser_DeviceSelected(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _DeviceNameChooser.DeviceSelected
        Me._AcceptButton.Enabled = Me._DeviceNameChooser.DeviceName.Length > 0
    End Sub

#End Region

End Class