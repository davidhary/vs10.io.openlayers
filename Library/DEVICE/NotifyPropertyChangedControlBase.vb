﻿Imports System.Drawing
Imports System.ComponentModel
Imports System.Windows.Forms

''' <summary> A base control implementing property notifications. Useful for a settings publisher. </summary>
''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="11/02/2010" by="David" revision="1.2.3988.x"> created. </history>
Public Class NotifyPropertyChangedControlBase
    Inherits System.Windows.Forms.UserControl
    Implements System.ComponentModel.INotifyPropertyChanged

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> A private constructor for this class making it not publicly creatable. This ensure
    ''' using the class as a singleton. </summary>
    Protected Sub New()
        MyBase.new()
        Me.InitializeComponent()
    End Sub

    ''' <summary> Executes the dispose managed resources action. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub onDisposeManagedResources()
        If Me.PropertyChangedEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.PropertyChangedEvent.GetInvocationList
                Try
                    RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
                Catch
                End Try
            Next
        End If
    End Sub

#Region " Windows Form Designer generated code "

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me.onDisposeManagedResources()
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'MyUserControlBase
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "MyUserControlBase"
        Me.Size = New System.Drawing.Size(175, 173)
        Me.ResumeLayout(False)

    End Sub

#End Region

#End Region

#Region " I NOTIFY PROPERTY CHANGED "

    ''' <summary> Occurs when a property value changes. </summary>
    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Raised to notify the bindable object of a change. </summary>
    ''' <remarks> Includes a work around because for some reason, the binding write value does not
    ''' occur. This is dangerous because it could lead to a stack overflow. It can be used if the
    ''' property changed event is raised only if the property changed. </remarks>
    ''' <param name="name"> The name. </param>
    Protected Overridable Sub OnPropertyChanged(ByVal name As String)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        If evt IsNot Nothing Then evt.Invoke(Me, New PropertyChangedEventArgs(name))
    End Sub

    ''' <summary>
    ''' Called when property changed from the property get context.
    ''' </summary>
    ''' <param name="name">The name.</param>
    ''' <remarks>Strips the "set_" prefix derived from using reflection to
    ''' get the current function name from a property set construct.</remarks>
    Protected Sub OnGetPropertyChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.OnPropertyChanged(name.TrimStart("get_".ToCharArray()))
        End If
    End Sub

    ''' <summary>
    ''' Called when property changed from the property set context.
    ''' </summary>
    ''' <param name="name">The name.</param>
    ''' <remarks>Strips the "set_" prefix derived from using reflection to
    ''' get the current function name from a property set construct.</remarks>
    Protected Sub OnSetPropertyChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.OnPropertyChanged(name.TrimStart("set_".ToCharArray()))
        End If
    End Sub

#End Region

#Region " TOOL TIP WORK AROUND "

    ''' <summary> Sets the Tool tip for all form controls that inherit a <see cref="Control">control base.</see> </summary>
    ''' <param name="parent">  Reference to the parent form or control. </param>
    ''' <param name="toolTip"> The parent form or control tool tip. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")>
    Public Shared Sub ToolTipSetter(ByVal parent As Windows.Forms.Control, ByVal toolTip As ToolTip)
        If parent Is Nothing Then Return
        If toolTip Is Nothing Then Return
        If TypeOf parent Is NotifyPropertyChangedControlBase Then
            CType(parent, NotifyPropertyChangedControlBase).ToolTipSetter(toolTip)
        ElseIf parent.HasChildren Then
            For Each control As Windows.Forms.Control In parent.Controls
                ToolTipSetter(control, toolTip)
            Next
        End If
    End Sub

    ''' <summary> Sets a tool tip for all controls on the user control. Uses the message already set
    ''' for this control. </summary>
    ''' <remarks> This is required because setting a tool tip from the parent form does not show the
    ''' tool tip if hovering above children controls hosted by the user control. </remarks>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    Public Sub ToolTipSetter(ByVal toolTip As ToolTip)
        If toolTip IsNot Nothing Then
            applyToolTipToChildControls(Me, toolTip, toolTip.GetToolTip(Me))
        End If
    End Sub

    ''' <summary> Sets a tool tip for all controls on the user control. </summary>
    ''' <remarks> This is required because setting a tool tip from the parent form does not show the
    ''' tool tip if hovering above children controls hosted by the user control. </remarks>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    ''' <param name="message"> The tool tip message to apply to all the children controls and their
    ''' children. </param>
    Public Sub ToolTipSetter(ByVal toolTip As ToolTip, ByVal message As String)
        applyToolTipToChildControls(Me, toolTip, message)
    End Sub

    ''' <summary> Applies the tool tip to all control hosted by the parent as well as all the children
    ''' with these control. </summary>
    ''' <param name="parent">  The parent control. </param>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    ''' <param name="message"> The tool tip message to apply to all the children controls and their
    ''' children. </param>
    Private Sub applyToolTipToChildControls(ByVal parent As Control, ByVal toolTip As ToolTip, ByVal message As String)
        For Each control As Control In parent.Controls
            toolTip.SetToolTip(control, message)
            If parent.HasChildren Then
                applyToolTipToChildControls(control, toolTip, message)
            End If
        Next
    End Sub

#End Region

End Class
