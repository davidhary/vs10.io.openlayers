''' <summary>
''' Selects a device name.
''' </summary>
''' <license>
''' (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
<Description("Open Layers Device Name Chooser Control"), DefaultEvent("DeviceSelected"), ToolboxBitmap("DeviceNameChooser.bmp")>
Public Class DeviceNameChooser

#Region " METHODS, PROPERTIES, EVENTS "

    ''' <summary>
    ''' Returns the selected device name.
    ''' </summary>
    ''' <value>The name of the device.</value>
    Public ReadOnly Property DeviceName() As String
        Get
            Return Me._NamesComboBox.Text
        End Get
    End Property

    ''' <summary>
    ''' Updates the device name list and raises the device located event if devices where found.
    ''' </summary>
    ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
    Public Sub RefreshDeviceNamesList()
        If OpenLayers.Base.DeviceMgr.Get.HardwareAvailable Then
            Me._NamesComboBox.DataSource = OpenLayers.Base.DeviceMgr.Get.GetDeviceNames()
            Me.onDevicesLocated(New EventArgs)
        Else
            Me._NamesComboBox.DataSource = New String() {}
        End If
    End Sub

#End Region

#Region " EVENTS "

    ''' <summary>
    ''' Notifies of the location of device.
    ''' </summary>
    Public Event DevicesLocated As EventHandler(Of EventArgs)

    ''' <summary>
    ''' Raises the device located event.
    ''' </summary>
    ''' <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    Private Sub onDevicesLocated(ByVal e As EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.DevicesLocatedEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

    ''' <summary>
    ''' Notifies of the selected of a device.
    ''' </summary>
    Public Event DeviceSelected As EventHandler(Of EventArgs)

    ''' <summary>
    ''' Raises the Device Selected event.
    ''' </summary>
    ''' <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    Protected Sub OnDeviceNameSelected(ByVal e As EventArgs)
        Dim evt As EventHandler(Of EventArgs) = Me.DeviceSelectedEvent
        If evt IsNot Nothing Then evt.Invoke(Me, e)
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>
    ''' Handles the SelectedIndexChanged event of the availableDeviceNamesComboBox control.
    ''' Selects a device.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub availableDeviceNamesComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _NamesComboBox.SelectedIndexChanged

        If Not Me._NamesComboBox.SelectedItem Is Nothing Then
            Me._ToolTip.SetToolTip(Me._NamesComboBox, Me._NamesComboBox.SelectedItem.ToString)
            OnDeviceNameSelected(New EventArgs)
        End If

    End Sub

    ''' <summary>
    ''' Handles the Click event of the findButton control.
    ''' Looks for devices.
    ''' </summary>
    ''' <param name="sender">The source of the event.</param>
    ''' <param name="e">The <see cref="System.EventArgs" /> instance containing the event data.</param>
    Private Sub findButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _FindButton.Click
        Me.RefreshDeviceNamesList()
    End Sub


#End Region

End Class
