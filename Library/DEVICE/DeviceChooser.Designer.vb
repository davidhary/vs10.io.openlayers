<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DeviceChooser
    Inherits FormBase

    ''' <summary>
    ''' Prevents a default instance of the <see cref="DeviceChooser" /> class from being created.
    ''' </summary>
    Private Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        'onInitialize()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        'onInstantiate()

    End Sub

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._DeviceNameChooser = New isr.IO.OL.DeviceNameChooser()
        Me._CancelButton = New System.Windows.Forms.Button()
        Me._AcceptButton = New System.Windows.Forms.Button()
        Me._DeviceNameChooserLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        '_DeviceNameChooser
        '
        Me._DeviceNameChooser.Location = New System.Drawing.Point(12, 22)
        Me._DeviceNameChooser.Name = "_DeviceNameChooser"
        Me._DeviceNameChooser.Size = New System.Drawing.Size(166, 29)
        Me._DeviceNameChooser.TabIndex = 28
        '
        '_CancelButton
        '
        Me._CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._CancelButton.Font = New System.Drawing.Font(Me.Font, FontStyle.Bold)
        Me._CancelButton.Location = New System.Drawing.Point(14, 52)
        Me._CancelButton.Name = "_CancelButton"
        Me._CancelButton.Size = New System.Drawing.Size(75, 23)
        Me._CancelButton.TabIndex = 27
        Me._CancelButton.Text = "&Cancel"
        '
        '_AcceptButton
        '
        Me._AcceptButton.Enabled = False
        Me._AcceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._AcceptButton.Font = New System.Drawing.Font(Me.Font, FontStyle.Bold)
        Me._AcceptButton.Location = New System.Drawing.Point(102, 52)
        Me._AcceptButton.Name = "_AcceptButton"
        Me._AcceptButton.Size = New System.Drawing.Size(75, 23)
        Me._AcceptButton.TabIndex = 26
        Me._AcceptButton.Text = "&OK"
        '
        '_DeviceNameChooserLabel
        '
        Me._DeviceNameChooserLabel.AutoSize = True
        Me._DeviceNameChooserLabel.BackColor = System.Drawing.SystemColors.Control
        Me._DeviceNameChooserLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._DeviceNameChooserLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._DeviceNameChooserLabel.ForeColor = System.Drawing.SystemColors.WindowText
        Me._DeviceNameChooserLabel.Location = New System.Drawing.Point(19, 3)
        Me._DeviceNameChooserLabel.Name = "_DeviceNameChooserLabel"
        Me._DeviceNameChooserLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._DeviceNameChooserLabel.Size = New System.Drawing.Size(152, 17)
        Me._DeviceNameChooserLabel.TabIndex = 25
        Me._DeviceNameChooserLabel.Text = "Select a Device by Name"
        Me._DeviceNameChooserLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'DeviceChooser
        '
        Me.AcceptButton = Me._AcceptButton
        Me.CancelButton = Me._CancelButton
        Me.ClientSize = New System.Drawing.Size(192, 81)
        Me.Controls.Add(Me._DeviceNameChooser)
        Me.Controls.Add(Me._CancelButton)
        Me.Controls.Add(Me._AcceptButton)
        Me.Controls.Add(Me._DeviceNameChooserLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "DeviceChooser"
        Me.Text = "Select a board"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _DeviceNameChooser As isr.IO.OL.DeviceNameChooser
    Private WithEvents _CancelButton As System.Windows.Forms.Button
    Private WithEvents _AcceptButton As System.Windows.Forms.Button
    Private WithEvents _DeviceNameChooserLabel As System.Windows.Forms.Label
End Class
