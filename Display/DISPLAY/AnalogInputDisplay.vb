Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Drawing
Imports isr.Core.Primitives.StringBuilderExtensions
Imports isr.Core.Controls.ControlExtensions
Imports isr.Core.Controls.CheckBoxExtensions
Imports isr.Core.Controls.NumericUpDownExtensions
Imports isr.Core.Controls.RadioButtonExtensions
Imports isr.Core.Controls.ToolStripExtensions
Imports isr.IO.OL

''' <summary> The new chart control based on the Open Layers
''' <see cref="OpenLayers.Controls.Display">display</see>
''' class. </summary>
''' <license> (c) 2007 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
<Description("Analog Input Display Control"), ToolboxBitmap(GetType(isr.IO.OL.AnalogInputDisplay), "AnalogInputDisplay.bmp")>
Public Class AnalogInputDisplay

#Region " WRAPPER BASE CLASS "
#If False Then
    ' Designing requires changing the condition to True.
    Inherits isr.Core.Diagnosis.TracePublisherControlBaseWrapper
#Else
    Inherits isr.Core.Diagnosis.TracePublisherControlBase
#End If
#End Region

#Region " CONSTRUCTORS AND DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="AnalogInputDisplay" /> class. </summary>
    Public Sub New()

        MyBase.New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        Me._StopTimeout = TimeSpan.FromMilliseconds(1000)
        Me.configureSuspensionQueue = New Generic.Queue(Of Boolean)
        Me._BroadcastLevel = TraceEventType.Warning
        Me._RefreshRateNumeric.SafeMaximumSetter(100)
        Me._RefreshRateNumeric.SafeMinimumSetter(1)
        Me._BuffersCountNumeric.SafeMaximumSetter(1000)
        Me._BuffersCountNumeric.SafeMinimumSetter(2)
        Me._MovingAverageLengthNumeric.SafeMinimumSetter(2)
        Me._SampleSizeNumeric.SafeMinimumSetter(2)
        Me._SampleSizeNumeric.SafeValueSetter(1000)
        Me._SignalBufferSizeNumeric.SafeMinimumSetter(2)
        Me._usbBufferSize = 256
        Me._voltsReadingsStringFormat = "{0:0.000 V}"
        Me._voltsReadingFormat = "0.000"
        Me._CalculatePeriodCount = 10
        Me._UpdatePeriodCount = 10


        With Me._SignalsListView
            ' allow single selections
            .MultiSelect = False

            ' Set the view to show details.
            .View = View.Details

            ' Do not Allow the user to edit item text.
            .LabelEdit = False

            ' Do not Allow the user to rearrange columns.
            .AllowColumnReorder = False

            ' Do Not Display check boxes.
            .CheckBoxes = False

            ' Select the item and sub items when selection is made.
            .FullRowSelect = True

            ' Display grid lines.
            .GridLines = True

            ' Do not Sort the items in the list in ascending order.
            .Sorting = SortOrder.None

            ' Create columns for the items and sub items.
            .Columns.Clear()
            .Columns.Add("#", -2, HorizontalAlignment.Left)
            .Columns.Add("Name", -2, HorizontalAlignment.Left)
            .Columns.Add("Range", -2, HorizontalAlignment.Left)
            .Invalidate()
        End With

        With Me._ChannelsListView
            ' allow single selections
            .MultiSelect = False

            ' Set the view to show details.
            .View = View.Details

            ' Do not Allow the user to edit item text.
            .LabelEdit = False

            ' Do not Allow the user to rearrange columns.
            .AllowColumnReorder = False

            ' Do Not Display check boxes.
            .CheckBoxes = False

            ' Select the item and sub items when selection is made.
            .FullRowSelect = True

            ' Display grid lines.
            .GridLines = True

            ' Do not Sort the items in the list in ascending order.
            .Sorting = SortOrder.None

            ' Create columns for the items and sub items.
            .Columns.Clear()
            .Columns.Add("#", -2, HorizontalAlignment.Left)
            .Columns.Add("Name", -2, HorizontalAlignment.Left)
            .Columns.Add("Range", -2, HorizontalAlignment.Left)
            .Invalidate()
        End With

        ' enable selecting a device.
        Me._DeviceNameChooser.Enabled = Not Me.IsOpen

        '    Me.BuffersCount = Me.BuffersCount

        ' disable controls.
        Me.enableChartControls()
        Me.enableSignalsControls()
        Me.enableChannelsControls()

    End Sub

    ''' <summary> Cleans up managed components. </summary>
    ''' <remarks> Use this method to reclaim managed resources used by this class. </remarks>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub onDisposeManagedResources()

        If Me.AcquisitionStartedEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.AcquisitionStartedEvent.GetInvocationList
                RemoveHandler Me.AcquisitionStarted, CType(d, Global.System.EventHandler(Of isr.Core.Diagnosis.TraceMessageEventArgs))
            Next
        End If

        If Me.AcquisitionStoppedEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.AcquisitionStoppedEvent.GetInvocationList
                RemoveHandler Me.AcquisitionStopped, CType(d, Global.System.EventHandler(Of isr.Core.Diagnosis.TraceMessageEventArgs))
            Next
        End If

        If Me.BoardsLocatedEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.BoardsLocatedEvent.GetInvocationList
                RemoveHandler Me.BoardsLocated, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
            Next
        End If

        If Me.BufferDoneEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.BufferDoneEvent.GetInvocationList
                RemoveHandler Me.BufferDone, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
            Next
        End If

        If Me.DisconnectedEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.DisconnectedEvent.GetInvocationList
                RemoveHandler Me.Disconnected, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
            Next
        End If

        If Me.UpdatePeriodDoneEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.UpdatePeriodDoneEvent.GetInvocationList
                RemoveHandler Me.UpdatePeriodDone, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
            Next
        End If

        If Me.DriverErrorEvent IsNot Nothing Then
            For Each d As [Delegate] In Me.DriverErrorEvent.GetInvocationList
                RemoveHandler Me.DriverError, CType(d, Global.System.EventHandler(Of isr.Core.Diagnosis.TraceMessageEventArgs))
            Next
        End If

        ' stop data acquisition and close the device.
        Try
            If Me.AnalogInput IsNot Nothing Then
                Me.TryReleaseAnalogInput()
                Me._analogInput.Dispose()
                Me._analogInput = Nothing
            End If
        Catch
        End Try

        Try
            If Me._bufferQueue IsNot Nothing Then
                Me._bufferQueue.Clear()
                Me._bufferQueue = Nothing
            End If
        Catch
        End Try

        Try
            If Me._Device IsNot Nothing Then
                Me._Device.Dispose()
                Me._Device = Nothing
            End If
        Catch
        End Try

    End Sub

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As System.EventArgs)
        MyBase.OnLoad(e)
        If String.IsNullOrWhiteSpace(Me.ChartTitle) Then
            Me.ChartTitle = "Analog Input Time Series"
        Else
            ' set default values.  For some reason the text values do not get set upon starting.
            Me.ChartTitle = Me.ChartTitle
        End If
        Me.ChartFooter = Me.ChartFooter
    End Sub

#End Region

#Region " STATE TRANSITION CONTROL  "

    ''' <summary> Gets the stop or abort timeout. </summary>
    ''' <value> The timeout. </value>
    Public Property StopTimeout As TimeSpan

    ''' <summary> Enumerates the state transition commands. </summary>
    Private Enum TransitionCommand
        ''' <summary> An Enum constant representing the none option. </summary>
        None
        ''' <summary> An Enum constant representing the open] option. </summary>
        [Open]
        ''' <summary> An Enum constant representing the configure option. </summary>
        Configure
        ''' <summary> An Enum constant representing the start] option. </summary>
        [Start]
        ''' <summary> An Enum constant representing the stop] option. </summary>
        [Stop]
        ''' <summary> An Enum constant representing the abort option. </summary>
        Abort
        ''' <summary> An Enum constant representing the close option. </summary>
        Close
    End Enum

    ''' <summary> Returns the analog input subsystem state. Returns the
    ''' <see cref="Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue">state </see>
    ''' as a dummy output to indicate that the analog input is not defined. </summary>
    ''' <value> The analog input state. </value>
    Public ReadOnly Property AnalogInputState() As Global.OpenLayers.Base.SubsystemBase.States
        Get
            If Me.AnalogInput Is Nothing Then
                Return Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue
            Else
                Return Me.AnalogInput.State
            End If
        End Get
    End Property

    ''' <summary> Report an illegal transition. </summary>
    ''' <param name="transitionCommand"> The transition command. </param>
    ''' <param name="state">             The state. </param>
    Private Sub onIllegalTransition(ByVal transitionCommand As TransitionCommand, ByVal state As Global.OpenLayers.Base.SubsystemBase.States)
        Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                   "Illegal transition command {0} issued when in the {1} state--Ignored;. ",
                                   transitionCommand, state)
    End Sub

    ''' <summary> Report an illegal state. </summary>
    ''' <param name="state"> The state. </param>
    Private Sub onIllegalState(ByVal state As Global.OpenLayers.Base.SubsystemBase.States)

        Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Illegal state {0};. ", state)

    End Sub

    ''' <summary> Report an unhandled transition request. </summary>
    ''' <param name="transitionCommand"> The transition command. </param>
    ''' <param name="state">             The state. </param>
    Private Sub onUnhandledTransition(ByVal transitionCommand As TransitionCommand, ByVal state As Global.OpenLayers.Base.SubsystemBase.States)
        Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Unhandled transition command {0} issued when in the {1} state--ignored;. ",
                                   transitionCommand, state)
    End Sub

    ''' <summary> Report an Unnecessary transition. </summary>
    ''' <param name="transitionCommand"> The transition command. </param>
    ''' <param name="state">             The state. </param>
    Private Sub onUnnecessaryTransition(ByVal transitionCommand As TransitionCommand, ByVal state As Global.OpenLayers.Base.SubsystemBase.States)
        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Unnecessary transition command {0};. issued when in the {1} state--ignored.",
                                   transitionCommand, state)

    End Sub

    ''' <summary> Returns true if the analog input is in continuous operation running state. </summary>
    ''' <value> The continuous operation running. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ContinuousOperationRunning() As Boolean
        Get
            Return Me.IsOpen AndAlso Me.AnalogInput.ContinuousOperationRunning
        End Get
    End Property

    ''' <summary> Returns true if the analog input is in continuous operation. </summary>
    ''' <value> The continuous operation active. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ContinuousOperationActive() As Boolean
        Get
            Return Me.IsOpen AndAlso Me.AnalogInput.ContinuousOperationActive
        End Get
    End Property

    ''' <summary> Stops a continuous operation on the subsystem immediately without waiting for the
    ''' current buffer to be filled. </summary>
    Public Sub Abort()

        Dim command As TransitionCommand = TransitionCommand.Abort
        Dim state As Global.OpenLayers.Base.SubsystemBase.States = Me.AnalogInputState

        Select Case state

            Case Global.OpenLayers.Base.SubsystemBase.States.Aborting

                Me.onUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous

                ' this is the stopped state.
                Me.onUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue

                ' this is the closed state
                Me.onIllegalTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.Initialized

                ' the board was not stated
                Me.onUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.IoComplete,
                 Global.OpenLayers.Base.SubsystemBase.States.PreStarted,
                 Global.OpenLayers.Base.SubsystemBase.States.Running

                ' The final analog output sample has been written from the FIFO on the device. 
                ' The subsystem was pre-started for a continuous operation. 
                Me.tryStopContinuousInput(Me.StopTimeout, False)

            Case Global.OpenLayers.Base.SubsystemBase.States.Stopping

                ' already aborting.
                Me.onUnnecessaryTransition(command, state)

            Case Else

                Me.onUnhandledTransition(command, state)

        End Select

    End Sub

    ''' <summary> Sends a signal to close the analog input and release the selected board. </summary>
    Public Sub [Close]()

        Dim command As TransitionCommand = TransitionCommand.Close
        Dim state As Global.OpenLayers.Base.SubsystemBase.States = Me.AnalogInputState
        Select Case state

            Case Global.OpenLayers.Base.SubsystemBase.States.Aborting

                ' this will await for aborting and then close.
                Me.tryCloseAnalogInput()

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous

                ' this is the stopped state.
                Me.tryCloseAnalogInput()

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue

                ' this is the closed state.
                Me.onUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.Initialized

                ' the board is connected but not yet configured.
                Me.tryCloseAnalogInput()

            Case Global.OpenLayers.Base.SubsystemBase.States.IoComplete,
                 Global.OpenLayers.Base.SubsystemBase.States.PreStarted,
                 Global.OpenLayers.Base.SubsystemBase.States.Running

                ' The final analog output sample has been written from the FIFO on the device. 
                ' The subsystem was pre-started for a continuous operation. 

                ' stop running.
                Me.Stop()

                ' close
                Me.tryCloseAnalogInput()

            Case Global.OpenLayers.Base.SubsystemBase.States.Stopping

                ' this will await for stopping and then close.
                Me.tryCloseAnalogInput()

            Case Else

                Me.onUnhandledTransition(command, state)

        End Select

    End Sub

    ''' <summary> Configures the board for data collection. </summary>
    ''' <param name="details"> [in,out] A non-empty validation outcome string containing the reason
    ''' configuration failed. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="0#")>
    Public Function TryConfigure(ByRef details As String) As Boolean

        Dim command As TransitionCommand = TransitionCommand.Configure
        Dim state As Global.OpenLayers.Base.SubsystemBase.States = Me.AnalogInputState
        Select Case state

            Case Global.OpenLayers.Base.SubsystemBase.States.Aborting

                ' must wait for aborting to complete.
                Me.onIllegalTransition(command, state)
                details = "Illegal Transition"
                Return False

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous

                ' the analog input can be reconfigured.
                Me.StartStopEnabled = Me._TryConfigure(details)
                Return Me.StartStopEnabled

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue

                ' this is the closed state. need to open first.
                Me.onIllegalTransition(command, state)
                details = "Illegal Transition"
                Return False

            Case Global.OpenLayers.Base.SubsystemBase.States.Initialized

                ' this is the first configuration after opening the board.
                Me.StartStopEnabled = Me._TryConfigure(details)
                Return Me.StartStopEnabled

            Case Global.OpenLayers.Base.SubsystemBase.States.IoComplete,
                 Global.OpenLayers.Base.SubsystemBase.States.PreStarted,
                 Global.OpenLayers.Base.SubsystemBase.States.Running

                ' The final analog output sample has been written from the FIFO on the device. 
                ' The subsystem was pre-started for a continuous operation. 
                Me.onIllegalTransition(command, state)
                details = "Illegal Transition"
                Return False

            Case Global.OpenLayers.Base.SubsystemBase.States.Stopping

                ' must wait for stopping to complete.
                Me.onIllegalTransition(command, state)
                details = "Illegal Transition"
                Return False

            Case Else

                Me.onUnhandledTransition(command, state)
                details = "Unhandled Transition"
                Return False

        End Select

    End Function

    ''' <summary> Sends a signal to select a board and open the analog input. </summary>
    ''' <param name="deviceName"> Name of the device. </param>
    Public Sub [Open](ByVal deviceName As String)

        Me.candidateDeviceName = deviceName
        Dim command As TransitionCommand = TransitionCommand.Open
        Dim state As Global.OpenLayers.Base.SubsystemBase.States = Me.AnalogInputState
        Select Case state

            Case Global.OpenLayers.Base.SubsystemBase.States.Aborting

                ' Already open
                Me.onIllegalTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous

                ' this is the stopped state. Already open
                Me.onIllegalTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue

                ' this is the closed state
                Me.tryOpenAnalogInput()

            Case Global.OpenLayers.Base.SubsystemBase.States.Initialized

                ' board is already open.
                Me.onUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.IoComplete,
                 Global.OpenLayers.Base.SubsystemBase.States.PreStarted,
                 Global.OpenLayers.Base.SubsystemBase.States.Running

                ' The final analog output sample has been written from the FIFO on the device. 
                ' The subsystem was pre-started for a continuous operation. 
                ' Already open
                Me.onIllegalTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.Stopping

                ' Already open
                Me.onIllegalTransition(command, state)

            Case Else

                Me.onUnhandledTransition(command, state)

        End Select

    End Sub

    ''' <summary> Starts data collection. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is
    ''' invalid. </exception>
    Public Sub [Start]()

        If Me.ConfigurationRequired Then
            Throw New Global.System.InvalidOperationException("Configuration required")
        Else
            Dim command As TransitionCommand = TransitionCommand.Start
            Dim state As Global.OpenLayers.Base.SubsystemBase.States = Me.AnalogInputState
            Select Case state

                Case Global.OpenLayers.Base.SubsystemBase.States.Aborting

                    Me.tryStartContinuousInput()

                Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous

                    ' this is the stopped state.
                    Me.tryStartContinuousInput()

                Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue

                    ' this is the closed state. need to open first.
                    Me.onIllegalTransition(command, state)

                Case Global.OpenLayers.Base.SubsystemBase.States.Initialized

                    ' illegal -- board not configured yet.
                    Me.onIllegalTransition(command, state)

                Case Global.OpenLayers.Base.SubsystemBase.States.IoComplete,
                     Global.OpenLayers.Base.SubsystemBase.States.PreStarted,
                     Global.OpenLayers.Base.SubsystemBase.States.Running

                    ' The final analog output sample has been written from the FIFO on the device. 
                    ' The subsystem was pre-started for a continuous operation. 
                    Me.onUnnecessaryTransition(command, state)

                Case Global.OpenLayers.Base.SubsystemBase.States.Stopping

                    Me.tryStartContinuousInput()

                Case Else

                    Me.onUnhandledTransition(command, state)

            End Select

        End If

    End Sub

    ''' <summary> Gets or sets the started status.  This is reflected in the status of the Start/Stop
    ''' control. </summary>
    ''' <value> The started. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Started() As Boolean
        Get
            Return Me._StartStopToolStripMenuItem.Checked
        End Get
        Set(ByVal value As Boolean)
            Me.StartStopEnabled = False
            Me._StartStopToolStripMenuItem.SafeCheckedSetter(value)
            Me._StartStopToolStripMenuItem.Invalidate()
            ' enable or disable all chart controls as necessary
            Me.enableChartControls()
        End Set
    End Property

    ''' <summary> Gets or sets the start stop enable state. </summary>
    ''' <value> The start stop enabled. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property StartStopEnabled() As Boolean

        Get
            Return Me._StartStopToolStripMenuItem.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me._StartStopToolStripMenuItem.SafeEnabledSetter(value)
        End Set
    End Property

    ''' <summary> Stops Running. </summary>
    Public Sub [Stop]()

        Dim command As TransitionCommand = TransitionCommand.Stop
        Dim state As Global.OpenLayers.Base.SubsystemBase.States = Me.AnalogInputState
        Select Case state

            Case Global.OpenLayers.Base.SubsystemBase.States.Aborting

                ' already stopping.
                Me.onUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous

                ' this is the stopped state. already stopped.
                Me.onUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.ConfiguredForSingleValue

                ' this is the closed state. Not open.
                Me.onIllegalTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.Initialized

                ' the board was not started.
                Me.onUnnecessaryTransition(command, state)

            Case Global.OpenLayers.Base.SubsystemBase.States.IoComplete,
                 Global.OpenLayers.Base.SubsystemBase.States.PreStarted,
                 Global.OpenLayers.Base.SubsystemBase.States.Running

                ' The final analog output sample has been written from the FIFO on the device. 
                ' The subsystem was pre-started for a continuous operation. 
                Me.tryStopContinuousInput(Me.StopTimeout, False)

            Case Global.OpenLayers.Base.SubsystemBase.States.Stopping

                ' already stopping.
                Me.onUnnecessaryTransition(command, state)

            Case Else

                Me.onUnhandledTransition(command, state)

        End Select

    End Sub

#End Region

#Region " CONFIGURATION/OPERATION COMMANDS "

    ''' <summary> Restores default settings. </summary>
    Public Sub RestoreDefaults()
        Me.SuspendConfig()
        If Me.ChartMode = ChartModeId.Scope Then
            Me.BuffersCount = 5
            Me.SampleSize = 1000
            Me.SamplingRate = 1000
        ElseIf Me.ChartMode = ChartModeId.StripChart Then
            Me.BuffersCount = 20
            Me.SampleSize = 2
            Me.SamplingRate = 240
            Me.SignalBufferSize = 240
        End If
        Me.CalculateEnabled = False
        Me.CalculatePeriodCount = 10
        Me.ChartingEnabled = True
        Me.DisplayAverageEnabled = False
        Me.DisplayVoltageEnabled = False
        Me.FiniteBuffering = False
        Me.MovingAverageLength = 10
        Me.RefreshRate = 24
        Me.UpdateEventEnabled = False
        Me.UpdatePeriodCount = 10
        Me.UsingSmartBuffering = False
        Me.ResumeConfig()
    End Sub

    ''' <summary> Reset to Opened and not running conditions. </summary>
    ''' <exception cref="TimeoutException"> Abort timeout--system still running after
    ''' abort. </exception>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Sub Reset()

        Try

            Me.Cursor = Cursors.WaitCursor

            ' abort if running.
            Me.tryStopContinuousInput(Me.StopTimeout, True)

            ' clear the configuration suspension Queue.
            Me.configureSuspensionQueue = New Generic.Queue(Of Boolean)

            If Me.AnalogInput Is Nothing Then
                ' clear the display.
                Me.ClearChannelsList()
            End If

        Catch

            Throw

        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

#End Region

#Region " FUNCTIONAL PROPERTIES "

    ''' <summary> Gets the
    ''' <see cref="System.Diagnostics.TraceEventType">broadcast level</see>. </summary>
    ''' <value> The broadcast level. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property BroadcastLevel() As System.Diagnostics.TraceEventType

#End Region

#Region " DEVICE "

    ''' <summary> Releases the analog input event handlers. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Sub TryReleaseAnalogInput()

        If Me.AnalogInput IsNot Nothing Then

            ' add event handlers for the events we're interested in
            Try
                RemoveHandler Me.AnalogInput.GeneralFailureEvent, AddressOf handleGeneralFailure
            Catch
            End Try

            Try
                RemoveHandler Me.AnalogInput.BufferDoneEvent, AddressOf handleBufferDone
            Catch
            End Try

            Try
                RemoveHandler Me.AnalogInput.QueueDoneEvent, AddressOf handleQueueDone
            Catch
            End Try

            Try
                RemoveHandler Me.AnalogInput.QueueStoppedEvent, AddressOf handleQueueStopped
            Catch
            End Try

            Try
                RemoveHandler Me.AnalogInput.DeviceRemovedEvent, AddressOf handleDeviceRemoved
            Catch
            End Try

            Try
                RemoveHandler Me.AnalogInput.DriverRunTimeErrorEvent, AddressOf handleDriverRuntimeError
            Catch
            End Try

        End If

    End Sub

    ''' <summary> Stops acquisition, if necessary, and closes the analog input board. </summary>
    ''' <remarks> Use this method to close the instance.  The method returns true if success or false
    ''' if it failed closing the instance. </remarks>
    ''' <exception cref="OperationFailedException"> Operation timed out occurred attempting to close
    ''' the analog input
    ''' <innerException cref="System.TimeoutException">Abort timeout--system still running after
    ''' abort</innerException>
    ''' or Open layers exception occurred attempting to close the analog input
    ''' <innerException cref="Global.OpenLayers.Base.OlException">open layers
    ''' exception.</innerException> </exception>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub closeAnalogInput()

        Try

            Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Closing...;. ")

            ' clear the channel list before closing the channel
            Me.ClearChannelsList()

            ' stop if still running
            Me.tryStopContinuousInput(Me.StopTimeout, False)

            ' removes event handlers for the events we're interested in
            Me.TryReleaseAnalogInput()

            If Me.AnalogInput IsNot Nothing Then
                Me.AnalogInput.Dispose()
                Me._analogInput = Nothing
            End If

            ' clear the buffer queue
            If Me._bufferQueue IsNot Nothing Then
                Me._bufferQueue.Clear()
                Me._bufferQueue = Nothing
            End If

            If Me._Device IsNot Nothing Then
                Me._Device.Dispose()
                Me._Device = Nothing
            End If

            ' clear the board name.
            Me.candidateDeviceName = ""

        Catch ex As TimeoutException

            Throw New OperationFailedException("Operation timed out attempting to close the analog input", ex)

        Catch ex As Global.OpenLayers.Base.OlException

            Throw New OperationFailedException("Open layers exception occurred attempting to close the analog input", ex)

        Finally

            Try
                If Me.AnalogInput IsNot Nothing Then
                    Me.AnalogInput.Dispose()
                    Me._analogInput = Nothing
                End If
            Catch
            End Try

            Try
                If Me._bufferQueue IsNot Nothing Then
                    Me._bufferQueue.Clear()
                    Me._bufferQueue = Nothing
                End If
            Catch
            End Try

            Try
                If Me._Device IsNot Nothing Then
                    Me._Device.Dispose()
                    Me._Device = Nothing
                End If
            Catch
            End Try


            ' force turning off of the start button
            Me.Started = False

            ' update the status of the open check box.
            Me._OpenCloseCheckBox.SilentCheckedSetter(Me.IsOpen)
            Me._DeviceNameChooser.Enabled = Not Me.IsOpen

            ' disable user interface
            Me.enableChartControls()
            Me.enableSignalsControls()
            Me.enableChannelsControls()

            If Me.IsOpen Then

                ' if still open, display message and leave in the closing state. 
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed closing;. ")

            Else

                ' set the closed prompt
                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Closed;. ")

            End If

        End Try

    End Sub

    ''' <summary> Tries to close the analog input. </summary>
    Private Sub tryCloseAnalogInput()

        Try

            Me.closeAnalogInput()

        Catch ex As OperationFailedException

            If TypeOf ex.InnerException Is Global.OpenLayers.Base.OlException Then

                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Open Layers Driver exception occurred closing;. Details: {0}.", ex)

            ElseIf TypeOf ex.InnerException Is System.TimeoutException Then

                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Timeout exception occurred closing;. Details: {0}", ex)

            Else

                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Unexpected exception occurred closing;. Details: {0}", ex)

            End If

        Catch

            Throw

        End Try

    End Sub

    ''' <summary> Disconnected an already disconnected board. </summary>
    ''' <remarks> Use this method to close the instance.  The method returns true if success or false
    ''' if it failed closing the instance. </remarks>
    ''' <exception cref="OperationFailedException"> Operation timed out occurred attempting to close
    ''' the analog input
    ''' <innerException cref="System.TimeoutException">Abort timeout--system still running after
    ''' abort</innerException>
    ''' or Open layers exception occurred attempting to close the analog input
    ''' <innerException cref="Global.OpenLayers.Base.OlException">open layers
    ''' exception.</innerException> </exception>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub disconnectAnalogInput()

        Try

            Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Disconnecting...;. ")

            ' removes event handlers for the events we're interested in
            Me.TryReleaseAnalogInput()

            ' clear the board name.
            Me.candidateDeviceName = ""

        Catch ex As TimeoutException

            Throw New OperationFailedException("Operation timed out attempting to close the analog input", ex)

        Catch ex As Global.OpenLayers.Base.OlException

            Throw New OperationFailedException("Open layers exception occurred attempting to close the analog input", ex)

        Finally

            Try
                If Me.AnalogInput IsNot Nothing Then
                    ' Me.AnalogInput.Dispose()
                    Me._analogInput = Nothing
                End If
            Catch
            End Try

            Try
                If Me._bufferQueue IsNot Nothing Then
                    'Me._bufferQueue.Clear()
                    Me._bufferQueue = Nothing
                End If
            Catch
            End Try

            Try
                If Me._Device IsNot Nothing Then
                    'Me._Device.Dispose()
                    Me._Device = Nothing
                End If
            Catch
            End Try

            Try
                ' clear the channel list -- this clears the displays.
                Me.ClearChannelsList()
            Catch
            End Try

            ' force turning off of the start button
            Me.Started = False

            ' update the status of the open check box.
            Me._OpenCloseCheckBox.SilentCheckedSetter(Me.IsOpen)
            Me._DeviceNameChooser.Enabled = True
            Me._OpenCloseCheckBox.Enabled = False

            Try
                ' clear the list of devices.
                Me._DeviceNameChooser.RefreshDeviceNamesList()
            Catch
            End Try

            ' disable user interface
            Me.enableChartControls()
            Me.enableSignalsControls()
            Me.enableChannelsControls()

            If Me.IsOpen Then

                ' if still open, display message and leave in the closing state. 
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed disconnecting;. ")

            Else

                ' set the closed prompt
                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Disconnected;. ")

            End If

        End Try

    End Sub

    ''' <summary> Name of the candidate device. </summary>
    Private candidateDeviceName As String

    ''' <summary> Gets the board name. </summary>
    ''' <value> The name of the device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property DeviceName() As String
        Get
            If Me.Device Is Nothing Then
                Return "n/a"
            Else
                Return Me.Device.DeviceName
            End If
        End Get
    End Property

    ''' <summary> Gets the default device. </summary>
    ''' <value> The device. </value>
    Private Property Device As Global.OpenLayers.Base.Device

    ''' <summary> Determines whether the specified range equals the analog input voltage range. Used as
    ''' a predicate in the array search for an index. </summary>
    ''' <param name="range"> The range. </param>
    ''' <returns> <c>True</c> if [is equal range] [the specified range]; otherwise, <c>False</c>. </returns>
    Private Function IsEqualRange(ByVal range As Global.OpenLayers.Base.Range) As Boolean
        Return range IsNot Nothing AndAlso isr.IO.OL.Equals(range, Me.AnalogInput.VoltageRange, 0.0001)
    End Function

    ''' <summary> <c>True</c> if board disconnected. </summary>
    Private _BoardDisconnected As Boolean

    ''' <summary> Gets or sets a value indicating whether the board was disconnected. </summary>
    ''' <remarks> Allows to immediately address the disconnection of the device. </remarks>
    ''' <value> <c>True</c> if disconnected; otherwise, <c>False</c>. </value>
    Public ReadOnly Property BoardDisconnected As Boolean
        Get
            Return Me._BoardDisconnected
        End Get
    End Property

    ''' <summary> Gets the Open status flag. </summary>
    ''' <remarks> Use this property to get the open status of this instance, which is True if the
    ''' instance was opened. </remarks>
    ''' <value> <c>IsOpen</c> is a Boolean property that can be read from (read only). </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property IsOpen() As Boolean
        Get
            Return Not (Me._BoardDisconnected OrElse Me.AnalogInput Is Nothing)
        End Get
    End Property

    ''' <summary> Opens the analog input board and instantiates the analog input subsystem. Requires
    ''' setting of the candidate device name. </summary>
    ''' <remarks> Use this method to open the instance.  The method returns true if success or false if
    ''' it failed opening the instance. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="OperationFailedException">  Thrown when operation failed to execute. </exception>
    Private Sub openAnalogInput()

        If Not Global.OpenLayers.Base.DeviceMgr.Get.HardwareAvailable Then
            ' Me.onHardwareDisconnected(New Global.OpenLayers.Base.GeneralEventArgs)
            Dim eventHandler As EventHandler(Of EventArgs) = Me.DisconnectedEvent
            If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, New EventArgs)
            Throw New InvalidOperationException("Hardware not available.")
        End If

        If String.IsNullOrWhiteSpace(Me.candidateDeviceName) Then
            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Attempted opening board with an empty board name;. ")
            Throw New System.InvalidOperationException("Board name not specified.")
        End If

        Try

            ' reset to open state conditions.
            Me.Reset()

            ' Opens the subsystem
            Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Opening board '{0}';. ", Me.candidateDeviceName)

            ' get a board 
            Me._Device = Global.OpenLayers.Base.DeviceMgr.Get.SelectDevice(Me.candidateDeviceName)

            If Me.Device Is Nothing Then

                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed opening board '{0}';. ", Me.candidateDeviceName)
                Throw New OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture, "Failed opening board '{0}'", Me.candidateDeviceName))

            Else

                Me.Text = Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Opened Device {0} Board #{1};. ",
                                             Me.Device.DeviceName, Me.Device.GetHardwareInfo.BoardId).Details

                ' update the board name.
                Me.candidateDeviceName = Me.Device.DeviceName

                ' instantiate the open layer analog input sub system
                Me._analogInput = New isr.IO.OL.AnalogInputSubsystem(Me.Device, 0)

                ' clear the selected channel and signal
                Me.selectChannel(-1)
                Me.selectSignal(-1)

                ' set the channel type
                If Me.AnalogInput.ChannelType = Global.OpenLayers.Base.ChannelType.SingleEnded Then
                    If Me.AnalogInput.SupportsSingleEndedInput Then
                        Me.AnalogInput.ChannelType = Global.OpenLayers.Base.ChannelType.SingleEnded
                    Else
                        Me.AnalogInput.ChannelType = Global.OpenLayers.Base.ChannelType.Differential
                    End If
                Else
                    If Me.AnalogInput.SupportsDifferentialInput Then
                        Me.AnalogInput.ChannelType = Global.OpenLayers.Base.ChannelType.Differential
                    Else
                        Me.AnalogInput.ChannelType = Global.OpenLayers.Base.ChannelType.SingleEnded
                    End If
                End If

                ' set the board voltage range selection list
                Me._BoardInputRangesComboBox.DataSource = Me.AnalogInput.AvailableBoardRanges("({0},{1}) V")
                Dim i As Integer = Array.FindIndex(Me.AnalogInput.SupportedVoltageRanges,
                                                   New Predicate(Of Global.OpenLayers.Base.Range)(AddressOf IsEqualRange))
                Me._BoardInputRangesComboBox.SelectedIndex = i

                ' set the channel selection list.
                Me._ChannelComboBox.DataSource = Me.AnalogInput.AvailableChannels("{0}")

                ' set the ranges combo
                Me._ChannelRangesComboBox.DataSource = Me.AnalogInput.AvailableRanges("({0},{1}) V")

                ' set some properties
                Me.AnalogInput.DataFlow = Global.OpenLayers.Base.DataFlow.Continuous
                Dim toolTipText As String = "Channel samples per second between {0} and {1}"
                toolTipText = String.Format(Globalization.CultureInfo.CurrentCulture,
                                            toolTipText, Me.AnalogInput.Clock.MinFrequency, Me.AnalogInput.Clock.MaxFrequency)
                Me._ToolTip.SetToolTip(Me._SamplingRateNumeric, toolTipText)

                ' add event handlers for the events we're interested in
                AddHandler Me.AnalogInput.GeneralFailureEvent, AddressOf handleGeneralFailure
                AddHandler Me.AnalogInput.BufferDoneEvent, AddressOf handleBufferDone
                AddHandler Me.AnalogInput.QueueDoneEvent, AddressOf handleQueueDone
                AddHandler Me.AnalogInput.QueueStoppedEvent, AddressOf handleQueueStopped
                AddHandler Me.AnalogInput.DeviceRemovedEvent, AddressOf handleDeviceRemoved
                AddHandler Me.AnalogInput.DriverRunTimeErrorEvent, AddressOf handleDriverRuntimeError

                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Initialized analog input on '{0}';. ", Me.DeviceName)

                ' reset if board was disconnected.
                If Me.BoardDisconnected Then
                    Me.Reset()
                End If

                Me._BoardDisconnected = False

            End If

        Catch

            ' close to meet strong guarantees
            Try
                Me.Close()
            Finally
            End Try

            ' throw an exception
            Throw

        Finally

            ' force turning off of the start button
            Me.Started = False

            ' enable user interface
            Me.enableChartControls()
            Me.enableSignalsControls()
            Me.enableChannelsControls()

            ' update the status of the open check box.
            Me._OpenCloseCheckBox.SilentCheckedSetter(Me.IsOpen)
            Me._DeviceNameChooser.Enabled = Not Me.IsOpen

            Me._AnalogInputConfigurationRequired = True
            Me._ChartConfigurationRequired = True

        End Try

    End Sub

    ''' <summary> Tries to open the analog input. </summary>
    Private Sub tryOpenAnalogInput()

        Try

            Me.openAnalogInput()

        Catch ex As InvalidOperationException

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Invalid operation exception occurred opening;. Details: {0}.", ex)

        Catch ex As OperationFailedException

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Operation failed exception occurred opening;. Details: {0}.", ex)

        Catch

            Throw

        End Try

    End Sub

#End Region

#Region " ANALOG INPUT CONFIGURATION "

    ''' <summary> The analog input. </summary>
    Private _analogInput As AnalogInputSubsystem

    ''' <summary> Gets reference to the analog input. </summary>
    ''' <value> The analog input. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property AnalogInput() As AnalogInputSubsystem
        Get
            Return Me._analogInput
        End Get
    End Property

#Region " CHANNELS "

    ''' <summary> Adds a channel to the channel list. </summary>
    ''' <param name="channelNumber"> . </param>
    ''' <param name="gain">          . </param>
    ''' <param name="name">          The name. </param>
    Public Sub AddChannel(ByVal channelNumber As Integer, ByVal gain As Double, ByVal name As String)

        ' add the new channel to the channel list.
        Me.AnalogInput.AddChannel(channelNumber, gain, name)
        Me.selectChannel(Me.AnalogInput.SelectedChannelLogicalNumber)

        ' update the display
        updateChannelList()

        Me.enableChartControls()
        Me.enableSignalsControls()
        Me.enableChannelsControls()

        ' request configuration of analog input.
        Me.AnalogInputConfigurationRequired = True

    End Sub

    ''' <summary> Adds a channel to the channel list. </summary>
    ''' <param name="channelNumber"> . </param>
    ''' <param name="gainIndex">     . </param>
    ''' <param name="name">          The name. </param>
    Public Sub AddChannel(ByVal channelNumber As Integer, ByVal gainIndex As Integer, ByVal name As String)

        ' add the new channel to the channel list.
        Me.AnalogInput.AddChannel(channelNumber, gainIndex, name)
        Me.selectChannel(Me.AnalogInput.SelectedChannelLogicalNumber)

        ' update the display
        Me.updateChannelList()

        Me.enableChartControls()
        Me.enableSignalsControls()
        Me.enableChannelsControls()

        ' request configuration of analog input.
        Me.AnalogInputConfigurationRequired = True

    End Sub

    ''' <summary> Clears the channel and signal lists. </summary>
    Public Sub ClearChannelsList()

        ' Clear the selected signal and list
        Me.ClearSignalsList()

        ' Clear the selected channel and list
        Me.selectChannel(-1)

        ' the list must have at least one item.
        If Me.AnalogInput IsNot Nothing AndAlso Not Me.ContinuousOperationActive Then
            Me.AnalogInput.ChannelList.Clear()
        ElseIf Me.AnalogInput IsNot Nothing AndAlso Me.ContinuousOperationActive Then
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Attempt to clear the channel list was ignored because the subsystem is running;. ")
        End If

        If Me.AnalogInput Is Nothing OrElse Me.AnalogInput.ChannelsCount = 0 Then

            ' remove all items from the list view
            Me._ChannelsListView.Items.Clear()
            Me._ChannelsListView.Invalidate()

            ' clear the channel list.
            Me._SignalChannelComboBox.Items.Clear()
            Me._SignalChannelComboBox.Invalidate()

        End If

        ' update user interface to reflect the removal of the channels.
        Me.enableChartControls()
        Me.enableSignalsControls()
        Me.enableChannelsControls()

    End Sub

    ''' <summary> Returns a channel list view item. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channel"> The channel. </param>
    ''' <returns> The new channel list view item. </returns>
    Private Function createChannelListViewItem(ByVal channel As Global.OpenLayers.Base.ChannelListEntry) As ListViewItem
        If channel Is Nothing Then
            Throw New ArgumentNullException("channel")
        End If
        ' Create the channel item
        Dim channelItem As Windows.Forms.ListViewItem = New Windows.Forms.ListViewItem(channel.PhysicalChannelNumber.ToString(Globalization.CultureInfo.CurrentCulture))
        channelItem.SubItems.Add(channel.Name)
        Dim formatValue As String = "({0},{1}) V"
        channelItem.SubItems.Add(String.Format(Globalization.CultureInfo.CurrentCulture, formatValue,
                                               Me.AnalogInput.MinVoltage, Me.AnalogInput.MaxVoltage))
        Return channelItem
    End Function

    ''' <summary> Returns a default channel name. </summary>
    ''' <param name="channelNumber"> . </param>
    ''' <returns> The default channel name. </returns>
    Private Shared Function defaultChannelName(ByVal channelNumber As Integer) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "Ch. {0}", channelNumber)
    End Function

    ''' <summary> Removes the specified channel from the list of channels. </summary>
    ''' <param name="channelNumber"> . </param>
    Public Sub RemoveChannel(ByVal channelNumber As Integer)

        Dim physicalChannelNumber As Integer = channelNumber
        If Me.AnalogInput.PhysicalChannelExists(physicalChannelNumber) Then
            Dim channel As Global.OpenLayers.Base.ChannelListEntry = Me.AnalogInput.SelectPhysicalChannel(physicalChannelNumber)

            Me.RemoveSignal(channel)

            Me.AnalogInput.ChannelList.Remove(channel)

            Me.updateChannelList()

            If Me.selectedChannel Is Nothing OrElse Not Me.AnalogInput.ChannelList.Contains(Me.selectedChannel) Then
                If Me.AnalogInput.HasChannels Then
                    Me.selectChannel(0)
                End If
            End If

            ' request configuration of analog input and chart.
            Me.AnalogInputConfigurationRequired = True
            Me.ChartConfigurationRequired = True

        End If

    End Sub

    ''' <summary> Removes the specified channel from the list of channels. </summary>
    ''' <param name="channel"> The channel. </param>
    Public Sub RemoveChannel(ByVal channel As Global.OpenLayers.Base.ChannelListEntry)

        If channel IsNot Nothing AndAlso Me.AnalogInput.ChannelList.Contains(channel) Then
            Me.RemoveSignal(channel)
            Me.AnalogInput.ChannelList.Remove(channel)
            Me.updateChannelList()

            If Me.selectedChannel Is Nothing OrElse Not Me.AnalogInput.ChannelList.Contains(Me.selectedChannel) Then
                If Me.AnalogInput.HasChannels Then
                    Me.selectChannel(0)
                End If
            End If

            If Me.selectedSignal Is Nothing OrElse Not Me.Chart.Signals.Contains(Me.selectedSignal) Then
                If Me.Chart.Signals.Count > 0 Then
                    Me.selectSignal(0)
                End If
            End If

        End If

    End Sub

    ''' <summary> Selects a channel. </summary>
    ''' <param name="channelIndex"> The channel logical number = channel index. </param>
    ''' <returns> The selected channel. </returns>
    Private Function selectChannel(ByVal channelIndex As Integer) As Global.OpenLayers.Base.ChannelListEntry

        Me.selectedChannelIndex = channelIndex
        If Me.AnalogInput IsNot Nothing AndAlso channelIndex >= 0 AndAlso
            channelIndex < Me.AnalogInput.ChannelList.Count Then

            Me.selectedChannel = Me.AnalogInput.ChannelList(Me.selectedChannelIndex)

        Else

            Me.selectedChannelIndex = -1
            Me.selectedChannel = Nothing

        End If

        Return Me.selectedChannel

    End Function

    ''' <summary> Gets or sets the index of the selected channel in the channel list. This is required
    ''' because some Open Layer methods requires the index rather than the channel. </summary>
    ''' <value> The selected channel index. </value>
    Private Property selectedChannelIndex As Integer

    ''' <summary> Gets or sets the selected channel. </summary>
    ''' <value> The selected channel. </value>
    Private Property selectedChannel() As Global.OpenLayers.Base.ChannelListEntry

    ''' <summary> Updates a channel in the channel list. </summary>
    ''' <param name="channelNumber"> . </param>
    ''' <param name="gainIndex">     . </param>
    ''' <param name="name">          The name. </param>
    Public Sub UpdateChannel(ByVal channelNumber As Integer, ByVal gainIndex As Integer, ByVal name As String)

        If String.IsNullOrWhiteSpace(name) Then
            name = ""
        End If
        Me.AnalogInput.UpdateChannel(channelNumber, gainIndex, name)

        ' add the new channel to the channel list.
        Me.selectChannel(Me.AnalogInput.SelectedChannelLogicalNumber)

        ' update the display
        updateChannelList()

        Me.enableChartControls()
        Me.enableSignalsControls()
        Me.enableChannelsControls()

        ' request configuration of analog input.
        Me.AnalogInputConfigurationRequired = True

    End Sub

    ''' <summary> Updates the channel list display. </summary>
    Private Sub updateChannelList()

        Dim selectedIndex As Integer = -1
        If Me._ChannelsListView.SelectedIndices.Count > 0 Then
            selectedIndex = Me._ChannelsListView.SelectedIndices.Item(0)
        End If

        Dim selectedSignalChannelIndex As Integer = -1
        If Me._SignalChannelComboBox.Items.Count > 0 Then
            selectedSignalChannelIndex = Me._SignalChannelComboBox.SelectedIndex
        End If

        ' update the list of available channels for signals.
        Me._SignalChannelComboBox.Items.Clear()
        Me._ChannelsListView.Items.Clear()
        For Each channel As Global.OpenLayers.Base.ChannelListEntry In Me.AnalogInput.ChannelList

            ' get a channel item,
            Dim channelItem As ListViewItem = createChannelListViewItem(channel)

            Me._ChannelsListView.Items.Add(channelItem)

            ' add the channel to the list of signal channels
            Me._SignalChannelComboBox.Items.Add(channel.Name)

        Next
        Me._ChannelsListView.Invalidate()

        If Me._ChannelsListView.Items.Count > 0 Then
            selectedIndex = Math.Min(Math.Max(selectedIndex, 0), Me._ChannelsListView.Items.Count - 1)
            Me._ChannelsListView.Items(selectedIndex).Selected = True
        End If

        If Me._SignalChannelComboBox.Items.Count > 0 Then
            selectedSignalChannelIndex = Math.Min(Math.Max(selectedSignalChannelIndex, 0),
                                                  Me._SignalChannelComboBox.Items.Count - 1)
            Me._SignalChannelComboBox.SelectedIndex = selectedSignalChannelIndex
        End If

    End Sub

#End Region

#Region " SIGNALS "

    ''' <summary> Adds a signal to the display. </summary>
    ''' <param name="channelIndex"> The channel index. </param>
    Public Sub AddSignal(ByVal channelIndex As Integer)

        Me.AddSignal(Me.AnalogInput.ChannelList(channelIndex),
                     CDbl(Me._SignalMinNumericUpDown.Value), CDbl(Me._SignalMaxNumericUpDown.Value),
                     Me._defaultSignalColors(Me.Chart.Signals.Count))

    End Sub

    ''' <summary> Adds a signal to the display. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channel">    The channel. </param>
    ''' <param name="minVoltage"> The minimum voltage. </param>
    ''' <param name="maxVoltage"> The maximum voltage. </param>
    ''' <param name="color">      The color. </param>
    Public Sub AddSignal(ByVal channel As Global.OpenLayers.Base.ChannelListEntry,
                              ByVal minVoltage As Double, ByVal maxVoltage As Double, ByVal color As System.Drawing.Color)

        If channel Is Nothing Then
            Throw New ArgumentNullException("channel")
        End If

        ' update the selected channel
        Me.selectChannel(Me.AnalogInput.ChannelList.LogicalChannelNumber(channel))

        ' select the specified channel
        Me.AnalogInput.SelectPhysicalChannel(channel.PhysicalChannelNumber)

        ' add signal 
        Dim signal As New OpenLayers.Signals.MemorySignal
        signal.Name = channel.Name
        signal.RangeMax = Me.AnalogInput.MaxVoltage
        signal.RangeMin = Me.AnalogInput.MinVoltage
        signal.CurrentRangeMax = maxVoltage
        signal.CurrentRangeMin = minVoltage
        signal.Unit = "Volts"

        Me.Chart.DisableRendering()

        ' add signal to the chart
        Me.Chart.Signals.Add(signal)

        ' set color.
        Me.Chart.SetCurveColor(Me.Chart.Signals.Count - 1, color)

        ' configure the chart
        Me.Chart.SignalListUpdate()

        Me.Chart.EnableRendering()

        ' update the list of signals.
        Me.updateSignalList()

        ' select this signal
        Me.selectSignal(Me.Chart.Signals.Count - 1)

        Me.enableChartControls()
        Me.enableSignalsControls()
        Me.enableChannelsControls()

        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True

    End Sub

    ''' <summary> Clears the channel and signal lists. </summary>
    Public Sub ClearSignalsList()

        ' Clear the selected signal and list
        Me.selectSignal(-1)
        Me.Chart.Signals.Clear()

        ' remove all items from the list view
        Me._SignalsListView.Items.Clear()
        Me._SignalsListView.Invalidate()

        ' update user interface to reflect the removal of the channels.
        Me.enableChartControls()
        Me.enableSignalsControls()
        Me.enableChannelsControls()

        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True

    End Sub

    ''' <summary> Returns a channel list view item. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channel"> The channel. </param>
    ''' <param name="signal">  . </param>
    ''' <returns> The new signal list view item. </returns>
    Private Shared Function createSignalListViewItem(ByVal channel As Global.OpenLayers.Base.ChannelListEntry,
                                                     ByVal signal As OpenLayers.Signals.MemorySignal) As ListViewItem
        If channel Is Nothing Then
            Throw New ArgumentNullException("channel")
        End If
        If signal Is Nothing Then
            Throw New ArgumentNullException("signal")
        End If
        ' Create the signal item
        Dim signalItem As Windows.Forms.ListViewItem = New Windows.Forms.ListViewItem(channel.PhysicalChannelNumber.ToString(Globalization.CultureInfo.CurrentCulture))
        signalItem.SubItems.Add(signal.Name)
        Dim formatValue As String = "({0},{1}) V"
        signalItem.SubItems.Add(String.Format(Globalization.CultureInfo.CurrentCulture, formatValue,
                                              signal.CurrentRangeMin, signal.CurrentRangeMax))
        Return signalItem
    End Function

    ''' <summary> Removes the signal associated with the specified channel. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="channel"> The channel. </param>
    Public Sub RemoveSignal(ByVal channel As Global.OpenLayers.Base.ChannelListEntry)
        If channel Is Nothing Then
            Throw New ArgumentNullException("channel")
        End If

        If Me.Chart.Signals.Contains(channel.Name) Then
            Me.RemoveSignal(Me.Chart.Signals.SelectNamedSignal(channel.Name))
        End If

    End Sub

    ''' <summary> Removes the signal associated with the specified channel. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="signal"> . </param>
    Public Sub RemoveSignal(ByVal signal As OpenLayers.Signals.MemorySignal)
        If signal Is Nothing Then
            Throw New ArgumentNullException("signal")
        End If
        If Me.Chart.Signals.Contains(signal) Then

            Me.Chart.DisableRendering()

            ' remove the signal
            Me.Chart.Signals.Remove(signal)

            ' update the chart
            Me.Chart.SignalListUpdate()

            Me.Chart.EnableRendering()

            ' update the list of signals.
            Me.updateSignalList()

            If Me.selectedSignal Is Nothing OrElse Not Me.Chart.Signals.Contains(Me.selectedSignal) Then
                If Me.Chart.Signals.Count > 0 Then
                    Me.selectSignal(0)
                End If
            End If
        End If

    End Sub

    ''' <summary> Selects a signal. </summary>
    ''' <param name="signalIndex"> . </param>
    ''' <returns> The selected signal. </returns>
    Private Function selectSignal(ByVal signalIndex As Integer) As OpenLayers.Signals.MemorySignal

        Me.selectedSignalIndex = signalIndex
        If signalIndex >= 0 AndAlso signalIndex < Me.Chart.Signals.Count Then

            Me.selectedSignal = Me.Chart.Signals(Me.selectedSignalIndex)

            ' update tool tips for displays
            Me.DisplayAverageEnabled = Me.DisplayAverageEnabled
            Me.DisplayVoltageEnabled = Me.DisplayVoltageEnabled

            ' update selected signal display
            If Me.selectedSignal.CurrentRangeMin < Me._SignalMinNumericUpDown.Maximum AndAlso
               Me.selectedSignal.CurrentRangeMin > Me._SignalMinNumericUpDown.Minimum Then
                Me._SignalMinNumericUpDown.Value = CDec(Me.selectedSignal.CurrentRangeMin)
            End If
            If Me.selectedSignal.CurrentRangeMax < Me._SignalMaxNumericUpDown.Maximum AndAlso
               Me.selectedSignal.CurrentRangeMax > Me._SignalMaxNumericUpDown.Minimum Then
                Me._SignalMaxNumericUpDown.Value = CDec(Me.selectedSignal.CurrentRangeMax)
            End If

            If Me.selectedSignalIndex >= 0 AndAlso Me.selectedSignalIndex < Me._SignalsListView.Items.Count Then
                If Me._SignalsListView.SelectedIndices.Count >= 1 AndAlso Me._SignalsListView.SelectedIndices.Item(0) <> signalIndex Then
                    Dim wasEnabled As Boolean = Me._SignalsListView.Enabled
                    Me._SignalsListView.Enabled = False
                    Me._SignalsListView.SelectedIndices.Clear()
                    Me._SignalsListView.SelectedIndices.Add(Me.selectedSignalIndex)
                    Me._SignalsListView.Enabled = wasEnabled
                End If
            End If

        Else

            Me.selectedSignalIndex = -1
            Me.selectedSignal = Nothing

        End If

        Return Me.selectedSignal

    End Function

    ''' <summary> Gets or sets the index of the selected signal in the signal list. This is required
    ''' because some Open Layer methods requires the index rather than the signal. </summary>
    ''' <value> The selected signal index. </value>
    Private Property selectedSignalIndex As Integer

    ''' <summary> Gets or sets the selected signal. </summary>
    ''' <value> The selected signal. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private Property selectedSignal() As OpenLayers.Signals.MemorySignal

    ''' <summary>
    ''' Gets or sets the logical channel numbers for each signal.
    ''' </summary>
    Private signalLogicalChannelNumbers() As Integer

    ''' <summary> Updates the list of signals. </summary>
    Private Sub updateSignalList()

        Dim selectedIndex As Integer = -1
        If Me._SignalsListView.SelectedIndices.Count > 0 Then
            selectedIndex = Me._SignalsListView.SelectedIndices.Item(0)
        End If

        If Me.Chart.Signals.Count > 0 Then
            ReDim Me.signalLogicalChannelNumbers(Me.Chart.Signals.Count - 1)
            Array.Clear(Me.signalLogicalChannelNumbers, 0, Me.signalLogicalChannelNumbers.Length)
        End If

        Dim signal As New OpenLayers.Signals.MemorySignal
        Me._SignalsListView.Items.Clear()
        For Each signal In Me.Chart.Signals

            ' get the channel index (logical number)
            Dim channelIndex As Integer = Me.AnalogInput.ChannelList.LogicalChannelNumber(signal.Name)

            ' save the channel index for selecting the buffer to display for the signal.
            Me.signalLogicalChannelNumbers(Me._SignalsListView.Items.Count) = channelIndex

            ' get a signal item,
            Dim signalItem As ListViewItem = createSignalListViewItem(Me.AnalogInput.ChannelList.Item(channelIndex), signal)

            ' get the item color from the signal color.
            signalItem.ForeColor = Me.Chart.GetCurveColor(Me._SignalsListView.Items.Count)

            ' add the entry.
            Me._SignalsListView.Items.Add(signalItem)

        Next
        Me._SignalsListView.Invalidate()

        ' update the signals control box.
        Me.enableSignalsControls()

        If Me._SignalsListView.Items.Count > 0 Then
        End If

        If Me._SignalsListView.Items.Count > selectedIndex + 1 Then
            If selectedIndex > 0 Then
                Me._SignalsListView.Items(selectedIndex).Selected = True
            End If
        End If

    End Sub


#End Region

#Region " BUFFERS COUNT "

    ''' <summary> Gets or sets the maximum Buffers Count. </summary>
    ''' <value> The max Buffers Count. </value>
    <Description("The display maximum Buffers Count."), Category("Data"), DefaultValue(1000)>
    Public Property MaximumBuffersCount As Integer
        Get
            Return CInt(Me._BuffersCountNumeric.Maximum)
        End Get
        Set(value As Integer)
            Me._BuffersCountNumeric.SafeMaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum Buffers Count. </summary>
    ''' <value> The Min Buffers Count. </value>
    <Description("The display minimum Buffers Count."), Category("Data"), DefaultValue(2)>
    Public Property MinimumBuffersCount As Integer
        Get
            Return CInt(Me._BuffersCountNumeric.Minimum)
        End Get
        Set(value As Integer)
            Me._BuffersCountNumeric.SafeMinimumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of data collection buffers. </summary>
    ''' <value> The number of buffers. </value>
    <Description("Gets or sets the number of data collection buffers."), Category("Data"), DefaultValue(5)>
    Public Property BuffersCount() As Integer

        Get
            Return CInt(Me._BuffersCountNumeric.Value)
        End Get
        Set(ByVal value As Integer)
            If value <> Me.BuffersCount Then
                Me._BuffersCountNumeric.SafeValueSetter(value)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#Region " CHANNEL BUFFER SIZE "

    ''' <summary> Size of the channel buffer. </summary>
    Private _channelBufferSize As Integer

    ''' <summary> Gets or sets the number of samples per channel. In scope mode this also equals the
    ''' signal size. In strip chart mode, the board collects small buffers of 2 or more points. The
    ''' signal is updated to display these new data when the buffers are filled. With
    ''' <see cref="UsingSmartBuffering">smart buffering</see> this is larger than the
    ''' <see cref="UsbBufferSize">USB buffer size</see>. </summary>
    ''' <value> The size of the channel buffer. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ChannelBufferSize() As Integer
        Get
            Return Me._channelBufferSize
        End Get
    End Property

    ''' <summary> Size of the sample buffer. </summary>
    Private _sampleBufferSize As Integer

    ''' <summary> Gets or sets total number of samples in the analog input buffer.  This is read only
    ''' because the control exposes the buffer size per channel and configure this value based on the
    ''' number of channels. Equals the channel buffer size times the number of channels. </summary>
    ''' <value> The size of the sample buffer. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SampleBufferSize() As Integer
        Get
            Return Me._sampleBufferSize
        End Get
    End Property

    ''' <summary> Gets or sets the maximum Sample Size. </summary>
    ''' <value> The max Sample Size. </value>
    <Description("The maximum Sample Size."), Category("Data"), DefaultValue(1000)>
    Public Property MaximumSampleSize As Integer
        Get
            Return CInt(Me._SampleSizeNumeric.Maximum)
        End Get
        Set(value As Integer)
            Me._SampleSizeNumeric.SafeMaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum Sample Size. </summary>
    ''' <value> The Min Sample Size. </value>
    <Description("The minimum Sample Size."), Category("Data"), DefaultValue(2)>
    Public Property MinimumSampleSize As Integer
        Get
            Return CInt(Me._SampleSizeNumeric.Minimum)
        End Get
        Set(value As Integer)
            Me._SampleSizeNumeric.SafeMinimumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of samples acquired per channel buffer. In strip chart mode
    ''' smart buffering this could be smaller than the signal buffer size. ????which is allocated
    ''' based on the USB buffer size of 256 samples. </summary>
    ''' <value> The size of the sample. </value>
    <Description("Gets or sets the number of samples to collect per channel buffer."), Category("Data"), DefaultValue(1000.0)>
    Public Property SampleSize() As Integer

        Get
            Return CInt(Me._SampleSizeNumeric.Value)
        End Get
        Set(ByVal value As Integer)
            If value <> Me.SampleSize Then
                Me._SampleSizeNumeric.SafeValueSetter(value)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#Region " MOVING AVERAGE "

    ''' <summary> Gets or sets the maximum Moving Average Length. </summary>
    ''' <value> The max Moving Average Length. </value>
    <Description("The display maximum Moving Average Length."), Category("Data"), DefaultValue(100)>
    Public Property MaximumMovingAverageLength As Integer
        Get
            Return CInt(Me._MovingAverageLengthNumeric.Maximum)
        End Get
        Set(value As Integer)
            Me._MovingAverageLengthNumeric.SafeMaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum Moving Average Length. </summary>
    ''' <value> The Min Moving Average Length. </value>
    <Description("The display minimum Moving Average Length."), Category("Data"), DefaultValue(1)>
    Public Property MinimumMovingAverageLength As Integer
        Get
            Return CInt(Me._MovingAverageLengthNumeric.Minimum)
        End Get
        Set(value As Integer)
            Me._MovingAverageLengthNumeric.SafeMinimumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the average length of the moving. </summary>
    ''' <value> The average length of the moving. </value>
    <Description("The number of point to use when calculating the moving average for the status bar."), Category("Data"), DefaultValue(10)>
    Public Property MovingAverageLength() As Integer

        Get
            Return CInt(Me._MovingAverageLengthNumeric.Value)
        End Get
        Set(ByVal Value As Integer)
            If Value <> Me.MovingAverageLength Then
                Me._MovingAverageLengthNumeric.SafeValueSetter(Value)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#Region " REFRESH RATE "

    ''' <summary> Gets or sets the maximum refresh rate. </summary>
    ''' <value> The max refresh rate. </value>
    <Description("The display maximum refresh rate per second."), Category("Appearance"), DefaultValue(100)>
    Public Property MaximumRefreshRate As Decimal
        Get
            Return Me._RefreshRateNumeric.Maximum
        End Get
        Set(value As Decimal)
            Me._RefreshRateNumeric.SafeMaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum refresh rate. </summary>
    ''' <value> The Min refresh rate. </value>
    <Description("The display minimum refresh rate per second."), Category("Appearance"), DefaultValue(1.0)>
    Public Property MinimumRefreshRate As Decimal
        Get
            Return Me._RefreshRateNumeric.Minimum
        End Get
        Set(value As Decimal)
            Me._RefreshRateNumeric.SafeMinimumSetter(value)
        End Set
    End Property

    ''' <summary> The next refresh time. </summary>
    Dim nextRefreshTime As DateTime
    ''' <summary> The refresh time span. </summary>
    Dim refreshTimespan As TimeSpan

    ''' <summary> Gets or sets the display refresh rate. </summary>
    ''' <value> The refresh rate. </value>
    <Description("The display refresh rate per second."), Category("Appearance"), DefaultValue(24.0)>
    Public Property RefreshRate() As Double
        Get
            Return Me._RefreshRateNumeric.Value
        End Get
        Set(ByVal value As Double)
            If value <> Me.RefreshRate Then
                Me._RefreshRateNumeric.SafeValueSetter(value)
                ' request configuration of chart.
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#Region " SAMPLING RATE "

    ''' <summary> Gets or sets the sampling period. This is the inverse of the
    ''' <see cref="ClockRate">clock frequency</see>. </summary>
    ''' <value> The sampling period. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SamplingPeriod As Double

        Get
            If Me.IsOpen Then
                Return 1.0# / Me.AnalogInput.Clock.Frequency
            Else
                Return 0
            End If
        End Get
        Set(value As Double)
            If value > 0 Then
                Me.ClockRate = 1.0# / value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the actual analog input board rate.  
    ''' The clock differs from the <see cref="SamplingRate">sampling rate</see> in cases where a
    ''' sampling rate slower then the minimum clock rate is desired or in strip chart
    ''' <see cref="UsingSmartBuffering">smart buffering mode</see>. </summary>
    ''' <value> The clock rate. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ClockRate() As Double
        Get
            If Me.IsOpen Then
                Return Me.AnalogInput.Clock.Frequency
            Else
                Return 0
            End If
        End Get
        Set(value As Double)
            If Me.IsOpen Then
                Me.AnalogInput.Clock.Frequency = value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the maximum Sampling rate. </summary>
    ''' <value> The max Sampling rate. </value>
    <Description("The display maximum Sampling rate per second."), Category("Appearance"), DefaultValue(100000)>
    Public Property MaximumSamplingRate As Decimal
        Get
            Return Me._SamplingRateNumeric.Maximum
        End Get
        Set(value As Decimal)
            Me._SamplingRateNumeric.SafeMaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum Sampling rate. </summary>
    ''' <value> The Min Sampling rate. </value>
    <Description("The display minimum Sampling rate per second."), Category("Appearance"), DefaultValue(60)>
    Public Property MinimumSamplingRate As Decimal
        Get
            Return Me._SamplingRateNumeric.Minimum
        End Get
        Set(value As Decimal)
            Me._SamplingRateNumeric.SafeMinimumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the sample rate in samples per second. </summary>
    ''' <value> The sampling rate. </value>
    <Description("Gets or sets the sample rate in samples per second."), Category("Data"), DefaultValue(1000.0)>
    Public Property SamplingRate() As Double

        Get
            Return Me._SamplingRateNumeric.Value
        End Get
        Set(ByVal value As Double)
            If Math.Abs(Me.SamplingRate - value) > 0.001 Then
                Me._SamplingRateNumeric.SafeValueSetter(CDec(value))
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
            If Me.IsOpen Then
                Me._ToolTip.SetToolTip(Me._SamplingRateNumeric,
                                       String.Format(Globalization.CultureInfo.CurrentCulture,
                                                     "Number of samples per second to collect. Board Clock Rate is {0}Hz",
                                                     Me.ClockRate.ToString("{0:0.00}", Globalization.CultureInfo.CurrentCulture)))
            Else
                Me._ToolTip.SetToolTip(Me._SamplingRateNumeric, "Number of samples per second to collect.")
            End If
        End Set
    End Property

#End Region

#Region " SIGNAL BUFFER SIZE "

    ''' <summary> Gets or sets the maximum Signal Buffer Size. </summary>
    ''' <value> The max Signal Buffer Size. </value>
    <Description("The maximum Signal Buffer Size."), Category("Data"), DefaultValue(10000)>
    Public Property MaximumSignalBufferSize As Integer
        Get
            Return CInt(Me._SignalBufferSizeNumeric.Maximum)
        End Get
        Set(value As Integer)
            Me._SignalBufferSizeNumeric.SafeMaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum Signal Buffer Size. </summary>
    ''' <value> The Min Signal Buffer Size. </value>
    <Description("The minimum Signal Buffer Size."), Category("Data"), DefaultValue(2)>
    Public Property MinimumSignalBufferSize As Integer
        Get
            Return CInt(Me._SignalBufferSizeNumeric.Minimum)
        End Get
        Set(value As Integer)
            Me._SignalBufferSizeNumeric.SafeMinimumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of samples per signal. This is the same as the maximum
    ''' visible samples on the chart. In scope mode, this is the same as the channel buffer size.  In
    ''' strip chart a signal includes the samples for many small sequential buffers that are
    ''' retrieved from the voltage buffer. </summary>
    ''' <value> The size of the signal buffer. </value>
    <Description("Gets or sets the number of samples per signal."), Category("Data"), DefaultValue(1000.0)>
    Public Property SignalBufferSize() As Integer
        Get
            Return CInt(Me._SignalBufferSizeNumeric.Value)
        End Get
        Set(ByVal value As Integer)
            If value <> Me.SignalBufferSize Then
                Me._SignalBufferSizeNumeric.SafeValueSetter(value)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#Region " SIGNAL MEMORY LENGTH "

    ''' <summary> Gets or sets the maximum Signal Memory Length. </summary>
    ''' <value> The max Signal Memory Length. </value>
    <Description("The maximum Signal Memory Length."), Category("Data"), DefaultValue(100000000)>
    Public Property MaximumSignalMemoryLength As Integer
        Get
            Return CInt(Me._SignalMemoryLengthNumeric.Maximum)
        End Get
        Set(value As Integer)
            Me._SignalMemoryLengthNumeric.SafeMaximumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Minimum Signal Memory Length. </summary>
    ''' <value> The Min Signal Memory Length. </value>
    <Description("The minimum Signal Memory Length."), Category("Data"), DefaultValue(2)>
    Public Property MinimumSignalMemoryLength As Integer
        Get
            Return CInt(Me._SignalMemoryLengthNumeric.Minimum)
        End Get
        Set(value As Integer)
            Me._SignalMemoryLengthNumeric.SafeMinimumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the signal memory length. </summary>
    ''' <value> The length of the signal memory. </value>
    <Description("The signal memory length"), Category("Data"), DefaultValue(10000.0)>
    Public Property SignalMemoryLength() As Integer
        Get
            Return CInt(Me._SignalMemoryLengthNumeric.Value)
        End Get
        Set(ByVal value As Integer)
            If value <> Me.SignalMemoryLength Then
                Me._SignalMemoryLengthNumeric.SafeValueSetter(value)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#Region " BUFFERING "

    ''' <summary> Gets or sets the finite buffering mode.  When true, the system collects the specified
    ''' <see cref="BuffersCount">number of buffers</see> and stops with a buffer done event. </summary>
    ''' <value> The finite buffering. </value>
    <Description("Toggles the continuous (infinite) buffering mode."), Category("Appearance"), DefaultValue(False)>
    Public Property FiniteBuffering() As Boolean
        Get
            Return Not Me._ContinuousBufferingCheckBox.Checked
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.FiniteBuffering Then
                Me._ContinuousBufferingCheckBox.SafeCheckedSetter(Not value)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

    ''' <summary> Size of the USB buffer. </summary>
    Private _usbBufferSize As Integer

    ''' <summary> Gets the number of samples allocated per USB buffer. Data collection is accomplished
    ''' in USB buffer sizes.  Thus, the buffer done event is invoked only upon completion of the last
    ''' USB buffer required to collect the sample buffer size.  If the sample buffer size is smaller
    ''' than a USB buffer, the board collects as many sample buffers as would fit into the USB buffer
    ''' size and then issues as many buffer done events as buffers collected. </summary>
    ''' <value> The size of the USB buffer. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property UsbBufferSize() As Integer
        Get
            Return Me._usbBufferSize
        End Get
    End Property

    ''' <summary> Gets or set the smart buffering option.  With smart buffering a buffer size of
    ''' multiples of the USB buffer size is allocated.  In strip chart smart buffering mode, a
    ''' channel buffer size of at least <see cref="UsbBufferSize">USB buffer size</see>
    ''' is allocated. </summary>
    ''' <value> The using smart buffering. </value>
    <Description("Gets or set the smart buffering option."), Category("Data"), DefaultValue(True)>
    Public Property UsingSmartBuffering() As Boolean
        Get
            Return Me._SmartBufferingCheckBox.Checked
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.UsingSmartBuffering Then
                Me._SmartBufferingCheckBox.SafeCheckedSetter(value)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
        End Set
    End Property

#End Region

#End Region

#Region " ANALOG INPUT ACQUISITION "

    ''' <summary> The sample counter. </summary>
    Private _sampleCounter As Long

    ''' <summary> Returns the total number of counts since the chart started. </summary>
    ''' <value> The sample counter. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SampleCounter() As Long
        Get
            Return Me._sampleCounter
        End Get
    End Property

#End Region

#Region " CALCULATION CONFIGURATION "

    ''' <summary> Gets or sets the option for calculating the average voltage. </summary>
    ''' <value> The calculate enabled. </value>
    <Description("Toggles the status for calculating averages."), Category("Data"), DefaultValue(False)>
    Public Property CalculateEnabled() As Boolean
        Get
            Return Me._CalculateAverageCheckBox.Checked
        End Get
        Set(ByVal Value As Boolean)
            If Value <> Me.CalculateEnabled Then
                Me._CalculateAverageCheckBox.SafeCheckedSetter(Value)
            End If
        End Set
    End Property

#End Region

#Region " CALCULATION OPERATION "

    ''' <summary> Gets or sets the calculation period down counts. </summary>
    ''' <value> The calculate period down counts. </value>
    Private Property calculatePeriodDownCounts As Long

    ''' <summary> Gets or sets the number of samples between calculate events. </summary>
    ''' <value> The number of calculate periods. </value>
    <Description("Gets or sets the number of samples between calculation updates."), Category("Data"), DefaultValue(10.0)>
    Public Property CalculatePeriodCount() As Integer

    ''' <summary> The last averages. </summary>
    Private _lastAverages() As Double = {}

    ''' <summary> Returns the last average saved for each channel. </summary>
    ''' <returns> The last average saved for each channel. </returns>
    Public Function LastAverages() As Double()
        Return Me._lastAverages
    End Function

    ''' <summary> The last voltages. </summary>
    Private _lastVoltages() As Double = {}

    ''' <summary> Returns the last voltages saved for each channel. </summary>
    ''' <returns> Te last voltages saved for each channel.  </returns>
    Public Function LastVoltages() As Double()
        Return Me._lastVoltages
    End Function

#End Region

#Region " CHART CONFIGURATION "

    ''' <summary> Gets or sets the chart axes color. </summary>
    ''' <value> The color of the axes. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property AxesColor() As System.Drawing.Color

        Get
            Return Me.Chart.AxesColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Me.Chart.DisableRendering()
            Me.Chart.AxesColor = value
            Me.Chart.EnableRendering()
            Me.Chart.SignalUpdate()
        End Set
    End Property

    ''' <summary> Gets or sets reference to the chart control. </summary>
    ''' <value> The chart. </value>
    Public ReadOnly Property Chart() As OpenLayers.Controls.Display
        Get
            Return Me._Chart
        End Get
    End Property

    ''' <summary> Gets or sets the <see cref="OpenLayers.Controls.BandMode">band mode</see>.  In a
    ''' single band all channels are displayed on a single graph. In multi bank, each channel is
    ''' allocated its own graph. </summary>
    ''' <value> The is multi-band mode. </value>
    <Description("Gets or sets the chart band mode.  Multi Band means separate chart for each signal"),
    Category("Appearance"), DefaultValue(False)>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Multi")>
    Public Property IsMultibandMode() As Boolean

        Get
            Return Me.Chart.BandMode = OpenLayers.Controls.BandMode.MultiBand
        End Get
        Set(ByVal value As Boolean)
            If value Then
                Me.ChartBandMode = OpenLayers.Controls.BandMode.MultiBand
            Else
                Me.ChartBandMode = OpenLayers.Controls.BandMode.SingleBand
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the <see cref="OpenLayers.Controls.BandMode">band mode</see>.  In a
    ''' single band all channels are displayed on a single graph. In multi bank, each channel is
    ''' allocated its own graph.  This property is not exposed directly so as not to require the
    ''' hosting assembly to directly reference the <see cref="OpenLayers.Controls">controls</see>
    ''' assembly. </summary>
    ''' <value> The chart band mode. </value>
    Private Property ChartBandMode() As OpenLayers.Controls.BandMode

        Get
            Return Me.Chart.BandMode
        End Get
        Set(ByVal value As OpenLayers.Controls.BandMode)
            Me.Chart.DisableRendering()
            Me.Chart.BandMode = value
            Me.Chart.EnableRendering()
            Me.Chart.SignalUpdate()
            If Me._SingleRadioButton.Checked Xor (Me.Chart.BandMode = OpenLayers.Controls.BandMode.SingleBand) Then
                Me._SingleRadioButton.SafeSilentCheckedSetter(Me.Chart.BandMode = OpenLayers.Controls.BandMode.SingleBand)
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the chart footer. </summary>
    ''' <value> The chart footer. </value>
    <Description("Gets or sets the chart footer."), Category("Appearance"), DefaultValue("")>
    Public Property ChartFooter() As String

        Get
            Return Me._ChartFooterTextBox.Text
        End Get
        Set(ByVal value As String)
            If value <> Me.ChartFooter Then
                Me._ChartFooterTextBox.SafeTextSetter(value)
            End If
            If Me.Chart IsNot Nothing Then
                Me.Chart.Footer = value
            End If
            If String.IsNullOrWhiteSpace(value) Then
                Me._ToolTip.SetToolTip(Me._ChartFooterTextBox, "Chart Footer: Empty")
            Else
                Me._ToolTip.SetToolTip(Me._ChartFooterTextBox, value)
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the <see cref="ChartModeId">scope or strip chart modes</see>. </summary>
    ''' <value> The chart mode. </value>
    <Description("Gets or sets the chart mode"), Category("Appearance"), DefaultValue(ChartModeId.Scope)>
    Public Property ChartMode() As ChartModeId

        Get
            If Me._ScopeRadioButton.Checked Then
                Return ChartModeId.Scope
            Else
                Return ChartModeId.StripChart
            End If
        End Get
        Set(ByVal value As ChartModeId)
            If value <> Me.ChartMode Then
                Me._ScopeRadioButton.SafeSilentCheckedSetter(value = ChartModeId.Scope)
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            End If
            Me.enableChannelsControls()
            Me.enableSignalsControls()
        End Set
    End Property

    ''' <summary> Gets or sets the chart title. </summary>
    ''' <value> The chart title. </value>
    <Description("Gets or sets the chart title."), Category("Appearance"), DefaultValue("Analog Input Time Series")>
    Public Property ChartTitle() As String

        Get
            Return Me._ChartTitleTextBox.Text
        End Get
        Set(ByVal value As String)
            If value <> Me.ChartTitle Then
                Me._ChartTitleTextBox.SafeTextSetter(value)
            End If
            If Me.Chart IsNot Nothing Then
                Me.Chart.Title = value
            End If
            If String.IsNullOrWhiteSpace(value) Then
                Me._ToolTip.SetToolTip(Me._ChartTitleTextBox, "Chart Title: Empty")
            Else
                Me._ToolTip.SetToolTip(Me._ChartTitleTextBox, value)
            End If
        End Set
    End Property

    ''' <summary> Gets or sets or set the charting enabled status. </summary>
    ''' <value> The charting enabled. </value>
    <Description("Toggles the charting of sampled data."), Category("Appearance"), DefaultValue(True)>
    Public Property ChartingEnabled() As Boolean

        Get
            Return Me._EnableChartingCheckBox.Checked
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.ChartingEnabled Then
                Me._EnableChartingCheckBox.SafeCheckedSetter(value)
            End If
        End Set
    End Property

    ''' <summary>Gets or sets the available signal colors.</summary>
    Private _defaultSignalColors As System.Drawing.Color() = {System.Drawing.Color.Red, System.Drawing.Color.Blue,
                                                              System.Drawing.Color.Magenta, System.Drawing.Color.Lime,
                                                              System.Drawing.Color.Green, System.Drawing.Color.Orange}

    ''' <summary> Gets or sets the default signal color. </summary>
    ''' <value> The default signal color. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property DefaultSignalColor(ByVal index As Integer) As System.Drawing.Color
        Get
            index = CInt(Math.Max(0, Math.Min(index, Me._defaultSignalColors.Length - 1)))
            Return Me._defaultSignalColors(index)
        End Get
        Set(ByVal value As System.Drawing.Color)
            index = CInt(Math.Max(0, Math.Min(index, Me._defaultSignalColors.Length - 1)))
            Me._defaultSignalColors(index) = value
        End Set
    End Property

    ''' <summary> Gets or sets the option for displaying the average voltage reading. </summary>
    ''' <value> The display average enabled. </value>
    <Description("Toggles the average voltage status display."), Category("Appearance"), DefaultValue(False)>
    Public Property DisplayAverageEnabled() As Boolean
        Get
            Return Me._DisplayAverageCheckBox.Checked
        End Get
        Set(ByVal Value As Boolean)
            If Value <> Me.DisplayAverageEnabled Then
                Me._DisplayAverageCheckBox.SafeCheckedSetter(Value)
            End If
            If Value Then
                If Me.selectedSignal Is Nothing Then
                    Me._AverageVoltageToolStripLabel.ToolTipText = "Average"
                Else
                    Me._AverageVoltageToolStripLabel.ToolTipText = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                                 "{0} Average", Me.selectedSignal.Name)
                End If
            Else
                Me._AverageVoltageToolStripLabel.Text = ""
                Me._AverageVoltageToolStripLabel.ToolTipText = "Average: Off"
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the option for displaying the voltage reading. </summary>
    ''' <value> The display voltage enabled. </value>
    <Description("Toggles the voltage reading display."), Category("Appearance"), DefaultValue(False)>
    Public Property DisplayVoltageEnabled() As Boolean
        Get
            Return Me._DisplayVoltageCheckBox.Checked
        End Get
        Set(ByVal Value As Boolean)
            If Value <> Me._DisplayVoltageCheckBox.Checked Then
                Me._DisplayVoltageCheckBox.SafeCheckedSetter(Value)
            End If
            If Value Then
                If Me.selectedSignal Is Nothing Then
                    Me._CurrentVoltageToolStripLabel.ToolTipText = "Current Voltage"
                Else
                    Me._CurrentVoltageToolStripLabel.ToolTipText = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                                 "{0} voltage", Me.selectedSignal.Name)
                End If
            Else
                Me._CurrentVoltageToolStripLabel.Text = ""
                Me._CurrentVoltageToolStripLabel.ToolTipText = "Current Voltage: Off"
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the chart grid color. </summary>
    ''' <value> The color of the grid. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property GridColor() As System.Drawing.Color

        Get
            Return Me.Chart.GridColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Me.Chart.DisableRendering()
            Me.Chart.GridColor = value
            Me.Chart.EnableRendering()
            Me.Chart.SignalUpdate()
        End Set
    End Property

    ''' <summary> Set the color of the selected signal. </summary>
    ''' <value> The color of the selected signal. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property SelectedSignalColor() As System.Drawing.Color

        Get
            If Me.selectedSignal IsNot Nothing Then
                Return Me.Chart.GetCurveColor(Me.selectedSignalIndex)
            Else
                Return System.Drawing.Color.Blue
            End If
        End Get
        Set(ByVal value As System.Drawing.Color)
            If Me.selectedSignalIndex >= 0 Then
                Me.Chart.DisableRendering()

                ' set the color
                Me.Chart.SetCurveColor(Me.selectedSignalIndex, value)

                ' update the chart
                Me.Chart.SignalListUpdate()

                Me.Chart.EnableRendering()

                ' update the list of signals.
                Me.updateSignalList()
            End If
        End Set
    End Property

    ''' <summary> The update period down counter. </summary>
    Private _updatePeriodDownCounter As Long

    ''' <summary> Gets or sets the number of samples between update events. </summary>
    ''' <value> The number of update periods. </value>
    <Description("Gets or sets the number of samples between update events."), Category("Data"), DefaultValue(10.0)>
    Public Property UpdatePeriodCount() As Integer

    ''' <summary> Gets or sets the option for calculating the average voltage. </summary>
    ''' <value> The update event enabled. </value>
    <Description("Toggles the status for update events."), Category("Data"), DefaultValue(False)>
    Public Property UpdateEventEnabled() As Boolean

    ''' <summary> The volts readings string format. </summary>
    Private _voltsReadingsStringFormat As String
    ''' <summary> The volts reading format. </summary>
    Private _voltsReadingFormat As String

    ''' <summary> Gets or sets the format for displaying the voltage. </summary>
    ''' <value> The volts reading format. </value>
    <Description("Formats the voltage reading display."), Category("Appearance"), DefaultValue("0.000")>
    Public Property VoltsReadingFormat() As String
        Get
            Return Me._voltsReadingFormat
        End Get
        Set(ByVal value As String)
            Me._voltsReadingFormat = value
            Me._voltsReadingsStringFormat = "{0:" & value & " v}"
        End Set
    End Property

#End Region

#Region " CHART DISPLAY "

    ''' <summary> Gets or sets the time axis period in seconds or milliseconds. </summary>
    ''' <value> The time axis period. </value>
    Private Property timeAxisPeriod As Double

    ''' <summary> Gets or sets the time axis duration in seconds or milliseconds. </summary>
    ''' <value> The time axis duration. </value>
    Private Property timeAxisDuration As Double

    ''' <summary> Scroll the time frame of the chart. </summary>
    Private Sub scrollTimeFrame()
        Dim newScrollValue As UInt64 = CULng(Me.SampleCounter - Me.SignalBufferSize)
        If newScrollValue < 0 Then
            newScrollValue = 0
        End If
        scrollTimeFrame(newScrollValue)
    End Sub

    ''' <summary> Scroll the time frame of the chart. </summary>
    ''' <param name="newScrollValue"> . </param>
    Private Sub scrollTimeFrame(ByVal newScrollValue As UInt64)

        Dim startTime As Double = newScrollValue * Me.timeAxisPeriod
        Dim endTime As Double = startTime + Me.timeAxisDuration

        ' setup range of x-axis
        Me.Chart.XDataCurrentRangeMin = startTime
        Me.Chart.XDataCurrentRangeMax = endTime
        Me.Chart.XDataRangeMin = startTime
        Me.Chart.XDataRangeMax = endTime

    End Sub

#End Region

#Region " VALIDATE AND CONFIGURE "

    ''' <summary> <c>True</c> if analog input configuration required. </summary>
    Private _AnalogInputConfigurationRequired As Boolean

    ''' <summary> Gets or sets a value indicating whether [analog input configuration required].
    ''' Disables start when set True. </summary>
    ''' <value> <c>True</c> if [analog input configuration required]; otherwise, <c>False</c>. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property AnalogInputConfigurationRequired As Boolean

        Get
            Return Me._AnalogInputConfigurationRequired OrElse
                   OpenLayers.Base.SubsystemBase.States.ConfiguredForContinuous <> Me.AnalogInputState
        End Get
        Private Set(value As Boolean)
            Me._AnalogInputConfigurationRequired = value
            If value Then
                Me.StartStopEnabled = False
            End If
        End Set
    End Property

    ''' <summary> <c>True</c> if chart configuration required. </summary>
    Private _ChartConfigurationRequired As Boolean

    ''' <summary> Gets or sets a value indicating whether [chart configuration required]. Disables
    ''' start when set True. </summary>
    ''' <value> <c>True</c> if [chart configuration required]; otherwise, <c>False</c>. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ChartConfigurationRequired As Boolean

        Get
            Return Me._ChartConfigurationRequired
        End Get
        Set(value As Boolean)
            Me._ChartConfigurationRequired = value
            If value Then
                Me.StartStopEnabled = False
            End If
        End Set
    End Property

    ''' <summary> Gets or sets a value indicating whether [configuration required]. </summary>
    ''' <value> <c>True</c> if [configuration required]; otherwise, <c>False</c>. </value>
    Public ReadOnly Property ConfigurationRequired As Boolean

        Get
            Return Me.AnalogInputConfigurationRequired OrElse Me.ChartConfigurationRequired
        End Get
    End Property

    ''' <summary> Validates the configuration. </summary>
    ''' <param name="details"> [in,out] A non-empty validation outcome string containing the reason
    ''' validation failed. </param>
    ''' <returns> <c>True</c> if validated; otherwise, <c>False</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="0#")>
    Public Function ValidateConfiguration(ByRef details As String) As Boolean

        Dim outcomeBuilder As New System.Text.StringBuilder
        If Not Me.IsOpen Then
            outcomeBuilder.Append("Board not initialized (not 'open')")
        ElseIf Not Me.AnalogInput.HasChannels Then
            ' configure only if we have defined channels.
            outcomeBuilder.Append("The list of analog input channels is empty.")
        Else
            If Me.SamplingRate > Me.AnalogInput.Clock.MaxFrequency Then
                Me.enunciate(Me._SamplingRateNumeric, "Requested sampling rate {0} is larger than the maximum {1}",
                             Me.SamplingRate, Me.AnalogInput.Clock.MaxFrequency)
                outcomeBuilder.AppendLine("Requested sampling rate {0} is larger than the maximum {1}",
                                             Me.SamplingRate, Me.AnalogInput.Clock.MaxFrequency)
            End If

            If Me.BuffersCount < Me.MinimumBuffersCount Then
                enunciate(Me._BuffersCountNumeric, "Requested number of channel buffers {0} is lower than the minimum {1}",
                                             Me.BuffersCount, Me.MinimumBuffersCount)
                outcomeBuilder.AppendLine("Requested number of channel buffers {0} is lower than the minimum {1}",
                                             Me.BuffersCount, Me.MinimumBuffersCount)
            ElseIf Me.BuffersCount > Me.MaximumBuffersCount Then
                enunciate(Me._BuffersCountNumeric, "Requested number of channel buffers {0} is greater than the maximum {1}",
                                             Me.BuffersCount, Me.MaximumBuffersCount)
                outcomeBuilder.AppendLine("Requested number of channel buffers {0} is greater than the maximum {1}",
                                             Me.BuffersCount, Me.MaximumBuffersCount)
            End If

            If Me.MovingAverageLength < Me.MinimumMovingAverageLength Then
                enunciate(Me._MovingAverageLengthNumeric, "Requested length of moving average {0} is lower than the minimum {1}",
                                             Me.MovingAverageLength, Me.MinimumMovingAverageLength)
                outcomeBuilder.AppendLine("Requested length of moving average {0} is lower than the minimum {1}",
                                             Me.MovingAverageLength, Me.MinimumMovingAverageLength)
            ElseIf Me.SignalBufferSize > 0 AndAlso Me.MovingAverageLength > Me.SignalBufferSize Then
                enunciate(Me._MovingAverageLengthNumeric, "Requested length of moving average {0} is greater than the signal buffer length {1}",
                                             Me.MovingAverageLength, Me.SignalBufferSize)
                outcomeBuilder.AppendLine("Requested length of moving average {0} is greater than the signal buffer length {1}",
                                             Me.MovingAverageLength, Me.SignalBufferSize)
            End If

            If Me.SampleSize < Me.MinimumSampleSize Then
                enunciate(Me._SampleSizeNumeric, "Requested size of sample buffer {0} is lower than the minimum {1}",
                           Me.SampleSize, Me.MinimumSampleSize)
                outcomeBuilder.AppendLine("Requested size of sample buffer {0} is lower than the minimum {1}",
                                             Me.SampleSize, Me.MinimumSampleSize)
            End If

            If Me.SamplingRate > Me.AnalogInput.Clock.MaxFrequency Then
                Me.enunciate(Me._SamplingRateNumeric, "Requested sampling rate {0} is larger than the minimum {1}",
                              Me.SamplingRate, Me.AnalogInput.Clock.MaxFrequency)
                outcomeBuilder.AppendLine("Requested sampling rate {0} is larger than the minimum {1}",
                                             Me.SamplingRate, Me.AnalogInput.Clock.MaxFrequency)
            End If

            If Me.SignalBufferSize < Me.MinimumSignalBufferSize Then
                Me.enunciate(Me._SignalBufferSizeNumeric, "Signal buffer size {0} must be larger than {1}",
                              Me.SignalBufferSize, Me.MinimumSignalBufferSize)
                outcomeBuilder.AppendLine("Signal buffer size {0} must be larger than {1}",
                                             Me.SignalBufferSize, Me.MinimumSignalBufferSize)
            End If

            If Me.SignalMemoryLength < Me.SignalBufferSize Then
                Me.enunciate(Me._SignalMemoryLengthNumeric, "Signal memory length {0} must be larger than signal buffer length {1}",
                              Me.SignalMemoryLength, Me.SignalBufferSize)
                outcomeBuilder.AppendLine("Signal memory length {0} must be larger than signal buffer length {1}",
                                             Me.SignalMemoryLength, Me.SignalBufferSize)
            End If

            If Me.RefreshRate > Me.MaximumRefreshRate Then
                Me.enunciate(Me._RefreshRateNumeric, "Refresh rate {0} exceeds maximum of {1}",
                              Me.RefreshRate, Me.MaximumRefreshRate)
            ElseIf Me.RefreshRate < Me.MinimumRefreshRate Then
                Me.enunciate(Me._RefreshRateNumeric, "Refresh rate {0} is below the minimum of {1}",
                              Me.RefreshRate, Me.MinimumRefreshRate)
            End If
        End If

        If outcomeBuilder.Length > 0 Then
            details = outcomeBuilder.ToString
            Me._errorProvider.SetError(Me._ConfigureButton, details)
            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary> Configures analog inputs and signals. Disables start if all clear. </summary>
    ''' <param name="details"> [in,out] A non-empty validation outcome string containing the reason
    ''' configuration failed. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Private Function _TryConfigure(ByRef details As String) As Boolean

        If Me.ConfigurationRequired Then
            If Not Me.ValidateConfiguration(details) Then
                Me._errorProvider.SetError(Me._ConfigureButton, details)
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Validation failed;. Details: {0}", details)
                Return False
            End If
        End If

        Me._errorProvider.Clear()

        If Me.AnalogInputConfigurationRequired Then
            If Not Me.TryConfigureAnalogInput(details) Then
                Me._errorProvider.SetError(Me._ConfigureButton, details)
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Analog input configuration failed;. Details: {0}", details)
                Return False
            End If
        End If

        If Me.ChartConfigurationRequired Then
            If Not Me.TryConfigureChart(details) Then
                Me._errorProvider.SetError(Me._ConfigureButton, details)
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Chart configuration failed;. Details: {0}", details)
                Return False
            End If
        End If
        Return True

    End Function

    ''' <summary> Configures the analog input. </summary>
    ''' <param name="details"> [in,out] A non-empty validation outcome string containing the reason
    ''' configuration failed. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="0#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function TryConfigureAnalogInput(ByRef details As String) As Boolean

        Dim outcomeBuilder As New System.Text.StringBuilder

        If Me.ConfigureSuspended Then
            outcomeBuilder.Append("Configuration suspended")
        ElseIf Not Me.IsOpen Then
            outcomeBuilder.Append("Board not initialized (not 'open')")
        ElseIf Not Me.AnalogInput.HasChannels Then
            ' configure only if we have defined channels.
            outcomeBuilder.Append("The list of analog input channels is empty.")
        End If

        ' turn off the configuration required sentinel.
        Me.AnalogInputConfigurationRequired = False


        ReDim _lastAverages(Me.AnalogInput.ChannelList.Count - 1)
        Array.Clear(Me._lastAverages, 0, Me._lastAverages.Length)

        ReDim _lastVoltages(Me.AnalogInput.ChannelList.Count - 1)
        Array.Clear(Me._lastAverages, 0, Me._lastAverages.Length)

        Try

            Me.SuspendConfig()

            Dim desiredClockFrequency As Double = Me.SamplingRate
            Me._decimationRatio = 1

            If Me.SamplingRate > Me.AnalogInput.Clock.MaxFrequency Then
                enunciate(Me._SamplingRateNumeric, "Requested sampling rate {0} is larger than the maximum {1}.",
                          Me.SamplingRate, Me.AnalogInput.Clock.MaxFrequency)
                outcomeBuilder.AppendLine("Requested sampling rate {0} is larger than the maximum {1}.",
                                          Me.SamplingRate, Me.AnalogInput.Clock.MaxFrequency)
            End If

            ' set channel buffer size to sample size unless we have some special conditions.
            Me._channelBufferSize = Me.SampleSize

            ' set the sample buffer size based on the number of channels
            Me._sampleBufferSize = Me._channelBufferSize * Me.AnalogInput.ChannelList.Count

            If Me.ChartMode = ChartModeId.StripChart Then

                If Me.UsingSmartBuffering Then

                    ' with smart buffering each sample buffer must be at least 256 points
                    If Me.SampleBufferSize < Me.UsbBufferSize Then

                        ' set channel buffer size to the use buffer at least minimum sampling rate.
                        Me._decimationRatio = CInt(Math.Max(Math.Ceiling(Me._usbBufferSize / Me.SampleBufferSize),
                                                            Math.Ceiling(Me.AnalogInput.Clock.MinFrequency / Me.SamplingRate)))

                        ' set the sampling rate to reflect the increase in channel buffer size
                        desiredClockFrequency = Me._decimationRatio * Me.SamplingRate
                        Me._channelBufferSize = CInt(Me._decimationRatio * Me.SampleSize)

                        If desiredClockFrequency > Me.AnalogInput.Clock.MaxFrequency Then
                            enunciate(Me._SampleSizeNumeric,
                                      "Decimated sampling rate {0} is larger than the maximum {1}. Either use simple buffering or increase the sample size.",
                                       Me.SamplingRate, Me.AnalogInput.Clock.MaxFrequency)
                            outcomeBuilder.AppendLine("Decimated sampling rate {0} is larger than the maximum {1}. Either use simple buffering or increase the sample size.",
                                                      Me.SamplingRate, Me.AnalogInput.Clock.MaxFrequency)
                        End If

                    Else

                        If desiredClockFrequency < Me.AnalogInput.Clock.MinFrequency Then

                            ' if sampling rate lower than the minimum clock frequency then we need to decimate.
                            Me._decimationRatio = CInt(Math.Ceiling((Me.AnalogInput.Clock.MinFrequency + 1) / Me.SamplingRate))
                            desiredClockFrequency = Me._decimationRatio * Me.SamplingRate
                            Me._channelBufferSize = CInt(Me._decimationRatio * Me.SampleSize)

                        End If

                    End If

                End If

                Me.SignalBufferSize = Math.Max(Me.SampleSize, Me.SignalBufferSize)

                ' the signal memory length must be longer than the buffer size.
                Me.SignalMemoryLength = Math.Max(Me.SignalMemoryLength, Me.SignalBufferSize)

            Else

                If Me.SamplingRate < Me.AnalogInput.Clock.MinFrequency Then

                    ' if sampling rate lower than the minimum clock frequency then we need to decimate.
                    Me._decimationRatio = CInt(Math.Ceiling((Me.AnalogInput.Clock.MinFrequency + 1) / Me.SamplingRate))
                    desiredClockFrequency = Me._decimationRatio * Me.SamplingRate
                    Me._channelBufferSize = CInt(Me._decimationRatio * Me.SampleSize)

                End If

                ' for scope we force the signal buffer size to equal the sample size.
                Me.SignalBufferSize = Me.SampleSize

                ' the signal memory length is the same as the buffer length.
                Me.SignalMemoryLength = Me.SignalBufferSize

            End If

            ' turn off the configuration required sentinel.
            Me.AnalogInputConfigurationRequired = False

            ' update the sample buffer size based on the number of channels
            Me._sampleBufferSize = Me.ChannelBufferSize * Me.AnalogInput.ChannelList.Count

            ' reallocate other value based on these calculations
            Me.MovingAverageLength = Math.Min(Me.MovingAverageLength, Me.SignalBufferSize)

            ' set the clock frequency
            Me.ClockRate = desiredClockFrequency

            ' configure the analog input
            Me.AnalogInput.Config()

            ' adjust the actual sampling rate based on the A/D clock frequency.
            Me.SamplingRate = Me.ClockRate / Me._decimationRatio

            ' free then allocate buffers
            Me.AnalogInput.AllocateBuffers(Me.BuffersCount, Me.SampleBufferSize)

            ' Me._bufferPeriod = Me.samplingPeriod * Me.ChannelBufferSize

            ' allocate buffers.
            Me.AnalogInput.AllocateBuffers(Me.BuffersCount, Me.SampleBufferSize)

            ' check if the configuration sentinel was turned on indicating the some values are inconsistent with the user required values
            If Me.AnalogInputConfigurationRequired Then
                outcomeBuilder.AppendLine("Analog input configuration altered user set value and needs to be repeated.")
            End If

            If outcomeBuilder.Length > 0 Then
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Analog Input configuration failed;. Details: {0}", outcomeBuilder.ToString)
            Else
                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Analog Input Configured;. ")
            End If

        Catch ex As Exception

            outcomeBuilder.AppendLine("Exception occurred;. Details: {0}", ex.ToString)

        Finally

            Me.ResumeConfig()


        End Try

        If outcomeBuilder.Length > 0 Then
            details = outcomeBuilder.ToString
            Return False
        Else
            Return True
        End If

    End Function

    ''' <summary> Configures the analog input. </summary>
    ''' <param name="details"> [in,out] A non-empty validation outcome string containing the reason
    ''' configuration failed. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="0#")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function TryConfigureChart(ByRef details As String) As Boolean

        Dim outcomeBuilder As New System.Text.StringBuilder

        If Me.ConfigureSuspended Then

            outcomeBuilder.Append("Configuration suspended")

        ElseIf Me.Chart.Signals.Count = 0 Then

            ' configure only if we have defined channels.
            outcomeBuilder.Append("The chart has no signal defined.")

        Else
            ' turn off the sentinel
            Me._ChartConfigurationRequired = False


            Try

                refreshTimespan = TimeSpan.FromSeconds(1 / Me.RefreshRate)

                ' stop the scope display.
                Me.Chart.DisableRendering()

                ' suspend configuration
                Me.SuspendConfig()

                ' set the chart duration to display one whole signal.
                Dim chartDuration As Double = Me.SignalBufferSize / Me.SamplingRate

                ' set chart for seconds or milliseconds
                If (chartDuration - 1) < 0.1 Then
                    Me.timeAxisDuration = chartDuration * 1000
                    Me.timeAxisPeriod = Me.SamplingPeriod * 1000
                    Me.Chart.XDataUnit = "Milliseconds"
                Else
                    Me.timeAxisDuration = chartDuration
                    Me.timeAxisPeriod = Me.SamplingPeriod
                    Me.Chart.XDataUnit = "Seconds"
                End If

                ' allocate signal size for the chart.
                Me.Chart.SignalBufferLength = Me.SignalBufferSize

                ' setup time axis title
                Me.Chart.XDataName = "Time"

                ' setup range of x-axis
                scrollTimeFrame(0)

                ' check if the configuration sentinel was turned on indicating the some values are inconsistent with the user required values
                If Me.ChartConfigurationRequired Then
                    outcomeBuilder.AppendLine("Chart configuration altered user set value and needs to be repeated.")
                End If

                If outcomeBuilder.Length > 0 Then
                    Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Chart configuration failed;. Details: {0}", outcomeBuilder.ToString)
                Else
                    Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Chart Configured;. ")
                End If

            Catch ex As Exception

                outcomeBuilder.AppendLine("Exception occurred;. Details: {0}", ex.ToString)

            Finally

                Me.ResumeConfig()

                ' resume the display.
                Me.Chart.EnableRendering()

                ' notify chart that data is available
                Me.Chart.SignalUpdate()

            End Try

        End If

        If outcomeBuilder.Length > 0 Then
            details = outcomeBuilder.ToString
            Return False
        Else
            Return True
        End If

    End Function

    ''' <summary> Shows or hides the configuration panel. </summary>
    ''' <value> The configuration panel visible. </value>
    <Description("Shows or hides the configuration panel"), Category("Appearance"), DefaultValue(True)>
    Public Property ConfigurationPanelVisible() As Boolean

        Get
            Return Me._Tabs.Visible
        End Get
        Set(ByVal value As Boolean)
            Me._Tabs.SafeVisibleSetter(value)
            Me._ConfigureToolStripMenuItem.SafeSilentCheckedSetter(value)
        End Set
    End Property

    ''' <summary> Gets or set the status of configuration suspension. </summary>
    ''' <value> A Queue of configure suspensions. </value>
    Private Property configureSuspensionQueue As Generic.Queue(Of Boolean)

    ''' <summary> Returns true if configuration is suspended. </summary>
    ''' <value> The configure suspended. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ConfigureSuspended() As Boolean
        Get
            Return Me.configureSuspensionQueue.Count > 0
        End Get
    End Property

    ''' <summary> Resumes configuration. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Sub ResumeConfig()
        If Me.ConfigureSuspended Then
            Me.configureSuspensionQueue.Dequeue()
        Else
            Throw New System.InvalidOperationException("Attempted to resume configuration that was not suspended.")
        End If
    End Sub

    ''' <summary> Suspends configuration to allow setting a few properties without reconfiguring. </summary>
    Public Sub SuspendConfig()
        Me.configureSuspensionQueue.Enqueue(True)
    End Sub

    ''' <summary> Resumes configuration without an exception. </summary>
    ''' <returns> <c>True</c> if resumed, <c>False</c> if not suspended. </returns>
    Public Function TryResumeConfig() As Boolean
        If Me.ConfigureSuspended Then
            Me.ResumeConfig()
            Return True
        Else
            Return False
        End If
    End Function

#End Region

#Region " GUI "

    ''' <summary> Shows or hides the action menu. </summary>
    ''' <value> The action menu visible. </value>
    <Description("Shows or hides the actions menu"), Category("Appearance"), DefaultValue(True)>
    Public Property ActionMenuVisible() As Boolean
        Get
            Return Me._ActionsToolStripDropDownButton.Visible
        End Get
        Set(ByVal value As Boolean)
            Me._ActionsToolStripDropDownButton.SafeVisibleSetter(value)
        End Set
    End Property

    ''' <summary> Enunciates the specified control. </summary>
    ''' <param name="control"> The control. </param>
    ''' <param name="format">  The format. </param>
    ''' <param name="args">    The arguments. </param>
    Private Sub enunciate(ByVal control As Control, ByVal format As String, ByVal ParamArray args() As Object)
        Me._errorProvider.SetError(control, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="Core.Diagnosis.TraceMessage">message</see> to display and log. </param>
    Protected Overrides Sub DisplayMessage(value As Core.Diagnosis.TraceMessage)
        If value IsNot Nothing Then
            Me._ChartToolStripStatusLabel.ToolTipText = value.Details
        End If
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected Overrides Sub DisplaySynopsis(value As String)
        Me._ChartToolStripStatusLabel.Text = value
    End Sub

#End Region

#Region " DATA METHODS AND PROPERTIES  "

    ''' <summary> Gets or sets the pointer to the most recent (current) data point acquired.
    ''' For a scope display this is the last data point of the signal.
    ''' For a strip chart this is the last data point that was added to the signal. </summary>
    Private _loopPointer As Integer

    ''' <summary> Allocates storage for voltages. </summary>
    ''' <param name="channels">         Number of channels. </param>
    ''' <param name="pointsPerChannel"> Number of samples per channel. </param>
    ''' <returns> The Voltages </returns>
    Public Function AllocateVoltages(ByVal channels As Integer, ByVal pointsPerChannel As Integer) As Double()()

        Me._voltages = New Double(channels - 1)() {}
        For i As Integer = 0 To channels - 1
            Me._voltages(i) = New Double(pointsPerChannel - 1) {}
        Next i
        Return Me._voltages

    End Function

    ''' <summary> The voltages. </summary>
    Private _voltages()() As Double

    ''' <summary> Returns a read only reference to the voltage array. </summary>
    ''' <returns> The Voltages </returns>
    Public Function Voltages() As Double()()
        Return Me._voltages
    End Function

    ''' <summary> Returns a read only reference to the voltage array. </summary>
    ''' <param name="channelIndex"> Specifies the channel index. </param>
    ''' <returns> The Voltages </returns>
    Public Function Voltages(ByVal channelIndex As Integer) As Double()
        Return Me._voltages(channelIndex)
    End Function

    ''' <summary> Returns the current voltage of the specifies channel. </summary>
    ''' <param name="channelIndex"> Specifies the channel index. </param>
    ''' <returns> The current voltage of the specifies channel </returns>
    Public Function CurrentVoltage(ByVal channelIndex As Integer) As Double
        If Me._loopPointer >= 0 Then
            Return Me._voltages(channelIndex)(Me._loopPointer)
        Else
            Return 0
        End If
    End Function

    ''' <summary> Return a voltages array from the specified voltages. </summary>
    ''' <param name="channelIndex"> . </param>
    ''' <param name="pointCount">   Specifies the number of data points to return. </param>
    ''' <returns> Voltages array from the specified voltages. </returns>
    Public Function DataEpoch(ByVal channelIndex As Integer, ByVal pointCount As Integer) As Double()
        Return AnalogInputSubsystem.DataEpoch(Me.Voltages(channelIndex), Me._loopPointer, pointCount, Me.SampleCounter)
    End Function

    ''' <summary> Return a voltages array from the specified voltages. </summary>
    ''' <param name="dataArray">  . </param>
    ''' <param name="pointCount"> Specifies the number of data points to return. </param>
    ''' <returns> Voltages array from the specified voltages. </returns>
    Public Function DataEpoch(ByVal dataArray() As Double, ByVal pointCount As Integer) As Double()
        If pointCount <= 0 OrElse dataArray Is Nothing OrElse dataArray.Length = 0 Then
            Dim nullData As Double() = {}
            Return nullData
        End If
        Return AnalogInputSubsystem.DataEpoch(dataArray, Me._loopPointer, pointCount, Me.SampleCounter)
    End Function

#End Region

#Region " PROCESS BUFFER METHODS "

    ''' <summary>
    ''' Gets or sets a queue of <see cref="OpenLayers.Base.OlBuffer">Open Layers buffers</see>.
    ''' </summary>
    Private _bufferQueue As System.Collections.Generic.Queue(Of Global.OpenLayers.Base.OlBuffer)

    ''' <summary> Calculate and save the averages. </summary>
    Private Sub calculateAverages()

        For channelIndex As Integer = 0 To Me.AnalogInput.ChannelsCount - 1

            ' calculate the average of the selected data epoch.
            Me._lastAverages(channelIndex) = AnalogInputSubsystem.Average(Me.DataEpoch(channelIndex, Me.MovingAverageLength))

        Next

    End Sub

    ''' <summary> The chart volts locker. </summary>
    Private chartVoltsLocker As New Object
    ''' <summary> Charts the saved voltages. </summary>
    Private Sub chartVoltageTimeSeries()

        If Me.ChartingEnabled AndAlso Me.SampleCounter > 0 Then

            Try

                Me.Chart.DisableRendering()

                If Me.ChartMode = ChartModeId.Scope Then

                    Dim signalIndex As Integer = 0
                    For Each signal As OpenLayers.Signals.MemorySignal In Me.Chart.Signals
                        signal.Data = Me.Voltages(Me.signalLogicalChannelNumbers(signalIndex))
                        signalIndex += 1
                    Next

                ElseIf Me.ChartMode = ChartModeId.StripChart Then

                    For signalIndex As Integer = 0 To Me.Chart.Signals.Count - 1
                        Me.Chart.Signals.Item(signalIndex).Data = AnalogInputSubsystem.DataEpoch(
                                                                            Me.Voltages(Me.signalLogicalChannelNumbers(signalIndex)),
                                                                            Me._loopPointer, Me.SignalBufferSize, Me.SampleCounter)
                    Next

                    ' This slows down operation big time.  So it is not done. 
                    ' update the time frame
                    ' Me.scrollTimeFrame()

                End If

            Catch

                Throw

            Finally

                Me.Chart.EnableRendering()
                Me.Chart.SignalUpdate()

                ' set next display time.
                nextRefreshTime = DateTime.Now().Add(refreshTimespan)

            End Try

        End If

    End Sub

    ''' <summary> The decimation ratio. </summary>
    Private _decimationRatio As Integer

    ''' <summary> Gets the relative number of samples collected per sample selected. </summary>
    ''' <value> The decimation ratio. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property DecimationRatio() As Integer
        Get
            Return Me._decimationRatio
        End Get
    End Property

    ''' <summary> Display values for the current channel. </summary>
    Private Sub displayVoltages()

        If Me.DisplayAverageEnabled AndAlso Me.CalculateEnabled Then
            Me._AverageVoltageToolStripLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                  Me._voltsReadingsStringFormat, Me._lastAverages(Me.selectedChannelIndex))
        End If

        If Me.DisplayVoltageEnabled Then
            Me._CurrentVoltageToolStripLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                  Me._voltsReadingsStringFormat, Me._lastVoltages(Me.selectedChannelIndex))
        End If

    End Sub

    ''' <summary> Processes the buffer queue. </summary>
    ''' <remarks> Test results for scope display: 5 KHz, 1000 Samples, 5 buffers, 10 points average.
    ''' Storage   CALC           Display        Chart         Total 0.1388ms  off: 0.0156ms  off:
    ''' 0.0134ms  on:  0.045ms  0.2128ms 0.1449ms  off: 0.0162ms  v:   0.8381ms; on:  0.0411ms
    ''' 1.0403ms 0.1435ms  on:  0.0185ms  v:   0.0316ms; on:  0.0463ms 0.2399ms 0.1519ms  on:
    ''' 0.0185ms  v,a: 1.5622ms; on:  0.0441ms 1.7767ms 0.1385ms  on:  0.0187ms  v,a: 1.4083ms; off:
    ''' 0.0159ms 1.5814ms Strip Chart 200 Hz, 10 samples. Storage   CALC           Display
    ''' Chart         Total 0.1209ms  off:0.0165ms   off: 0.0137ms  on:  0.0841ms 0.2352ms 0.0385ms
    ''' 0.0154ms        0.0131ms       5.7326ms 5.7996ms. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub processBufferQueue()

        If Me._bufferQueue.Count <= 0 Then
            ' this allows simple repeating without having to worry about 
            ' an empty queue
            Exit Sub
        End If

        Dim buffer As Global.OpenLayers.Base.OlBuffer = Me._bufferQueue.Dequeue

        Try

            If buffer.ValidSamples >= Me.SampleBufferSize Then

                ' save buffered data
                Me.copyBuffer(buffer)

                If Me.calculatePeriodDownCounts <= 0 Then
                    ' reset the count
                    Me.calculatePeriodDownCounts = Me.CalculatePeriodCount
                    ' calculate averages as necessary
                    Me.calculateAverages()
                End If

                If Me._updatePeriodDownCounter <= 0 Then
                    Me._updatePeriodDownCounter = Me.UpdatePeriodCount
                    Me.OnUpdatePeriodDone(New EventArgs)
                End If

            Else

                Debug.WriteLine("Missing points=" & buffer.ValidSamples)

            End If

        Catch ex As Global.OpenLayers.Base.OlException

            Me.OnDisplayDriverError(TraceEventType.Error, My.MyLibrary.TraceEventId, "Failed processing buffer",
                                    "Failure occurred on sample {0} loop pointer {1};. Details: {2}",
                                    Me.SampleCounter, Me._loopPointer, ex)

        Catch ex As Exception

            Me.OnDisplayDriverError(TraceEventType.Error, My.MyLibrary.TraceEventId, "Failed processing buffer",
                                    "Failure occurred on sample {0} loop pointer {1};. Details: {2}",
                                    Me.SampleCounter, Me._loopPointer, ex)

        Finally

            ' release the buffer to the queue
            If Not Me.FiniteBuffering Then
                ' if infinite buffering, re-queue the buffer so that it can be filled again.
                Me.AnalogInput.BufferQueue.QueueBuffer(buffer)
            End If

        End Try

        If Me._bufferQueue.Count <= 0 Then

            ' if done processing, check if time to update the display
            If DateTime.Now().Subtract(nextRefreshTime) > TimeSpan.Zero Then

                SyncLock Me.chartVoltsLocker

                    ' display 
                    Me.displayVoltages()

                    ' chart if required.
                    Me.chartVoltageTimeSeries()

                End SyncLock

            End If
        Else
            ' repeat until the queue is processed.
            Me.processBufferQueue()
        End If

    End Sub

    ''' <summary> Copy buffer data to the voltages array. </summary>
    ''' <param name="buffer"> . </param>
    Private Sub copyBuffer(ByVal buffer As Global.OpenLayers.Base.OlBuffer)

        Dim channelIndex As Integer
        Dim loopPointer As Integer

        If Me.ChartMode = ChartModeId.Scope AndAlso Me.DecimationRatio = 1 Then

            ' if we are saving the entire buffer size, just go ahead.

            ' set the loop pointer to the last data point in the channel buffer.
            loopPointer = Me.ChannelBufferSize - 1

            ' if saving sub buffers, decimate starting with the last loop pointer or zero for scope.
            For channelIndex = 0 To Me.AnalogInput.ChannelsCount - 1

                ' update the voltage buffer.
                Me._voltages(channelIndex) = buffer.GetDataAsVolts(Me.AnalogInput.ChannelList(channelIndex))

                ' save the last voltage
                Me._lastVoltages(channelIndex) = Me._voltages(channelIndex)(loopPointer)

            Next

        Else

            Dim initialLoopPoint As Integer = -1
            If Me.ChartMode = ChartModeId.StripChart Then
                initialLoopPoint = Me._loopPointer
            End If

            ' if saving sub buffers, decimate starting with the last loop pointer or zero for scope.
            For channelIndex = 0 To Me.AnalogInput.ChannelsCount - 1

                ' get the voltages for this channel
                Dim channelVolts() As Double = buffer.GetDataAsVolts(Me.AnalogInput.ChannelList(channelIndex))

                loopPointer = initialLoopPoint
                Dim voltage As Double
                For i As Integer = 0 To channelVolts.Length - 1 Step Me._decimationRatio
                    voltage = channelVolts(i)
                    loopPointer += 1
                    If loopPointer >= Me.SignalMemoryLength Then
                        loopPointer = 0
                    End If
                    Me._voltages(channelIndex)(loopPointer) = voltage
                Next

                ' save the last voltage
                Me._lastVoltages(channelIndex) = voltage

            Next

        End If

        ' update loop pointer
        ' Me._loopPointer += Me._channelBufferSize
        Me._loopPointer = loopPointer

        ' update the sample counters
        Me._sampleCounter += Me.SampleSize

        If Me.CalculateEnabled Then
            Me.calculatePeriodDownCounts -= Me.SampleSize
        End If
        If Me._UpdateEventEnabled Then
            Me._updatePeriodDownCounter -= Me.SampleSize
        End If

    End Sub

#End Region

#Region " PRIVATE GUI METHODS AND PROPERTIES "

    ''' <summary> Updates the enabled status of the add signal control. </summary>
    Private Sub enableAddSignalControl()
        Me._AddSignalButton.Enabled = Me.IsOpen AndAlso Me._SignalChannelComboBox.SelectedIndex >= 0 AndAlso Not Me.ContinuousOperationActive
    End Sub

    ''' <summary> Updates the enabled status of the channels controls. </summary>
    Private Sub enableChannelsControls()

        Me._AddChannelButton.Enabled = Me.IsOpen
        Me._ChannelsGroupBox.Enabled = Me.IsOpen
        Me._SamplingGroupBox.Enabled = Me.IsOpen
        Me._RemoveChannelButton.Enabled = Me.selectedChannel IsNot Nothing AndAlso Not Me.ContinuousOperationActive
        Me._BoardGroupBox.Enabled = Me.IsOpen
        Me._ProcessingGroupBox.Enabled = True
        Me._SmartBufferingCheckBox.Enabled = ChartMode = ChartModeId.StripChart

    End Sub

    ''' <summary> Updates the enabled status of the chart controls. </summary>
    Private Sub enableChartControls()

        Me._ChartEnableGroupBox.SafeEnabledSetter(True)
        Me._ChartTypeGroupBox.SafeEnabledSetter(Not Me.ContinuousOperationActive)
        Me._BandModeGroupBox.SafeEnabledSetter(True)
        Me._ChartColorsGroupBox.SafeEnabledSetter(True)
        Me.StartStopEnabled = Me.IsOpen AndAlso Me.AnalogInput.HasChannels
        Me._ChartTitlesGroupBox.SafeEnabledSetter(True)

    End Sub

    ''' <summary> Updates the enabled status of the signals controls. </summary>
    Private Sub enableSignalsControls()
        Me.enableAddSignalControl()
        Me._SignalChannelGroupBox.Enabled = Me.IsOpen AndAlso Me.AnalogInput.HasChannels AndAlso Not Me.ContinuousOperationActive
        Me._SignalSamplingGroupBox.Enabled = Not Me.ContinuousOperationActive
        Me._UpdateSignalButton.Enabled = Me.selectedSignal IsNot Nothing
        Me._ColorSignalButton.Enabled = Me.selectedSignal IsNot Nothing
        Me._SignalsListView.Enabled = True
        Me._RemoveSignalButton.Enabled = Me.selectedSignal IsNot Nothing AndAlso Not Me.ContinuousOperationActive
        Me._SignalBufferSizeNumeric.Enabled = Me.ChartMode = ChartModeId.StripChart
        Me._SignalMemoryLengthNumeric.Enabled = Me.ChartMode = ChartModeId.StripChart

    End Sub

#End Region

#Region " EVENTS AND ON EVENTS "

#Region " ACQUISITION STATED "

    ''' <summary>Defines the event for announcing that acquisition started.</summary>
    Public Event AcquisitionStarted As EventHandler(Of isr.Core.Diagnosis.TraceMessageEventArgs)

    ''' <summary> Raises the Acquisition Started event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnAcquisitionStarted(ByVal e As isr.Core.Diagnosis.TraceMessageEventArgs)
        Dim eventHandler As EventHandler(Of isr.Core.Diagnosis.TraceMessageEventArgs) = Me.AcquisitionStartedEvent
        If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, e)
    End Sub

    ''' <summary> Displays and raises the acquisition started message. </summary>
    ''' <param name="severity">      The severity. </param>
    ''' <param name="id">            The identity. </param>
    ''' <param name="detailsFormat"> The details format. </param>
    ''' <param name="detailsArgs">   The details args. </param>
    Private Sub OnDisplayAcquisitionStarted(ByVal severity As TraceEventType, ByVal id As Integer,
                                            ByVal detailsFormat As String, ByVal ParamArray detailsArgs() As Object)
        Dim traceMessage As isr.Core.Diagnosis.TraceMessage = New isr.Core.Diagnosis.TraceMessage(severity, id, detailsFormat, detailsArgs)
        Me.DisplayTraceMessage(traceMessage)
        Me.OnAcquisitionStarted(New isr.Core.Diagnosis.TraceMessageEventArgs(traceMessage))
    End Sub

    ''' <summary> Starts data acquisition and display. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <exception cref="OperationFailedException">  Failed to start. </exception>
    Private Sub startContinuousInput()

        If Not Global.OpenLayers.Base.DeviceMgr.Get.HardwareAvailable Then
            ' Me.onHardwareDisconnected(New Global.OpenLayers.Base.GeneralEventArgs)
            Dim eventHandler As EventHandler(Of EventArgs) = Me.DisconnectedEvent
            If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, New EventArgs)
            Throw New InvalidOperationException("Hardware not available.")
        End If

        If Me.ChartMode = ChartModeId.Scope Then
        ElseIf Me.ChartMode = ChartModeId.StripChart Then
            ' Set time of start
            scrollTimeFrame(0)
        Else
            Throw New InvalidOperationException("Strip chart mode not set.")
        End If

        ' instantiate the buffer queue
        Me._bufferQueue = New System.Collections.Generic.Queue(Of Global.OpenLayers.Base.OlBuffer)

        ' clear the voltage storage
        Me.AllocateVoltages(Me.AnalogInput.ChannelList.Count, Me.SignalMemoryLength)

        ' initialize sample count
        Me._sampleCounter = 0

        ' initialize the update period counter.
        Me._updatePeriodDownCounter = Me.UpdatePeriodCount

        ' initialize the calculate period counter.
        Me.calculatePeriodDownCounts = Me.CalculatePeriodCount

        If Me.ChartMode = ChartModeId.Scope Then
            Me._loopPointer = 0
        ElseIf Me.ChartMode = ChartModeId.StripChart Then
            Me._loopPointer = -1
        End If

        ' Disable user interface until the chart is stopped.
        Me.enableChartControls()
        Me.enableSignalsControls()
        Me.enableChannelsControls()
        Me._DeviceNameChooser.Enabled = Not Me.IsOpen

        ' enable selection of signals.
        Me._SignalsListView.Enabled = True

        ' select the channel
        If Me.selectedSignal Is Nothing Then
            Me.selectSignal(0)
        End If
        Me.AnalogInput.SelectNamedChannel(Me.selectedSignal.Name)
        Me.selectChannel(Me.AnalogInput.SelectedChannelLogicalNumber)

        ' Set the status of the start/stop control making sure we can stop
        Me.Started = True

        ' clear the displays.
        Me._CurrentVoltageToolStripLabel.Text = ""
        Me._AverageVoltageToolStripLabel.Text = ""

        ' start data acquisition
        Me.AnalogInput.Start()

        If Me.ChartingEnabled Then
            Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Charting...;. ")
        Else
            Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Running...;. ")
        End If

        ' notify display that new data is available
        Me.Chart.SignalUpdate()

        ' set next display update time
        Me.nextRefreshTime = DateTime.Now().Add(refreshTimespan)

        ' raise the acquisition started event
        Me.OnDisplayAcquisitionStarted(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Acquisition started")

        If Not Me.Started Then
            Throw New OperationFailedException("Failed to start")
        End If

    End Sub

    ''' <summary> Tries to start continuous input. </summary>
    Private Sub tryStartContinuousInput()

        Try

            Me.startContinuousInput()

        Catch ex As Global.OpenLayers.Base.OlException

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Open Layers Driver exception occurred starting;. Details: {0}.", ex)

        Catch ex As System.InvalidOperationException

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Invalid operation occurred starting;. Details: {0}", ex)

        Catch ex As isr.IO.OL.OperationFailedException

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Operation failed exception occurred starting;. Details: {0}", ex)
        Catch ex As System.TimeoutException

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Timeout exception occurred starting;. Details: {0}", ex)

        Catch

            Throw

        End Try

    End Sub

#End Region

#Region " ACQUISITION STOPPED "

    ''' <summary>Defines the event for announcing that acquisition stopped.</summary>
    Public Event AcquisitionStopped As EventHandler(Of isr.Core.Diagnosis.TraceMessageEventArgs)

    ''' <summary> Raises the Acquisition Stopped event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overridable Sub OnAcquisitionStopped(ByVal e As isr.Core.Diagnosis.TraceMessageEventArgs)

        Dim eventHandler As EventHandler(Of isr.Core.Diagnosis.TraceMessageEventArgs) = Me.AcquisitionStoppedEvent
        If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, e)

        ' force abort
        Me.tryStopContinuousInput(Me.StopTimeout, True)

    End Sub

    ''' <summary> Displays and raises the acquisition Stopped message. </summary>
    ''' <param name="severity">      The severity. </param>
    ''' <param name="id">            The identity. </param>
    ''' <param name="detailsFormat"> The details format. </param>
    ''' <param name="detailsArgs">   The details args. </param>
    Private Sub OnDisplayAcquisitionStopped(ByVal severity As TraceEventType, ByVal id As Integer,
                                            ByVal detailsFormat As String, ByVal ParamArray detailsArgs() As Object)
        Dim traceMessage As isr.Core.Diagnosis.TraceMessage = New isr.Core.Diagnosis.TraceMessage(severity, id, detailsFormat, detailsArgs)
        Me.DisplayTraceMessage(traceMessage)
        Me.OnAcquisitionStopped(New isr.Core.Diagnosis.TraceMessageEventArgs(traceMessage))
    End Sub

    ''' <summary> Stops or aborts continuous input. This also stops charting. </summary>
    ''' <param name="timeout"> The timeout. </param>
    ''' <param name="abort">   if set to <c>True</c> [abort]. </param>
    ''' <exception cref="TimeoutException"> Abort timeout--system still running after abort. </exception>
    Private Sub stopContinuousInput(ByVal timeout As TimeSpan, ByVal abort As Boolean)

        Try

            If Me.ContinuousOperationActive Then

                ' stop data acquisition if running.
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Aborting data acquisition;. ")
                If abort Then
                    Me.AnalogInput.Abort(timeout)
                Else
                    Me.AnalogInput.Stop(timeout)
                End If

            End If

        Catch

            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed aborting data acquisition;. ")
            Throw

        Finally

            ' make sure we can start
            ' Set the status of the start/stop control making sure we can start
            Me.Started = Me.ContinuousOperationActive

            ' enable GUI
            Me.enableChartControls()
            Me.enableSignalsControls()
            Me.enableChannelsControls()

            ' update the status of the open check box.
            Me._OpenCloseCheckBox.SilentCheckedSetter(Me.IsOpen)
            Me._DeviceNameChooser.Enabled = Not Me.IsOpen

        End Try

    End Sub

    ''' <summary> Tries the stop continuous input. </summary>
    ''' <param name="timeout"> The timeout. </param>
    ''' <param name="abort">   if set to <c>True</c> [abort]. </param>
    Private Sub tryStopContinuousInput(ByVal timeout As TimeSpan, ByVal abort As Boolean)

        Try

            Me.stopContinuousInput(timeout, abort)

        Catch ex As Global.OpenLayers.Base.OlException

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Open Layers Driver exception occurred stopping;. Details: {0}.", ex)

        Catch ex As System.TimeoutException

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Timeout exception occurred stopping;. Details: {0}", ex)

        Catch

            Throw

        End Try

    End Sub

#End Region

#Region " BOARDS LOCATED "

    ''' <summary>Defines the event handler for locating the boards.</summary>
    Public Event BoardsLocated As EventHandler(Of EventArgs)

    ''' <summary> Raises the Boards Located event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnBoardsLocated(ByVal e As EventArgs)
        Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Data Acquisition board(s) located;. ")
        Dim eventHandler As EventHandler(Of EventArgs) = Me.BoardsLocatedEvent
        If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, e)
    End Sub

#End Region

#Region " BUFFER DONE "

    ''' <summary>Defines the buffer done event.</summary>
    Public Event BufferDone As EventHandler(Of EventArgs)

#End Region

#Region " DISCONNECTED "

    ''' <summary>Occurs when the subsystem detects that the hardware is no longer
    '''   connected.
    ''' </summary>
    Public Event Disconnected As EventHandler(Of EventArgs)

#End Region

#Region " UPDATE PERIOD DONE "

    ''' <summary>Notifies of new data.</summary>
    Public Event UpdatePeriodDone As EventHandler(Of EventArgs)

    ''' <summary> Raises the update period event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnUpdatePeriodDone(ByVal e As EventArgs)
        Dim eventHandler As EventHandler(Of EventArgs) = Me.UpdatePeriodDoneEvent
        If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, e)
    End Sub

#End Region

#Region " DRIVER ERROR "

    ''' <summary>Occurs when the driver had a general error.
    ''' </summary>
    Public Event DriverError As EventHandler(Of isr.Core.Diagnosis.TraceMessageEventArgs)

    ''' <summary> Raises the driver general error event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overridable Sub OnDriverError(ByVal e As isr.Core.Diagnosis.TraceMessageEventArgs)

        ' force stop without raising exceptions.
        Me.tryStopContinuousInput(Me.StopTimeout, True)

        ' invoke the device error event
        Dim eventHandler As EventHandler(Of isr.Core.Diagnosis.TraceMessageEventArgs) = Me.DriverErrorEvent
        If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, e)

    End Sub

    ''' <summary> Displays and raises the driver error message. </summary>
    ''' <param name="severity">      The severity. </param>
    ''' <param name="id">            The identity. </param>
    ''' <param name="detailsFormat"> The details format. </param>
    ''' <param name="detailsArgs">   The details args. </param>
    Private Sub OnDisplayDriverError(ByVal severity As TraceEventType, ByVal id As Integer,
                                            ByVal detailsFormat As String, ByVal ParamArray detailsArgs() As Object)
        Dim traceMessage As isr.Core.Diagnosis.TraceMessage = New isr.Core.Diagnosis.TraceMessage(severity, id, detailsFormat, detailsArgs)
        Me.DisplayTraceMessage(traceMessage)
        Me.OnDriverError(New isr.Core.Diagnosis.TraceMessageEventArgs(traceMessage))
    End Sub

#End Region

#End Region

#Region " OPEN LAYERS HANDLERS "

    ''' <summary> Updates the acquired data and displays signals. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Buffer done event information. </param>
    Private Sub handleBufferDone(ByVal sender As Object, ByVal e As Global.OpenLayers.Base.BufferDoneEventArgs)

        If Me.InvokeRequired Then
            Me.Invoke(New Global.OpenLayers.Base.BufferDoneHandler(AddressOf handleBufferDone), New Object() {sender, e})
        Else
            Me.OnBufferDone(e)
        End If

    End Sub

    ''' <summary> The process buffer queue locker. </summary>
    Private processBufferQueueLocker As New Object

    ''' <summary> Handles the buffer done event from the board driver. </summary>
    ''' <remarks> Test results for scope display: 5 KHz, 1000 Samples, 5 buffers, 10 points average.
    ''' Storage   CALC           Display        Chart         Total 0.1388ms  off: 0.0156ms  off:
    ''' 0.0134ms  on:  0.045ms  0.2128ms 0.1449ms  off: 0.0162ms  v:   0.8381ms; on:  0.0411ms
    ''' 1.0403ms 0.1435ms  on:  0.0185ms  v:   0.0316ms; on:  0.0463ms 0.2399ms 0.1519ms  on:
    ''' 0.0185ms  v,a: 1.5622ms; on:  0.0441ms 1.7767ms 0.1385ms  on:  0.0187ms  v,a: 1.4083ms; off:
    ''' 0.0159ms 1.5814ms. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overridable Sub OnBufferDone(ByVal e As Global.OpenLayers.Base.BufferDoneEventArgs)

        If e IsNot Nothing AndAlso Me._bufferQueue IsNot Nothing Then
            Try
                If e.OlBuffer Is Nothing Then
                    Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Unexpected null reference to Open Layer buffer occurred @{0};. ",
                                               Reflection.MethodInfo.GetCurrentMethod.Name)
                Else
                    ' Add the buffer to the queue separating the buffer that is queued from the
                    ' one that is processed.
                    Me._bufferQueue.Enqueue(e.OlBuffer)

                    SyncLock Me.processBufferQueueLocker
                        ' process the buffer queue
                        Me.processBufferQueue()
                    End SyncLock
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred handling buffer done;. Details: {0}", ex)
            End Try
        End If

    End Sub

    ''' <summary> Handles the hardware disconnected event. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      General event information. </param>
    Private Sub handleDeviceRemoved(ByVal sender As Object, ByVal e As Global.OpenLayers.Base.GeneralEventArgs)

        If Me.InvokeRequired Then
            Me.Invoke(New Global.OpenLayers.Base.DeviceRemovedHandler(AddressOf handleDeviceRemoved),
                      New Object() {sender, e})
        Else
            Me.OnHardwareDisconnected(e)
        End If

    End Sub

    ''' <summary> Close and raise the hardware disconnected event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overridable Sub OnHardwareDisconnected(ByVal e As Global.OpenLayers.Base.GeneralEventArgs)

        Try
            Me._BoardDisconnected = True
            Me.disconnectAnalogInput()
            If e IsNot Nothing Then
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Driver disconnected on subsystem {0} at {1:T};. ", e.Subsystem, e.DateTime)
            End If
        Catch ex As OperationFailedException
            ' ignore operation failure as this is expected at this point.
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred handling hardware disconnected;. Details: {0}", ex)
        Finally
            Dim eventHandler As EventHandler(Of EventArgs) = Me.DisconnectedEvent
            If eventHandler IsNot Nothing Then eventHandler.Invoke(Me, e)
        End Try

    End Sub

    ''' <summary> Reports driver runtime errors. </summary>
    ''' <remarks> The Driver runtime error occurs when the device driver detects one of the following
    ''' error conditions during runtime: FifoOverflow, FifoUnderflow, DeviceOverClocked, TriggerError,
    ''' or DeviceError. See OpenLayerError for more information. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Driver run time error event information. </param>
    Private Sub handleDriverRuntimeError(ByVal sender As Object, ByVal e As Global.OpenLayers.Base.DriverRunTimeErrorEventArgs)

        If Me.InvokeRequired Then
            Me.Invoke(New Global.OpenLayers.Base.DriverRunTimeErrorEventHandler(AddressOf handleDriverRuntimeError),
                      New Object() {sender, e})
        Else
            Me.OnDriverRuntimeError(e)
        End If

    End Sub

    ''' <summary> Displays a message and raises the driver error event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnDriverRuntimeError(ByVal e As Global.OpenLayers.Base.DriverRunTimeErrorEventArgs)

        If e IsNot Nothing Then
            Me.OnDisplayDriverError(TraceEventType.Error, My.MyLibrary.TraceEventId, "Driver Runtime Error occurred",
                                    "Driver Runtime Failure #{0}: {1}. occurred on subsystem {2} at {3:T}",
                                    e.ErrorCode, e.Message, e.Subsystem, e.DateTime)
            Select Case e.ErrorCode
                Case Global.OpenLayers.Base.ErrorCode.AccessDenied, Global.OpenLayers.Base.ErrorCode.CannotOpenDriver,
                    Global.OpenLayers.Base.ErrorCode.DeviceError, Global.OpenLayers.Base.ErrorCode.GeneralFailure
                    Me.tryStopContinuousInput(Me.StopTimeout, True)
                Case Global.OpenLayers.Base.ErrorCode.FifoOverflow, Global.OpenLayers.Base.ErrorCode.FifoUnderflow
                    ' do nothing on bad data?!
                Case Else
                    ' do nothing
            End Select
        End If

    End Sub

    ''' <summary> Handles General Failure events.  Displays a message. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      General event information. </param>
    Private Sub handleGeneralFailure(ByVal sender As Object, ByVal e As Global.OpenLayers.Base.GeneralEventArgs)
        If Me.InvokeRequired Then
            Me.Invoke(New Global.OpenLayers.Base.GeneralFailureHandler(AddressOf handleGeneralFailure),
                      New Object() {sender, e})
        Else
            Me.OnGeneralFailure(e)
        End If
    End Sub 'HandleOverrunError

    ''' <summary> Displays a message and raises the driver error event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnGeneralFailure(ByVal e As Global.OpenLayers.Base.GeneralEventArgs)
        If e IsNot Nothing Then
            Me.OnDisplayDriverError(TraceEventType.Error, My.MyLibrary.TraceEventId, "General Failure Error occurred",
                                    "General Failure occurred on subsystem {0} at {1:T}.",
                                    e.Subsystem, e.DateTime)
            Me.tryStopContinuousInput(Me.StopTimeout, True)
        End If

    End Sub

    ''' <summary> Handles the Queue Done events.  Displays a message. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      General event information. </param>
    Private Sub handleQueueDone(ByVal sender As Object, ByVal e As Global.OpenLayers.Base.GeneralEventArgs)
        If Me.InvokeRequired Then
            Me.Invoke(New Global.OpenLayers.Base.QueueDoneHandler(AddressOf handleQueueDone), New Object() {sender, e})
        Else
            Me.OnQueueDone(e)
        End If
    End Sub 'HandleQueueDone

    ''' <summary> Handles the normal end of the queue. </summary>
    ''' <remarks> Rev. 2.0.2773. Finite buffering Queue tested successfully with suspend and recover. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnQueueDone(ByVal e As Global.OpenLayers.Base.GeneralEventArgs)

        ' check if we have a finite or infinite buffering mode
        If Me.FiniteBuffering Then
            If e IsNot Nothing Then
                Me.OnDisplayAcquisitionStopped(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                                              "Finished",
                                                              "Queue done on subsystem {0} at {1:T}",
                                                              e.Subsystem, e.DateTime)
            End If

        Else
            If e IsNot Nothing Then
                ' if this occurred on infinite buffering this represents a buffer overrun.
                Me.OnDisplayDriverError(TraceEventType.Error, My.MyLibrary.TraceEventId, "Buffer overrun",
                                        "Buffer overrun Failure occurred on subsystem {0} at {1:T}. Queue count is {2}.",
                                        Me.AnalogInput, DateTime.Now(), Me.AnalogInput.BufferQueue.QueuedCount)
            End If

        End If

    End Sub

    ''' <summary> Handles the Queue Stopped events.  Displays a message. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      General event information. </param>
    Private Sub handleQueueStopped(ByVal sender As Object, ByVal e As Global.OpenLayers.Base.GeneralEventArgs)
        If Me.InvokeRequired Then
            Me.Invoke(New Global.OpenLayers.Base.QueueStoppedHandler(AddressOf handleQueueStopped), New Object() {sender, e})
        Else
            Me.OnQueueStopped(e)
        End If
    End Sub

    ''' <summary> Handles a stopped queue. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnQueueStopped(ByVal e As Global.OpenLayers.Base.GeneralEventArgs)
        If e IsNot Nothing Then
            Me.OnDisplayAcquisitionStopped(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Queue stopped on subsystem {0} at {1:T};. ",
                                           e.Subsystem, e.DateTime)
        End If
    End Sub

#End Region

#Region " BOARD CONTROLS HANDLERS "

    ''' <summary> Enables the initialization button. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _deviceNameChooser_DeviceSelected(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _DeviceNameChooser.DeviceSelected
        Me._OpenCloseCheckBox.Enabled = Me._DeviceNameChooser.DeviceName.Length > 0
    End Sub

    ''' <summary> Handles the CheckedChanged event of the _openCloseCheckBox control. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub _openCloseCheckBox_CheckedChanged(sender As Object, e As System.EventArgs) Handles _OpenCloseCheckBox.CheckedChanged
        If Me._OpenCloseCheckBox.Checked Then
            Me._OpenCloseCheckBox.Text = "Cl&ose"
        Else
            Me._OpenCloseCheckBox.Text = "&Open"
        End If
    End Sub

    ''' <summary> Opens of closes the strip chart. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub openCloseCheckBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _OpenCloseCheckBox.Click

        If Me._OpenCloseCheckBox.Enabled Then
            If Me._OpenCloseCheckBox.Checked Then
                Try
                    Me.Open(Me._DeviceNameChooser.DeviceName)
                Catch ex As Global.OpenLayers.Base.OlException
                    Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred opening;. Details: {0}.", ex)
                End Try
            Else
                Try
                    Me.Close()
                Catch ex As Global.OpenLayers.Base.OlException
                    Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred closing;. Details: {0}.", ex)
                End Try
            End If
        End If

    End Sub

#End Region

#Region " CHART CONTROLS HANDLERS "

    ''' <summary> Event handler. Called by _AbortToolStripMenuItem for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _AbortToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _AbortToolStripMenuItem.Click
        If Me.IsOpen Then
            Try
                Me.Abort()
            Catch ex As Global.OpenLayers.Base.OlException
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred aborting;. Details: {0}.", ex)
            End Try
        End If
    End Sub

    ''' <summary> Handles the Validating event of the ChartTitleTextBox control. Updates the chart
    ''' title. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.ComponentModel.CancelEventArgs" /> instance
    ''' containing the event data. </param>
    Private Sub _ChartTitleTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _ChartTitleTextBox.Validating
        Try
            Me.ChartTitle = Me.ChartTitle
        Catch ex As Global.OpenLayers.Base.OlException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting chart;. Details: {0}.", ex)
        End Try
    End Sub

    ''' <summary> Handles the Validating event of the ChartFooterTextBox control. Updates the chart
    ''' footer. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.ComponentModel.CancelEventArgs" /> instance
    ''' containing the event data. </param>
    Private Sub _ChartFooterTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _ChartFooterTextBox.Validating
        Try
            Me.ChartFooter = Me.ChartFooter
        Catch ex As Global.OpenLayers.Base.OlException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting chart footer;. Details: {0}.", ex)
        End Try
    End Sub

    ''' <summary> Handles the Click event of the colorAxesButton control. Selects the color. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="EventArgs" /> instance containing the event data. </param>
    Private Sub _colorAxesButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ColorAxesButton.Click
        Try
            Using dialog As New Windows.Forms.ColorDialog
                dialog.Color = Me.AxesColor
                If dialog.ShowDialog() = DialogResult.OK Then
                    Me.AxesColor = dialog.Color
                End If
            End Using
        Catch ex As Global.OpenLayers.Base.OlException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting axis color;. Details: {0}.", ex)
        End Try
    End Sub

    ''' <summary> Event handler. Called by _colorGridsButton for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _colorGridsButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ColorGridsButton.Click
        Try
            Using dialog As New Windows.Forms.ColorDialog
                dialog.Color = Me.GridColor
                If dialog.ShowDialog() = DialogResult.OK Then
                    Me.GridColor = dialog.Color
                End If
            End Using
        Catch ex As Global.OpenLayers.Base.OlException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting grid color;. Details: {0}.", ex)
        End Try
    End Sub

    ''' <summary> Event handler. Called by _DisplayVoltageCheckBox for checked changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _DisplayVoltageCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _DisplayVoltageCheckBox.CheckedChanged
        If Me._DisplayVoltageCheckBox.Enabled Then
            Me.DisplayVoltageEnabled = Me._DisplayVoltageCheckBox.Checked
        End If
    End Sub

    ''' <summary> Event handler. Called by _DisplayAverageCheckBox for checked changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _DisplayAverageCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _DisplayAverageCheckBox.CheckedChanged
        If Me._DisplayAverageCheckBox.Enabled Then
            Me.DisplayAverageEnabled = Me._DisplayAverageCheckBox.Checked
        End If
    End Sub

    ''' <summary> Event handler. Called by _EnableChartingCheckBox for checked changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _EnableChartingCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _EnableChartingCheckBox.CheckedChanged
        If Me._EnableChartingCheckBox.Enabled Then
            Me.ChartingEnabled = Me._EnableChartingCheckBox.Checked
        End If
    End Sub

    ''' <summary> Event handler. Called by _RefreshRateNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _RefreshRateNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _RefreshRateNumeric.ValueChanged
        ' request configuration of chart.
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Selects the chart mode as scope or strip chart. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ScopeRadioButton_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ScopeRadioButton.CheckedChanged
        If Me._ChartTypeGroupBox.Enabled AndAlso Me._ScopeRadioButton.Enabled Then
            Try
                If Me._ScopeRadioButton.Checked Then
                    Me.ChartMode = ChartModeId.Scope
                Else
                    Me.ChartMode = ChartModeId.StripChart
                End If
                ' request configuration of analog input and chart.
                Me.AnalogInputConfigurationRequired = True
                Me.ChartConfigurationRequired = True
            Catch ex As Global.OpenLayers.Base.OlException
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting chart mode;. Details: {0}.", ex)
            End Try
        End If
    End Sub

    ''' <summary> Change BandMode. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _singleRadioButton_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles _SingleRadioButton.CheckedChanged
        If Me._SingleRadioButton.Enabled Then
            Try
                Me.Chart.DisableRendering()
                If Me._SingleRadioButton.Checked Then
                    Me.ChartBandMode = OpenLayers.Controls.BandMode.SingleBand
                Else
                    Me.ChartBandMode = OpenLayers.Controls.BandMode.MultiBand
                End If
                Me.Chart.EnableRendering()
                Me.Chart.SignalUpdate()
            Catch ex As Global.OpenLayers.Base.OlException
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting chart band mode;. Details: {0}.", ex)
            End Try
        End If
    End Sub

    ''' <summary> Handles the CheckedChanged event of the _StartStopToolStripMenuItem control. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Private Sub _StartStopToolStripMenuItem_CheckedChanged(sender As Object, e As System.EventArgs) Handles _StartStopToolStripMenuItem.CheckedChanged
        If _StartStopToolStripMenuItem.Checked Then
            Me._StartStopToolStripMenuItem.SafeTextSetter("&STOP")
            Me._StartStopToolStripMenuItem.SafeToolTipTextSetter("Stops data collection and chart (if enabled)")
        Else
            Me._StartStopToolStripMenuItem.SafeTextSetter("&START")
            Me._StartStopToolStripMenuItem.SafeToolTipTextSetter("Starts data collection and chart (if enabled)")
        End If
        Me._StartStopToolStripMenuItem.Invalidate()
    End Sub 'singleRadioButton_CheckedChanged

    ''' <summary> Starts or stops Running. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _StartStopToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _StartStopToolStripMenuItem.Click
        If Me._StartStopToolStripMenuItem.Enabled Then
            If Me.Started Then
                Try
                    Me.Stop()
                Catch ex As Global.OpenLayers.Base.OlException
                    Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred stopping;. Details: {0}.", ex)
                End Try
            Else
                Try
                    Me.Start()
                Catch ex As InvalidOperationException
                    Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Invalid Operation Exception occurred starting;. Details: {0}.", ex)
                Catch ex As Global.OpenLayers.Base.OlException
                    Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Driver Exception occurred starting;. Details: {0}.", ex)
                End Try
            End If
        End If
    End Sub

#End Region

#Region " GLOBAL CONTROLS HANDLERS "

    ''' <summary> Event handler. Called by _ConfigureButton for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ConfigureButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ConfigureButton.Click
        If Me._ConfigureButton.Enabled Then
            Try
                Dim details As String = ""
                Me.StartStopEnabled = Me._TryConfigure(details)
            Catch ex As Global.OpenLayers.Base.OlException
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred configuring analog input or chart;. Details: {0}", ex)
            End Try
        End If
    End Sub

    ''' <summary> Show or hide the configuration menu. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ConfigureToolStripMenuItem1_Checkchanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ConfigureToolStripMenuItem.CheckedChanged
        If Me._ConfigureToolStripMenuItem.Enabled Then
            If (Me.ConfigurationPanelVisible = Me._ConfigureToolStripMenuItem.Checked) Then
                Me._ConfigureToolStripMenuItem.Checked = Not Me._ConfigureToolStripMenuItem.Checked
            End If
        End If
        Me.ConfigurationPanelVisible = Me._ConfigureToolStripMenuItem.Checked
    End Sub

    ''' <summary> Event handler. Called by _ResetButton for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ResetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ResetButton.Click
        If Me._ResetButton.Enabled Then
            Try
                Me.Reset()
            Catch ex As Global.OpenLayers.Base.OlException
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred resetting to opened state values;. Details: {0}.", ex)
            End Try
        End If
    End Sub

    ''' <summary> Restores default values. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _RestoreDefaultsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _RestoreDefaultsButton.Click
        Me.RestoreDefaults()
    End Sub

#End Region

#Region " CHANNELS CONTROLS HANDLERS "

    ''' <summary> Adds or updates a channel. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _AddChannelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AddChannelButton.Click
        If Me._AddChannelButton.Enabled AndAlso Me._ChannelsGroupBox.Enabled AndAlso Me._ChannelRangesComboBox.SelectedIndex >= 0 Then
            Dim physicalChannelNumber As Integer
            If Me.IsOpen AndAlso Integer.TryParse(Me._ChannelComboBox.Text, physicalChannelNumber) Then
                Try
                    Me.AddChannel(physicalChannelNumber, Me._ChannelRangesComboBox.SelectedIndex, Me._ChannelNameTextBox.Text)
                Catch ex As Global.OpenLayers.Base.OlException
                    Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                               "Exception occurred adding channel {0};. Details: {1}.", physicalChannelNumber, ex)
                Finally
                    If Me.AnalogInput.HasChannels Then
                        If Me.AnalogInput.PhysicalChannelExists(physicalChannelNumber) Then
                            Me.UpdateChannel(physicalChannelNumber, Me._ChannelRangesComboBox.SelectedIndex, Me._ChannelNameTextBox.Text)
                        Else
                            Me.AddChannel(physicalChannelNumber, Me._ChannelRangesComboBox.SelectedIndex, Me._ChannelNameTextBox.Text)
                        End If
                    Else
                        Me.AddChannel(physicalChannelNumber, Me._ChannelRangesComboBox.SelectedIndex, Me._ChannelNameTextBox.Text)
                    End If
                End Try
            End If
        End If
    End Sub

    ''' <summary> Selects the board input range. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _BoardInputRangesComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _BoardInputRangesComboBox.SelectedIndexChanged
        If Me.IsOpen AndAlso Me._BoardInputRangesComboBox.SelectedIndex >= 0 Then
            Try
                Me.AnalogInput.VoltageRange = Me.AnalogInput.SupportedVoltageRanges(Me._BoardInputRangesComboBox.SelectedIndex)
            Catch ex As Global.OpenLayers.Base.OlException
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting voltage range;. Details: {0}", ex)
            End Try

            Dim max As Decimal
            Dim min As Decimal
            max = Math.Min(Me._SignalMinNumericUpDown.Maximum, CDec(Me.AnalogInput.VoltageRange.High))
            min = Math.Max(Me._SignalMinNumericUpDown.Minimum, CDec(Me.AnalogInput.VoltageRange.Low))
            Me._SignalMinNumericUpDown.Value = Math.Max(Math.Min(Me._SignalMinNumericUpDown.Value, max), min)
            Me._SignalMinNumericUpDown.Maximum = CDec(Me.AnalogInput.VoltageRange.High)
            Me._SignalMinNumericUpDown.Minimum = CDec(Me.AnalogInput.VoltageRange.Low)

            max = Math.Min(Me._SignalMaxNumericUpDown.Maximum, CDec(Me.AnalogInput.VoltageRange.High))
            min = Math.Max(Me._SignalMaxNumericUpDown.Minimum, CDec(Me.AnalogInput.VoltageRange.Low))
            Me._SignalMaxNumericUpDown.Value = Math.Max(Math.Min(Me._SignalMaxNumericUpDown.Value, max), min)
            Me._SignalMaxNumericUpDown.Maximum = CDec(Me.AnalogInput.VoltageRange.High)
            Me._SignalMaxNumericUpDown.Minimum = CDec(Me.AnalogInput.VoltageRange.Low)

            ' request configuration of analog input and chart.
            Me.AnalogInputConfigurationRequired = True
            Me.ChartConfigurationRequired = True
        End If
    End Sub

    ''' <summary> Event handler. Called by _BuffersCountNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _BuffersCountNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _BuffersCountNumeric.ValueChanged
        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Event handler. Called by _CalculateAverageCheckBox for checked changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _CalculateAverageCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CalculateAverageCheckBox.CheckedChanged
        If Me._CalculateAverageCheckBox.Enabled Then
            ' request configuration of analog input and chart.
            Me.AnalogInputConfigurationRequired = True
            Me.ChartConfigurationRequired = True
        End If
    End Sub

    ''' <summary> selects a new channel or an existing channel. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ChannelComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ChannelComboBox.SelectedIndexChanged
        If Me._ChannelComboBox.SelectedIndex >= 0 Then
            Dim physicalChannelNumber As Short = Short.Parse(Me._ChannelComboBox.Text, Globalization.CultureInfo.CurrentCulture)
            If Me.IsOpen AndAlso Me.AnalogInput.HasChannels Then
                If Me.AnalogInput.PhysicalChannelExists(physicalChannelNumber) Then
                    Dim channel As Global.OpenLayers.Base.ChannelListEntry = Me.AnalogInput.ChannelList.SelectPhysicalChannel(physicalChannelNumber)
                    Me._ChannelNameTextBox.Text = channel.Name
                    Me._ChannelRangesComboBox.SelectedIndex = Me.AnalogInput.SupportedGainIndex(channel.Gain)
                    Me._AddChannelButton.Text = "Update"
                Else
                    Me._AddChannelButton.Text = "Add"
                    Me._ChannelNameTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                "A in {0}", physicalChannelNumber)
                End If
            Else
                Me._ChannelNameTextBox.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "A in {0}", physicalChannelNumber)
            End If
        End If
    End Sub

    ''' <summary> Event handler. Called by _ChannelsListView for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ChannelsListView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ChannelsListView.Click
        If Me.IsOpen AndAlso Me._ChannelsListView.SelectedIndices.Count > 0 Then
            Try
                Me.selectChannel(Me._ChannelsListView.SelectedIndices.Item(0))
            Catch ex As Global.OpenLayers.Base.OlException
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred selecting channel;. Details: {0}.", ex)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called by _ContinuousBufferingCheckBox for checked changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ContinuousBufferingCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ContinuousBufferingCheckBox.CheckedChanged
        If Me._ContinuousBufferingCheckBox.Enabled Then
            Me.FiniteBuffering = Not Me._ContinuousBufferingCheckBox.Checked
            ' request configuration of analog input and chart.
            Me.AnalogInputConfigurationRequired = True
            Me.ChartConfigurationRequired = True
        End If
    End Sub

    ''' <summary> Event handler. Called by _MovingAverageLengthNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _MovingAverageLengthNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _MovingAverageLengthNumeric.ValueChanged
        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Removes the selected channel and signal from the lists. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _RemoveChannelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _RemoveChannelButton.Click
        If Me.selectedChannel IsNot Nothing Then
            Try
                Me.RemoveChannel(Me.selectedChannel)
            Catch ex As Global.OpenLayers.Base.OlException
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred removing the selected channel;. Details: {0}.", ex)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called by _SampleSizeNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _SampleSizeNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _SampleSizeNumeric.ValueChanged
        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Event handler. Called by _SamplingRateNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _SamplingRateNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _SamplingRateNumeric.ValueChanged
        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Event handler. Called by _SmartBufferingCheckBox for checked changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _SmartBufferingCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SmartBufferingCheckBox.CheckedChanged
        If Me._SmartBufferingCheckBox.Enabled Then
            ' request configuration of analog input and chart.
            Me.AnalogInputConfigurationRequired = True
            Me.ChartConfigurationRequired = True
        End If
    End Sub

#End Region

#Region " SIGNALS CONTROLS HANDLERS "

    ''' <summary> Adds a signals. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _AddSignalButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AddSignalButton.Click
        If Me.AnalogInput IsNot Nothing AndAlso Me._AddSignalButton.Enabled AndAlso Me._SignalChannelComboBox.SelectedIndex >= 0 Then
            Try
                Me.AddSignal(Me.AnalogInput.ChannelList(Me._SignalChannelComboBox.SelectedIndex),
                             CDbl(Me._SignalMinNumericUpDown.Value), CDbl(Me._SignalMaxNumericUpDown.Value),
                             Me._defaultSignalColors(Me.Chart.Signals.Count))
            Catch ex As Global.OpenLayers.Base.OlException
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred adding signal;. Details: {0}.", ex)
            End Try
        End If
    End Sub

    ''' <summary> Update selected signal color. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _ColorSignalButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles _ColorSignalButton.Click
        If Me.selectedSignal IsNot Nothing Then
            Try
                Using dialog As New Windows.Forms.ColorDialog
                    dialog.Color = Me.SelectedSignalColor
                    If dialog.ShowDialog() = DialogResult.OK Then
                        Me.SelectedSignalColor = dialog.Color
                    End If
                End Using
            Catch ex As Global.OpenLayers.Base.OlException
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting signal color;. Details: {0}.", ex)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called by _RemoveSignalButton for click events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _RemoveSignalButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _RemoveSignalButton.Click
        Try
            Me.RemoveSignal(Me.selectedSignal)
        Catch ex As Global.OpenLayers.Base.OlException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred removing signal;. Details: {0}.", ex)
        End Try
    End Sub

    ''' <summary> Event handler. Called by _SignalBufferSizeNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _SignalBufferSizeNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _SignalBufferSizeNumeric.ValueChanged
        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Event handler. Called by _SignalMemoryLengthNumeric for value changed events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _SignalMemoryLengthNumeric_ValueChanged(sender As Object, e As System.EventArgs) Handles _SignalMemoryLengthNumeric.ValueChanged
        ' request configuration of analog input and chart.
        Me.AnalogInputConfigurationRequired = True
        Me.ChartConfigurationRequired = True
    End Sub

    ''' <summary> Event handler. Called by _SignalChannelComboBox for selected value changed
    ''' events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _SignalChannelComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SignalChannelComboBox.SelectedValueChanged
        ' Update the signal ranges based on the channel
        Me.enableAddSignalControl()
        If Me.IsOpen Then
            Try
                Me.AnalogInput.SelectLogicalChannel(Me._SignalChannelComboBox.SelectedIndex)
                Me._SignalMaxNumericUpDown.Value = Decimal.Parse(Me.AnalogInput.MaxVoltage.ToString(Globalization.CultureInfo.CurrentCulture),
                                                                 Globalization.CultureInfo.CurrentCulture)
                Me._SignalMinNumericUpDown.Value = Decimal.Parse(Me.AnalogInput.MinVoltage.ToString(Globalization.CultureInfo.CurrentCulture),
                                                                 Globalization.CultureInfo.CurrentCulture)
            Catch ex As Global.OpenLayers.Base.OlException
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred selecting signal channel;. Details: {0}.", ex)
            End Try
        End If
    End Sub

    ''' <summary> Removes a signal from the list. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _SignalsListView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SignalsListView.Click
        If Me.Chart.Signals.Count > 0 AndAlso Me._SignalsListView.SelectedIndices.Count > 0 Then
            Try
                Me.selectSignal(Me._SignalsListView.SelectedIndices.Item(0))
            Catch ex As Global.OpenLayers.Base.OlException
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred selecting signal;. Details: {0}.", ex)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called by _SignalMaxNumericUpDown for validating events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub _SignalMaxNumericUpDown_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _SignalMaxNumericUpDown.Validating
        e.Cancel = Me._SignalMaxNumericUpDown.Value <= Me._SignalMinNumericUpDown.Value
    End Sub

    ''' <summary> Event handler. Called by _SignalMinNumericUpDown for validating events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub _SignalMinNumericUpDown_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _SignalMinNumericUpDown.Validating
        e.Cancel = Me._SignalMaxNumericUpDown.Value <= Me._SignalMinNumericUpDown.Value
    End Sub

    ''' <summary> Updates the selected signal. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _UpdateSignalButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _UpdateSignalButton.Click
        If Me.selectedSignal IsNot Nothing Then
            If Me._SignalMinNumericUpDown.Value < Me._SignalMaxNumericUpDown.Value Then
                Try
                    Me.Chart.DisableRendering()
                    If Me.selectedSignal.RangeMin < Me._SignalMinNumericUpDown.Value AndAlso
                        Me.selectedSignal.RangeMax > Me._SignalMinNumericUpDown.Value Then
                        Me.selectedSignal.CurrentRangeMin = CDbl(Me._SignalMinNumericUpDown.Value)
                    End If
                    If Me.selectedSignal.RangeMin < Me._SignalMaxNumericUpDown.Value AndAlso
                        Me.selectedSignal.RangeMax > Me._SignalMaxNumericUpDown.Value Then
                        Me.selectedSignal.CurrentRangeMax = CDbl(Me._SignalMaxNumericUpDown.Value)
                    End If
                    Me.Chart.SignalListUpdate()
                    Me.Chart.EnableRendering()
                    Me.updateSignalList()
                Catch ex As Global.OpenLayers.Base.OlException
                    Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred updating selected signal;. Details: {0}.", ex)
                End Try
            End If
        End If
    End Sub

#End Region

End Class

