﻿ICE82
We are getting the following warnings:
Warning	4	ICE82: This action System64Folder.42E31C33_C04F_4C2C_A473_E8670D8D0CD5 has duplicate sequence number 3 in the table AdminExecuteSequence	light.exe	0	1	SingleIo.WiX
Warning	3	ICE82: This action System64Folder.42E31C33_C04F_4C2C_A473_E8670D8D0CD5 has duplicate sequence number 2 in the table InstallUISequence	light.exe	0	1	SingleIo.WiX
Warning	2	ICE82: This action System64Folder.42E31C33_C04F_4C2C_A473_E8670D8D0CD5 has duplicate sequence number 2 in the table InstallExecuteSequence	light.exe	0	1	SingleIo.WiX
Warning	6	ICE82: This action System64Folder.42E31C33_C04F_4C2C_A473_E8670D8D0CD5 has duplicate sequence number 2 in the table AdvtExecuteSequence	light.exe	0	1	SingleIo.WiX
Warning	5	ICE82: This action System64Folder.42E31C33_C04F_4C2C_A473_E8670D8D0CD5 has duplicate sequence number 2 in the table AdminUISequence	light.exe	0	1	SingleIo.WiX

Per Rob Mensching: "Duplicate sequence numbers are not a problem as long as you don't need the
order of the CustomActions to be consistent.  These type 51 CustomActions' 
order relative to each other shouldn't be important."

(see http://windows-installer-xml-wix-toolset.687559.n2.nabble.com/ICE82-Warnings-from-duplicate-sequence-number-created-from-lt-Directory-gt-elements-using-Merge-Modus-td687752.html)

Also at http://windows-installer-xml-wix-toolset.687559.n2.nabble.com/LGHT1076-ICE82-This-action-has-duplicate-sequence-number-1-td3181987.html
Rob Mensching: "Its a warning telling you that multiple Merge Modules have had the action that correctly CommonFilesFolder in the Merge Modules scheduled at the same number. This is caused by a stupidity in mergemod.dll. 
Have you considered .wixlibs instead of Merge Modules?"

Fragments versus Merge Modules:
Rob Mensching from the above thread: "...Personally, I always recommend breaking your setup code into as many Fragments that make sense.  Then build those Fragments into MSI files or Merge Modules as appropriate for your redistribution needs. 
Fragments are the most flexible way to break your setup code into reusable pieces. 
